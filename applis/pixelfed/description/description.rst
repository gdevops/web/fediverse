
.. _pixelfed_description:

======================================================================================================
**pixelfed description**
======================================================================================================


https://fr.wikipedia.org/wiki/Pixelfed
=======================================

- https://fr.wikipedia.org/wiki/Pixelfed

Pixelfed est un système de partage d'images sous la forme d'un logiciel libre,
utilisant le protocole ActivityPub, pour se fédérer au « Fediverse »,
lancé à la mi-avril 2018, par un développeur canadien, Daniel Supernault.

Il permet de partager des images avec Friendica25, Mastodon, Nextcloud,
PeerTube, Pleroma, etc..
Comme c'est un logiciel libre et à source ouvertes, il est possible
d'installer sa propre instance, tout en se connectant à cette fédération
du Fédiverse.

Il est parfois présenté comme une alternative à Instagram (de Facebook)
et sa politique de censure, dont il permet d'importer les données.

Le système est développé dans le langage de script PHP et utilise le
framework Laravel.

Il utilise une interface utilisateur, appelée Compose UI permettant lors
du chargement d'images de faire des modifications sur son aspect colorimétrique,
il est également possible d'inclure les publications de PixelFed dans
n'importe quelle page web, via la balise embed.

Il comporte également des outils de modération et un concept appelé « stories »,
permettant d'échanger des histoires entre instances PixelFed ou autre
instance ActivityPub supportant ce type d'activité (au sens d'ActivityPub)


Client tiers
==============

Une application Android est disponible pour interagir avec l'API de Pixelfed
