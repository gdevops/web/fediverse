.. index::
   pair: servers ; pixelfed

.. _pixelfed_servers:

======================================================================================================
**pixelfed servers**
======================================================================================================

- https://fedidb.org/software/pixelfed


.. toctree::
   :maxdepth: 3


   pixelfed_fr/pixelfed_fr
   pixelfed_photos/pixelfed_photos
