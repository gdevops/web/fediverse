
.. _pixelfed_0_11_4:

======================================================================================================
**pixelfed 0.11.4 (2022-10-05)**
======================================================================================================

- https://github.com/pixelfed/pixelfed/releases/tag/v0.11.4


New Features
===============

- Custom content warnings/spoiler text (d4864213)
- Add NetworkTimelineService cache (1310d95c)
- Customizable Legal Notice page (0b7d0a96)

