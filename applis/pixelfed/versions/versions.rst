
.. _pixelfed_versions:

======================================================================================================
**pixelfed**
======================================================================================================

- https://github.com/pixelfed/pixelfed
- https://github.com/pixelfed/pixelfed/releases


.. toctree::
   :maxdepth: 3

   0.11.4/0.11.4
