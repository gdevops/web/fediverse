.. index::
   pair: Fediverse ; pixelfed
   ! pixelfed


.. figure:: images/logo_pixelfed.png
   :align: center
   :width: 400

.. _pixelfed:

======================================================================================================
**pixelfed (Photo Sharing. For Everyone)**
======================================================================================================

- https://github.com/pixelfed/pixelfed
- https://fr.wikipedia.org/wiki/Pixelfed
- https://pixelfed.org/
- https://mastodon.social/@pixelfed

.. figure:: images/photo_sharing.png
   :align: center


.. toctree::
   :maxdepth: 3

   description/description
   doc/doc
   instances/instances
   versions/versions
