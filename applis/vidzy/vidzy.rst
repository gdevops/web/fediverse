.. index::
   pair: Fediverse ; vidzy
   ! vidzy

.. _vidzy:

======================================================================================================
**vidzy (The federated alternative to TikTok)**
======================================================================================================

- https://vidzy.codeberg.page/
- https://github.com/vidzy-social/vidzy
- https://fosstodon.org/@vidzy
