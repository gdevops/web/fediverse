.. index::
   pair: Fediverse ; Applications


.. _applis_fediverse:

===================================================================
**Fediverse applications**
===================================================================

- https://gofoss.net/fr/fediverse/


.. figure::  ../images/fediverse_applis.png
   :align: center
   :width: 800

   https://axbom.com/fediverse/


.. toctree::
   :maxdepth: 5

   bookwyrm/bookwyrm
   castopod/castopod
   catodon/catodon
   emissary/emissary
   firefish/firefish
   loops/loops   
   mastodon/mastodon
   mobilizon/mobilizon
   nextcloud/nextcloud
   peertube/peertube
   pixelfed/pixelfed
   pleroma/pleroma
   smithereen/smithereen
   vidzy/vidzy   
   wordpress/wordpress
