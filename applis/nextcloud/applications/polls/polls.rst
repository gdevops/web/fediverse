.. index::
   pair: polls ; nextcloud
   ! polls

.. _nextcloud_polls:

=============================================================================================
**polls**
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/polls

.. contents::
   :depth: 3


Features
=========

- Create / edit polls (datetimes and texts)
- Set an expiration date
- Restrict access (all site users or invited users only)
- Comments
- Create public polls
- Invite users, groups and contacts (directly or via circles or contact groups)
- Hide results until the poll is closed
- Create anonymised polls (participants names get pseudonymized for other users)
