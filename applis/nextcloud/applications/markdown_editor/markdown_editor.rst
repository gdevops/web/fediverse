.. index::
   pair: markdown ; mardown editor
   ! mardown editor

.. _nextcloud_markdown_editor:

==========================================================
**mardown editor** Nextcloud markdown editor
==========================================================

.. seealso::

   - https://github.com/icewind1991/files_markdown
   - https://opensource.com/article/20/12/nextcloud-markdown



.. contents::
   :depth: 3


