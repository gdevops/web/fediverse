.. index::
   pair: onlyoffice ; nextcloud
   ! onlyoffice

.. _nextcloud_onlyoffice:

================================================================================================================
**onlyoffice**  allows multiple users to collaborate in real time and to save back those changes to Nextcloud
================================================================================================================

.. seealso::

   - https://github.com/ONLYOFFICE/onlyoffice-nextcloud
   - https://apps.nextcloud.com/apps/onlyoffice
   - https://www.onlyoffice.com/fr/

.. contents::
   :depth: 3


Description
===========


Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
