.. index::
   pair: external ; nextcloud
   ! external

.. _nextcloud_external:

=============================================================================================
**external**  Embed external sites in your Nextcloud
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/external
   - https://apps.nextcloud.com/apps/external
   - https://docs.nextcloud.com/server/14/admin_manual/configuration_server/external_sites.html

.. contents::
   :depth: 3

Sphinx documentation
======================

.. seealso::

   - https://docs.nextcloud.com/server/14/admin_manual/configuration_server/external_sites.html


Description
===========

This application allows an admin to add additional links into the
Nextcloud menus.

Following a link, the external website appears in the Nextcloud frame.

It is also possible to add links only for a given language, device type
or user group.

More information is available in the External sites documentation.



OCS API
==========

It is also possible to get the sites via an OCS endpoint.

The request must be authenticated. Only sites for the user´s language are
returned::

    curl  -H "OCS-APIRequest: true" \
      https://admin:admin@localhost/ocs/v2.php/apps/external/api/v1
