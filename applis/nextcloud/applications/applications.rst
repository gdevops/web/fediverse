.. index::
   pair: Applications ; nextcloud

.. _nextcloud_applications:

============================================================
Applications
============================================================

.. seealso::

   - https://github.com/nextcloud/appstore
   - https://x.com/nextcloudupdate

.. toctree::
   :maxdepth: 3

   appstore/appstore
   activity/activity
   bbb/bbb
   calendar/calendar
   circles/circles
   deck/deck
   external/external
   federation/federation
   files/files
   mail/mail
   markdown_editor/markdown_editor
   notes/notes
   onlyoffice/onlyoffice
   polls/polls
   spreed/spreed
   tasks/tasks
   text/text
