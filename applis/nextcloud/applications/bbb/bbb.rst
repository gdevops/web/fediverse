.. index::
   ! Big Blue Button

.. _nextcloud_bbb:

=============================================================================================
**Big Blue Button**
=============================================================================================

.. seealso::

   - https://apps.nextcloud.com/apps/bbb
   - https://github.com/sualko/cloud_bbb/

.. contents::
   :depth: 3


Description
===============

This app allows to create meetings with an external installation of BigBlueButton.

- Room setup Create multiple room configurations with name, welcome message …
- Share guest link Share the room link with all your guests
- Share rooms Share rooms with members, groups or circles
- Custom presentation Start a room with a selected presentation from your file browser
- Manage recordings View, share and delete recordings for your rooms
- Restrictions Restrict room creation to certain groups
- Activities Get an overview of your room activities

Developer wanted! If you have time it would be awesome if you could help
to enhance this application.

Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
