.. index::
   pair: Big Blue Button; Versions

.. _nextcloud_bbb_versions:

=============================================================================================
**Big Blue Button** versions
=============================================================================================

.. seealso::

   - https://github.com/sualko/cloud_bbb/releases
   - https://github.com/sualko/cloud_bbb/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   1.1.4/1.1.4
