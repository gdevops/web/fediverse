
.. _nextcloud_text_versions:

=============================================================
Versions
=============================================================

.. seealso::

   - https://github.com/nextcloud/text/releases

.. toctree::
   :maxdepth: 3


   1.0.2/1.0.2

