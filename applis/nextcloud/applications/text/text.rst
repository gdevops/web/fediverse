.. index::
   pair: Text ; nextcloud
   ! text

.. _nextcloud_text:

=============================================================
**📑 text** (Collaborative document editing using Markdown)
=============================================================

.. seealso::

   - https://github.com/nextcloud/text
   - https://github.com/nextcloud/text/graphs/contributors
   - https://github.com/juliushaertl


.. toctree::
   :maxdepth: 3


   description/description
   versions/versions


