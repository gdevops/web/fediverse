.. index::
   pair: federation ; nextcloud
   ! federation

.. _nextcloud_federation:

=============================================================================================
**federation**
=============================================================================================

.. contents::
   :depth: 3


Federation allows you to connect with other trusted servers to exchange
the user directory.

For example this will be used to auto-complete external users for
federated sharing.
