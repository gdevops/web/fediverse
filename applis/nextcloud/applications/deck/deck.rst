.. index::
   pair: deck ; nextcloud
   pair: deck ; kanban
   ! deck

.. _nextcloud_deck:

=============================================================================================
**deck** (Kanban-style project & personal management tool for Nextcloud, similar to Trello)
=============================================================================================

.. seealso::

   - :ref:`project:project_nextcloud_deck`
   - https://github.com/nextcloud/deck
   - https://apps.nextcloud.com/apps/deck
   - https://deck.readthedocs.io/en/latest/
