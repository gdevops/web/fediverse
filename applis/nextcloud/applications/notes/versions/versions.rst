
.. _nextcloud_notes_versions:

==========================================================
Versions
==========================================================

.. seealso::

   - https://github.com/nextcloud/notes/blob/master/CHANGELOG.md
   - https://github.com/nextcloud/notes/releases
   - https://github.com/nextcloud/notes/commits/master


.. toctree::
   :maxdepth: 3

   4.0.1/4.0.1
