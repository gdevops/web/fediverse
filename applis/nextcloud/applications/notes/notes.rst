.. index::
   pair: notes ; nextcloud
   ! notes

.. _nextcloud_notes:

==========================================================
**✎ notes** (Distraction-free notes and writing)
==========================================================

.. seealso::

   - https://github.com/nextcloud/notes
   - https://github.com/nextcloud/notes/graphs/contributors
   - https://apps.nextcloud.com/apps/notes
   - https://github.com/nextcloud/notes/wiki
   - https://en.wikipedia.org/wiki/Markdown


.. contents::
   :depth: 3


Description
===========


The **Notes app** is a distraction free notes taking app for Nextcloud.

It provides categories for better organization and supports formatting
using `Markdown syntax <https://en.wikipedia.org/wiki/Markdown>`_.

Notes are saved as files in your Nextcloud, so you can view and edit
them with every Nextcloud client.

Furthermore, a separate REST API allows for an easy integration into
third-party apps (currently, there are notes apps for Android, iOS and
the console which allow convenient access to your Nextcloud notes).

Further features include marking notes as favorites.


wiki
=====

.. seealso::

   - https://github.com/nextcloud/notes/wiki


issues
==========

.. seealso::

   - https://github.com/nextcloud/notes/issues

Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
