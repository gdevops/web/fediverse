.. index::
   pair: circles ; nextcloud
   ! circles

.. _nextcloud_circles:

=============================================================================================
**circles**
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/circles
   - https://github.com/nextcloud/circles/graphs/contributors
   - https://github.com/daita

.. contents::
   :depth: 3


Description
===============

**Circles** allows your users to create their own groups of users/colleagues/friends.

Those **groups of users** (or circles) can then be used by any other app
for sharing purpose (files, social feed, status update, messaging, …)
through the Circles API.

**Different types of circles** can be created:

- **A Personal Circle** is a list of users known only to the owner.
  This is the right option if you want to do recurrent sharing with the same list of local users.

- **A Public Circle** is an open group visible to anyone willing to join.
  Anyone can see the circle, can join the circle and access the items
  shared to the circle.

- Joining a **Closed Circle** requires an invitation or a confirmation by a
  moderator.
  Anyone can find the circle and request an invitation; but only members
  will see who's in it and get access to shared items.

- A **Secret Circle** is an hidden group that can only be seen by its
  members or by people knowing the exact name of the circle.

  Non-members won't be able to find your secret circle using the search bar.


Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
