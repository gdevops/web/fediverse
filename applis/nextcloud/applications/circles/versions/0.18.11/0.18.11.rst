
.. _nextcloud_circles_0_18_11:

=============================================================================================
**0.18.11 (2020-12-11)**
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/circles/releases/tag/v0.18.11
   - https://github.com/nextcloud/circles/tree/v0.18.11
   - https://github.com/nextcloud/circles/commit/ce96bbb584531bcb149a3e0313163d2c7ee8dfa7

