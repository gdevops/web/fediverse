
.. _nextcloud_circles_versions:

=============================================================================================
Versions
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/circles/releases
   - https://github.com/nextcloud/circles/releases/latest


.. toctree::
   :maxdepth: 3

   0.18.11/0.18.11
