.. index::
   pair: activity ; nextcloud
   ! activity

.. _nextcloud_activity:

=============================================================================================
**activity** Activity app for Nextcloud
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/activity

.. contents::
   :depth: 3

