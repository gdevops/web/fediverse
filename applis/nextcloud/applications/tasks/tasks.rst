.. index::
   pair: tasks ; nextcloud
   ! tasks

.. _nextcloud_tasks:

=============================================================================================
**tasks** (Tasks app for Nextcloud)
=============================================================================================

.. seealso::

   - :ref:`project:project_nextcloud_tasks`
   - https://github.com/nextcloud/tasks
   - https://apps.nextcloud.com/apps/tasks

.. contents::
   :depth: 3



Description
===========

.. seealso::

   - https://apps.nextcloud.com/apps/tasks


Once enabled, a new Tasks menu will appear in your Nextcloud apps menu.

From there you can add and delete tasks, edit their title, description,
start and due dates and mark them as important.

Tasks can be shared between users.

Tasks can be synchronized using CalDav (each task list is linked to an
Nextcloud calendar, to sync it to your local client - Thunderbird,
Evolution, KDE Kontact, iCal, … - just add the calendar as a remote
calendar in your client).

You can download your tasks as ICS files using the download button
for each calendar.


Features
=============

.. seealso::

   - https://github.com/nextcloud/tasks


- add and delete tasks, edit their title, description, start and due dates,
  set their priority and status
- support for subtasks
- smart collections showing you your important, current and upcoming tasks
- simply drag and drop tasks to other calendars or make them subtasks


Tutorials
===========

.. toctree::
   :maxdepth: 3

   tutorials/tutorials
