.. index::
   pair: calendar ; nextcloud
   ! calendar

.. _nextcloud_calendar:

=============================================================================================
**calendar**
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/calendar

.. contents::
   :depth: 3

