.. index::
   pair: spreed ; nextcloud
   ! spreed

.. _nextcloud_spreed:

=============================================================================================
spreed 💬 **Nextcloud Talk** – chat, video & audio calls for Nextcloud
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/spreed
   - https://nextcloud.com/talk/
   - https://github.com/nextcloud/spreed/milestones/

.. contents::
   :depth: 3


Description
============

Why is this so awesome ?
---------------------------

- 💬 **Chat** Nextcloud Talk comes with a simple text chat, allowing you
  to share or upload files from your Nextcloud Files app or local device
  and mentioning other participants.
- 👥 **Private, group, public and password protected calls!**
  Invite someone, a whole group or send a public link to invite to a call.
- 💻 **Screen sharing!** Share your screen with participants of your call.
  You just need to use Firefox version 52 (or newer), latest Edge or
  Chrome 49 (or newer) with this `Chrome extension <https://chrome.google.com/webstore/detail/screensharing-for-nextclo/kepnpjhambipllfmgmbapncekcmabkol>`_.
- 🚀 **Integration with other Nextcloud apps** like Files, Calendar,
  User status, Dashboard, Flow, Contacts and Deck, with more to come.
- 🎡 **We’re not reinventing the wheel!**
  Based on the great `simpleWebRTC <https://simplewebrtc.com>`_ library.
- 🌉 **Sync with other chat solutions** With `Matterbridge <https://github.com/42wim/matterbridge>`_
  being integrated in Talk, you can easily sync a lot of other chat
  solutions to Nextcloud Talk and vice-versa.


Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
