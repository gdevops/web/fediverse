.. index::
   pair: desktop nextcloud ; Versions

.. _nextcloud_desktop_versions:

==============================================================
nextcloud desktop client versions
==============================================================

.. seealso::

   - https://github.com/nextcloud/desktop/releases


.. toctree::
   :maxdepth: 3

   2.5.2/2.5.2
