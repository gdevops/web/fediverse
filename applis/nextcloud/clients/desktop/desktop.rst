.. index::
   pair: desktop ; nextcloud
   pair: sync ; nextcloud

.. _nextcloud_desktop:

==============================================================
nextcloud desktop client (Desktop sync client for Nextcloud)
==============================================================

.. seealso::

   - https://github.com/nextcloud/desktop


.. toctree::
   :maxdepth: 3

   versions/versions
