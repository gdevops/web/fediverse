.. index::
   pair: Nextcloud; discourse


.. _nextcloud_forum:
.. _nextcloud_discourse:

======================================================================================================
Forum discourse
======================================================================================================

.. seealso::

   - https://help.nextcloud.com/

.. contents::
   :depth: 3

Description par Brume
=========================

.. seealso::

   - https://blog.genma.fr/?10-manieres-de-se-tenir-au-courant-de-l-actualite-Nextcloud
   - https://brume.ink/

Billet invité, rédigé par Brume https://brume.ink/ Merci à elle. ;-)


Le forum est un lieu de discussion privilégié avec la communauté Nextcloud.
Les questions, rapports de bugs, débats, nouveautés s’échangent régulièrement
à raison de plusieurs messages par heure.

Le forum est majoritairement en anglais, mais il est possible de poster
des messages dans d’autres langues, dont le français.

