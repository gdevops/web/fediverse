.. index::
   pair: Fediverse ; nextcloud
   ! nextcloud

.. _nextcloud:
.. _fediverse_nextcloud:

======================================================================================================
**nextcloud** (A safe home for all your data)
======================================================================================================

- https://nextcloud.com/
- https://fr.wikipedia.org/wiki/Nextcloud
- https://mastodon.xyz/@nextcloud
- https://github.com/nextcloud
- https://nextcloud.com/
- https://x.com/Nextclouders
- https://x.com/nextcloudupdate
- https://x.com/fkarlitschek
- https://docs.nextcloud.com/
- https://nextcloud.com/blog/
- https://nextcloud.com/blog/nextcloud-introduces-social-features-joins-the-fediverse/
- https://github.com/nextcloud/documentation
- https://docs.nextcloud.com/
- https://nextcloud.com/blogfeed
- https://github.com/nextcloud/nextcloud_announcements

.. figure:: Nextcloud_Logo.svg.png
   :align: center
   :width: 200

.. figure:: Mastodon-and-Nextcloud-small.png
   :align: center
   :width: 200

   https://nextcloud.com/blog/nextcloud-introduces-social-features-joins-the-fediverse/

.. toctree::
   :maxdepth: 3

   applications/applications
   people/people
   blog/blog
   conferences/conferences
   include/include
   doc/doc
   forum/forum
   mastodon/mastodon
   tutorials/tutorials
   hebergeurs/hebergeurs
   server/server
   clients/clients
   youtube/youtube
