.. index::
   ! Frank Karlitschek

.. _frank_karlitschek:

======================================================================================================
**Frank Karlitschek**
======================================================================================================

.. seealso::

   - https://karlitschek.de/
   - http://en.wikipedia.org/wiki/Open_Collaboration_Services
   - http://userdatamanifesto.org/

.. toctree::
   :maxdepth: 3


Frank Karlitschek is a long time open source contributor and privacy
activist.

He contributed to KDE and other free software projects since the end of
the 90s and was a board member of the KDE e.V.
He is a regular keynote speaker at conferences.

He managed engineering teams for over 10 years and worked as head of unit
and managing director at different internet companies.

2007 he founded a startup which develops social networking and e-commerce
products for several fortune 500 companies.

Over the years he helped several startups with management consulting and
investments to bootstrap and build successful products

Frank launched several initiatives over the years to make the internet
more secure and more federated.

Examples are the `open collaboration services <http://en.wikipedia.org/wiki/Open_Collaboration_Services>`_
and the `user data manifesto <http://userdatamanifesto.org/>`_.

In 2010 he started the ownCloud project and is leading the community
project since then.

In 2011 he co-founded ownCloud Inc. to offer commercial services around
ownCloud.

In 2016 he left ownCloud and founded Nextcloud to bring the vision to
the next level.

He currently serves as managing director at Nextcloud GmbH.

All pictures and text licensed as CC-by-sa 2.0

Email: frank (at) karlitschek.de

::

    GPG: public key
    Fingerprint: 4F3C B271 9631 AA66 D74F 9F99 FB10 6A0B 7766 CF1E
