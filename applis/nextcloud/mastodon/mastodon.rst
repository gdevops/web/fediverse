.. index::
   pair: mastodon ; nextcloud

.. _nextcloud_mastodon:

======================================================================================================
**nextcloud on mastodon** (@nextcloud@mastodon.xyz)
======================================================================================================


- https://mastodon.xyz/@nextcloud
- https://the-federation.info/



.. figure:: join_the_federation.jpeg
   :width: 400
   :align: center

   https://the-federation.info/

.. figure:: mastodon.png
   :align: center

   https://mastodon.xyz/@nextcloud
