.. index::
   pair: server ; nextcloud

.. _nextcloud_server:

==========================
nextcloud server
==========================

.. seealso::

   - https://github.com/nextcloud/server
   - https://docnexcloud.gitlab.io/admin
   - https://nextcloud.com/support/
   - https://docs.nextcloud.com/server/20/admin_manual/

.. figure:: logo_nextcloud_server.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   sphinx/sphinx
