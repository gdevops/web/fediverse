.. index::
   pair: sphinx ; nextcloud

.. _nextcloud_sphinx:

==========================
nextcloud documentation
==========================

.. seealso::

   - https://github.com/nextcloud/documentation
   - https://docs.nextcloud.com/server/latest/developer_manual/index.html
   - https://docnexcloud.gitlab.io/admin
   - https://nextcloud.com/support/
   - https://docs.nextcloud.com/server/20/admin_manual/
