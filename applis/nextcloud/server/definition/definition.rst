
.. _nextcloud_server_def:

=============================
nextcloud server definition
=============================

.. contents::
   :depth: 3


Github owncloud server definition
=====================================

.. seealso::

   - https://github.com/nextcloud/server/blob/master/README.md


.. include:: README.rst
