.. index::
   pair: nextcloud server; versions

.. _nextcloud_server_versions:

==========================
nextcloud server versions
==========================

.. seealso::

   - https://github.com/nextcloud/server/releases
   - https://nextcloud.com/changelog/
   - https://github.com/nextcloud/nextcloud_announcements
   - https://x.com/nextcloudupdate

.. toctree::
   :maxdepth: 3

   20.0.0/20.0.0
   16.0.0/16.0.0
   15.0.0/15.0.0
