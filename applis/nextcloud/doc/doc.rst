.. index::
   pair: Documentation ; nextcloud
   ! Sphinx documentation

.. _fediverse_nextcloud_doc:

=================================
Nextcloud Documentation
=================================

.. seealso::

   - https://github.com/nextcloud/documentation
   - https://docs.nextcloud.com/
   - https://forum.chatons.org/t/des-moocs-pour-apprendre-a-utiliser-nextcloud-etherpad-etc/1380/13


.. toctree::
   :maxdepth: 3

   admin_manual/admin_manual
   developer_manual/developer_manual
   user_manual/user_manual
   tree/tree
