
.. _nextcloud_doc_tree:

==========================
Tree
==========================

.. seealso::

   - https://github.com/nextcloud/documentation


.. literalinclude:: tree.txt
   :linenos:
