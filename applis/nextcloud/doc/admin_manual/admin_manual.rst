.. index::
   pair: Documentation ; Admin manual

.. _nextcloud_admin_manual:

==========================
**Admin** manual
==========================

.. seealso::

   - https://docnexcloud.gitlab.io/admin
   - https://nextcloud.com/support/
   - https://docs.nextcloud.com/server/20/admin_manual/
