.. index::
   pair: Documentation ; User manual

.. _nextcloud_user_manual:

==========================
**User** manual
==========================

.. seealso::

   - https://docnexcloud.gitlab.io/user_manual
   - https://nextcloud.com/support/
   - https://github.com/nextcloud/documentation/tree/master/user_manual
   - https://docs.nextcloud.com/server/latest/user_manual/en/
