.. index::
   pair: Documentation ; Developer manual

.. _nextcloud_developer_manual:

==========================
**Developer** manual
==========================

.. seealso::

   - https://docnexcloud.gitlab.io/developer
   - https://nextcloud.com/support/
   - https://docs.nextcloud.com/server/latest/user_manual/fr/
