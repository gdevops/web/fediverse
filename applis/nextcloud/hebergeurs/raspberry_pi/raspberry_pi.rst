.. index::
   ! Raspberry Pi

.. _raspberry_pi_nextcloud:

=============================================================================================
Nextcloud pour Raspberry Pi: installation et fonctions
=============================================================================================

.. seealso::

   - https://www.hebergementwebs.com/configuration/nextcloud-pour-raspberry-pi-installation-et-fonctions

.. contents::
   :depth: 3

Nextcloud sur Raspberry Pi : un cloud privé pour peu cher
===========================================================

Il y a quelques années, il était impensable d’héberger soi-même un cloud.

Aujourd’hui, il suffit de posséder le logiciel libre Nextcloud et le
matériel approprié.

Etant donné que Nextcloud est maintenant également adapté au matériel
de nano-ordinateurs comme Raspberry Pi, vous pouvez théoriquement obtenir
votre propre solution cloud à un prix avantageux.

Bien que vous deviez accepter des limitations en termes de fonctionnalité
et de performance, les possibilités sont tout de même suffisantes pour
remplir de nombreux objectifs.

Les free-lances, les travailleurs indépendants et les PME peuvent notamment
tirer profit de la solution peu onéreuse Raspberry Pi Nextcloud.

