.. index::
   ! Gandhi

.. _gandhi_nextcloud_hebergeur:

=============================================================================================
Gandhi
=============================================================================================

.. seealso::

   - https://www.gandi.net/fr/simple-hosting/nextcloud

.. contents::
   :depth: 3

Reprenez le contrôle de vos données
=======================================

Depuis plus de 20 ans Gandi est un acteur indépendant de l'internet, et
nous cherchons à vous proposer les solutions pour permettre à nos clients
d'être eux aussi le plus indépendants possibles.

C'est pourquoi nous voulons faciliter l'accès à une solution de stockage
partagée. NextCloud est aujourd'hui reconnue comme l'alternative open
source des solutions telles que Dropbox, Google Drive ou iCloud.

Nextcloud permet de disposer de son propre espace Cloud de stockage et
de partage de fichiers, que vous soyez un particulier qui souhaite
stocker les photos de son smartphone ou bien une association à la
recherche d'une solution de partage.

Avec Simple Hosting Nextcloud nous souhaitons faciliter l'accès à cette
solution en vous fournissant un hébergement Simple Hosting configuré
avec Nextcloud pré-installé et prêt à l'emploi.

Vous en aurez ensuite la pleine liberté et responsabilité de l'administrer
selon votre usage et vos besoins.

