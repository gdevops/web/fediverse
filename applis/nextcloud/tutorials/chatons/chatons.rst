.. index::
   pair: Nextcloud; CHATONS

.. _nextcloud_chatons_tutorial:

======================================================================================================
**CHATONS** Tutorial
======================================================================================================

.. seealso::

   - https://wiki.chatons.org/doku.php/des_documents_en_commun_avec_nextcloud


.. contents::
   :depth: 3

Introduction
==============


Nextcloud est un logiciel libre qui permet de nombreuses choses.

Sa fonction principale est le partage de documents entre utilisateurs
de Nextcloud ou externes.

Il se présente comme une alternative complète libre et éthique à Google
Drive et Dropbox.

Ce logiciel est intégré à la liste des logiciels libres préconisés par
l’État français dans le cadre de la modernisation globale de ses systèmes
d’informations.
