.. index::
   pair: Nextcloud; wikipedia

.. _nextcloud_wikipedia_tutorial:

======================================================================================================
**wikipedia** Tutorial
======================================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Nextcloud


.. contents::
   :depth: 3

Introduction
==============

Nextcloud est un logiciel libre, de site d'hébergement de fichiers et une
plateforme de collaboration.

À l'origine accessible via WebDAV, n'importe quel navigateur web, ou des
clients spécialisés, son architecture ouverte a permis de voir ses
fonctionnalités s'étendre depuis ses origines.

En 2020, il propose de nombreux services.
