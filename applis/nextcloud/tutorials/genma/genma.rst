
.. _nextcloud_genma_tutorial:

======================================================================================================
Genma articles, @genma@x.com, @genma@framapiaf.org
======================================================================================================

.. seealso::

   - https://blog.genma.fr/?-Nextcloud-103-
   - https://x.com/genma
   - https://framapiaf.org/@genma
   - https://blog.genma.fr/?Mon-serveurs-Peertube
   - https://blog.genma.fr/?10-manieres-de-se-tenir-au-courant-de-l-actualite-Nextcloud
   - https://blog.genma.fr/?Lifehacking-Creation-de-tickets-dans-le-Kanban-Gitlab-via-l-API
   - https://blog.genma.fr/?Nextcloud-et-l-application-Notes
   - https://git.42l.fr/brume/Script_API_Gitlab/src/branch/master/tutoriel-api-gitlab-bash.md

.. contents::
   :depth: 3


.. figure:: blog_genma.png
   :align: center

   https://blog.genma.fr/?-Nextcloud-103-


Nextcloud-et-l-application-Notes
===================================

.. seealso::

   - https://blog.genma.fr/?Nextcloud-et-l-application-Notes
