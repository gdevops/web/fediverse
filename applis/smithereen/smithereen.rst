.. index::
   pair: Fediverse ; smithereen
   ! smithereen

.. _smithereen:

===========================================================================================================
**smithereen (Federated, ActivityPub-compatible social network server with friends, walls, and groups)**
===========================================================================================================

- https://github.com/grishka/Smithereen
- https://friends.grishka.me/grishka
- https://grishka.me/blog/activitypub-from-scratch/


Announce
===============

- https://grishka.me/blog/activitypub-from-scratch/
- https://github.com/grishka/miscellaneous/tree/master/Minimal%20ActivityPub%20server

Lately, after Elon Musk bought Twitter, people have started looking for its alternatives – and many found one in Mastodon.

Mastodon is a decentralized social media platform that works on the 
federation model, like email. The federation protocol is called ActivityPub 
and is a W3C standard, and Mastodon is far from being its only implementation, 
albeit it is the most popular one. 

Different implementations of this protocol are generally compatible with 
each other, as much as the overlaps in their user-facing functionality 
permit. 

I have my own ActivityPub server project, Smithereen. 

It’s basically VKontakte, but it’s decentralized and comes in green, and 
I’ll bring the wall back someday.



On mastodon
=================

- https://mastodon.social/@dansup/113650591610528742

My favourite fediverse project isn't my own

It's Smithereen by @grishka 

Grishka is building a federated Facebook/VK alternative, and also happens 
to be the official Mastodon android developer (guess who helped him land that job) 

He is a very talented developer, and his project deserves way more attention!

- https://github.com/grishka/Smithereen
