.. index::
   pair: Fediverse ; Firefish


.. _firefish:

====================================================================================================================
|firefish| **Firefish** (A greatly enhanced fork of Misskey with better UI/UX, security, features, and more!)
====================================================================================================================

- https://codeberg.org/firefish
- https://joinfirefish.org/
- https://codeberg.org/firefish/firefish.rss


.. toctree::
   :maxdepth: 3

   description/description
