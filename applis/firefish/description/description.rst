
.. _firefish_desc:

====================================================================================================================
Description
====================================================================================================================

- https://codeberg.org/firefish/firefish
- https://codeberg.org/firefish/firefish/src/branch/develop/FIREFISH.md

Introduction
===============

**Firefish** is based off of Misskey, a powerful microblogging server on
ActivityPub with features such as emoji reactions, a customizable web UI,
rich chatting, and much more!

Firefish adds many quality of life changes and bug fixes for users and
server admins alike.

Read `this document <https://codeberg.org/firefish/firefish/src/branch/develop/FIREFISH.md>`_
all for current and future differences.

Notable differences:

- Improved UI/UX (especially on mobile)
- Post editing
- Content importing
- Improved notifications
- Improved server security
- Improved accessibility
- Improved threads
- Recommended Servers timeline
- OCR image captioning
- New and improved Groups
- Better intro tutorial
- Compatibility with Mastodon clients/apps
- Backfill user information
- Advanced search
- Many more user and admin settings
- So much more!

🥂 Links
=============

- https://codeberg.org/firefish/firefish/src/branch/develop/CONTRIBUTING.md
- https://hosted.weblate.org/engage/firefish/
- https://joinfirefish.org/contact/

Want to get involved? Great!

If you have the means to, donations are a great way to keep us going.

If you know how to program in TypeScript, Vue, or Rust, read the `contributing document <https://codeberg.org/firefish/firefish/src/branch/develop/CONTRIBUTING.md>`_.

If you know a non-English language, translating Firefish on `Weblate <https://hosted.weblate.org/engage/firefish/>`_ help
bring Firefish to more people. No technical experience needed!

Want to write/report about us, have any professional inquiries, or just
have questions to ask? Contact us `here! <https://joinfirefish.org/contact/>`_

All links
===========

- Homepage: https://joinfirefish.org
- Flagship server: https://firefish.social
- Matrix support room https://matrix.to/#/#firefish:matrix.fedibird.com
- Official account: https://i.firefish.cloud/@firefish
- Server list: https://joinfirefish.org/join
- Weblate: https://hosted.weblate.org/engage/firefish
- Contact: https://joinfirefish.org/contact
