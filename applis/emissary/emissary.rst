.. index::
   pair: Fediverse ; Emissary


.. _emissary:

=========================================================================================================
|emissary| **Emissary**
=========================================================================================================

- https://github.com/EmissarySocial/emissary
- https://github.com/EmissarySocial/emissary/commits.atom
- https://emissary.dev
- https://mastodon.social/@benpate
- https://mastodon.social/@benpate.rss

::

    Yes. And there’s room for many similar apps that do this.
    I’m trying to build exactly this with #Emissary -> #ActivityPub + #RSS + #WebMentions.

    If you know Go or HTML/CSS, I’ll welcome all the help I can get:
    https://emissary.dev  @rmdes

    https://mastodon.social/@benpate/110634094995095724


.. toctree::
   :maxdepth: 3

   description/description
