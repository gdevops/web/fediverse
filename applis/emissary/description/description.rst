

.. _emissary_desc:

=========================================================================================================
**Description**
=========================================================================================================

- https://emissary.dev
- https://mastodon.social/@benpate
- https://mastodon.social/@benpate.rss


Introduction
=============

Emissary is a new kind of social app that natively connects to the Fediverse
and the IndieWeb.

This site is a resource for developers and designers who are building
Emissary apps, or contributing to the core server.

Please visit emissary.social to learn more about using Emissary for your
own website.

Technical Overview
=======================

Emissary is an open source project, available under the AGPL license.
It is built with: Go, MongoDB, HTMX, and Hyperscript.

Emissary is designed to run well in high-volume, multi-tenant environments,
and should play nice with your existing DevOps and CI processes.

It has minimal dependencies, requiring only MongoDB to run, and includes
many options for storing files and page templates.

Project Status
===================

This is a very new project, and is not ready for non-technical users.

The current project status is posted on this website.

You are also welcome to visit the GitHub repository or Project Kanban
board on Trello.
