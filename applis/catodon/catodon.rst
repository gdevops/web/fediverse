.. index::
   pair: Fediverse ; catodo


.. _catodon:

=========================================================================================================
**castodon** a new fediverse project. It is based on Misskey/Firefish
=========================================================================================================

- https://codeberg.org/catodon/catodon

A new fediverse project.

It is based on Misskey/Firefish, and it is the initiative of two Firefish
core team members.
