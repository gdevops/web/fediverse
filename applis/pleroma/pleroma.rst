.. index::
   pair: Fediverse ; pleroma
   ! pleroma

.. _pleroma:

======================================================================================================
**pleroma (plénitude)**
======================================================================================================

- https://git.pleroma.social/pleroma/pleroma
- https://fr.wikipedia.org/wiki/Pleroma
- https://blog.soykaf.com/post/what-is-pleroma/


.. figure:: images/logo_pleroma.png
   :align: center

.. toctree::
   :maxdepth: 3

   description/description
   doc/doc
   serveurs/serveurs
   versions/versions
