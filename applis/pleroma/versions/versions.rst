
.. _pleroma_versions:

======================================================================================================
**pleroma versions**
======================================================================================================

- https://git.pleroma.social/pleroma/pleroma
- https://pleroma.social/announcements/feed.xml


.. toctree::
   :maxdepth: 3

   2.4.4/2.4.4
