
.. _pleroma_doc:

======================================================================================================
**pleroma documentation**
======================================================================================================

- https://docs-develop.pleroma.social/
- https://git.pleroma.social/pleroma/docs


We use mkdocs to build the documentation and have the admonition extensions
that make it possible to add block-styled side content like example
summaries, notes, hints or warnings.

If you are unsure of how a specific syntax should look like, feel free
to look through the docs for an example.

