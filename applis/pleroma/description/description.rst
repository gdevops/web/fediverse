
.. _pleroma_description:

======================================================================================================
**pleroma description**
======================================================================================================


https://fr.wikipedia.org/wiki/Pleroma
======================================

- https://fr.wikipedia.org/wiki/Pleroma

Pleroma (en référence à Plérôme, du grec ancien : πλήρωμα (pleroma),
signifiant plénitude), est un réseau social et logiciel de microblog
créé en 2017, pouvant être auto-hébergé, libre, distribué et beaucoup
plus léger que Mastodon.

**Il est, comme Mastodon, fédéré sur le Fediverse, via le protocole ActivityPub**.

Il est donc possible d'échanger dans un même fil de discussion, avec des
instance de ces deux moteurs, ainsi que Peertube, Funkwhale, Pixelfed,
GNU social, Mobilizon, etc.

Pleroma permet de créer des billets au format brut, HTML, BBCode et Markdown
jusqu'à 5 000 caractères par défaut, mais ajustable par instance.

Depuis la version 2.1.0, sortie le 28 août 2020, il est possible de
prévisualiser le rendu avant de poster, sur le frontend par défaut5.

Le Backend Pleroma est développé avec le langage Elixir, le framework
Phoenix (en) et utilise une base de données PostgreSQL.

Le frontend Pleroma FE est écrit en JavaScript avec Vue.js.

Le frontend alternatif Mastodon FE, fourni de base avec Pleroma, est
basé sur celui de glitch-soc [archive], lui même issu du frontend
officiel de Mastodon.
