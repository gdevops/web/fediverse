.. index::
   pair: Fediverse ; bookwyrm


.. _bookwyrm:

=========================================================================================================
**bookwyrm** (réseau social pour garder la trace de vos lectures)
=========================================================================================================

- https://github.com/bookwyrm-social/bookwyrm
- https://joinbookwyrm.com/fr/


.. toctree::
   :maxdepth: 5


