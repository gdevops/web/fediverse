

.. _partage_video:

=========================================================================================================
**Pour partager une vidéo sur mastodon, créez un compte Peertube**
=========================================================================================================

- https://mastodon.social/@nicolasvivant/109380868768984807

Pour partager une vidéo sur mastodon, créez un compte Peertube et mettez-y votre
vidéo (en « non listée » plutôt qu'en « public » si vous voulez).

Ensuite collez le lien vers votre vidéo dans votre message sur Mastodon.

La liste des serveurs Peertube disponibles → https://joinpeertube.org/instances#instances-list

Permet de poster des vidéos longues sans surcharger votre instance.

#mastotips
