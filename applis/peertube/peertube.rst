.. index::
   pair: Fediverse ; Peertube


.. _peertube:

=========================================================================================================
**peertube** (ActivityPub-federated video streaming platform using P2P directly in your web browser)
=========================================================================================================

- https://github.com/Chocobozzz/PeerTube
- https://github.com/Chocobozzz/PeerTube/graphs/contributors
- https://joinpeertube.org/
- https://gofoss.net/fr/fediverse/#peertube
- https://gofoss.net/fr/fediverse/


.. toctree::
   :maxdepth: 5

   tips/tips
   tools/tools


