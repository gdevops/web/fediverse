.. index::
   pair: sepiasearch ; Peertube
   ! sepiasearch

.. _sepiasearch:

========================
**sepiasearch**
========================

- https://sepiasearch.org
- https://framagit.org/framasoft/peertube/search-index


Vous êtes sur le #FÉDIVERSE, c'est pas le royaume des #GAFAMS ici, alors
quand vous postez une vidéo, avant de choisir YouTube, regardez si elle
n'est pas déjà sur #PeerTube !!!

Pour ça, #SepiaSearch est votre ami·e : https://sepiasearch.org


Exemple::

    https://sepiasearch.org/search?search=The+Internet%27s+Own+Boy

