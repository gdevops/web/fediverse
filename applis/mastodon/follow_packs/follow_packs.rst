

.. _follow_pack:

======================================================================================================
**Follow Pack Directory**
======================================================================================================

- https://mastodonmigration.wordpress.com/2024/11/18/mastodon-follow-packs/

2024-11-19 The first Mastodon Follow Pack Directory is now published on the Mastodon Migration Blog
=====================================================================================================

- https://mastodon.online/@mastodonmigration/113506415884851750
- https://mastodonmigration.wordpress.com/2024/11/18/mastodon-follow-packs/

NOTICE - Follow Pack Directory Published

The first Mastodon Follow Pack Directory is now published on the Mastodon Migration Blog.

Click >>> @mastodonmigration.wordpress.com

Note this 'FediBlog' view provides local (followable!) access to listed Follow Pack accounts.

Follow the Mastodon Migration Blog profile to receive future directory updates directly to your feed. 

To view on Wordpress click here >>> 
