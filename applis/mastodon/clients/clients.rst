.. index::
   pair: Mastodon; Clients

.. _lecteurs_mastodon:

======================================================================================================
**Clients interface graphique mastodon**
======================================================================================================

- https://github.com/hueyy/awesome-mastodon/#clients

.. toctree::
   :maxdepth: 3

   fedilab/fedilab
   tusky/tusky
