.. index::
   pair: Mastodon client ; Tusky

.. _tusky:

======================================================================================================
**Tusky** (An Android client for the microblogging server Mastodon)
======================================================================================================

- https://github.com/tuskyapp/Tusky
- https://tusky.app/

.. toctree::
   :maxdepth: 3

   versions/versions
