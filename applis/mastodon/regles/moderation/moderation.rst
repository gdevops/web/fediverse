
.. _mastodon_regles_moderation:

==========================================================================================================
**Règles de modération**
==========================================================================================================


Il ne faut jamais débattre avec l’extrême-droite
===================================================

.. toctree::
   :maxdepth: 3

   ne_jamais_debattre_avec_l_extreme_droite/ne_jamais_debattre_avec_l_extreme_droite



https://framapiaf.org/@sebsauvage/109365056069686848 Vous prenez la modération à revers
===========================================================================================

Vous prenez la modération à revers.
Le problème n'est pas d'empêcher ces gens de parler.
Ça ne sera jamais possible. Sur Twitter ou ailleurs.

La modération, c'est qu'un utilisateur ou un admin d'instance décide
"Je ne veux pas voir ça dans ma TL".

C'est ÇA la différence entre la modération twitter et Mastodon.
Et c'est très bien comme ça.

Parce qu'on a vu que la modération à la sauce GAFAM, ça ne marche pas.
**Alors on va se protéger nous-mêmes. Entre nous**.


https://piaille.fr/@lokigwyn/109364878364133304
===================================================

- https://piaille.fr/@lokigwyn/109364878364133304

Avec l'arrivée massive de nouvelles personnes sur nos instances mastodon,
il y a aussi le risque de voir débarquer des fachos, des réac et autres bigots.

N'hésitez pas à voir avec vos admin et équipes de modération (le cas échéant)
si vous pouvez aider.

Et n'oubliez pas qu'on a également une part de responsabilité ici !

ON NE PARLE PAS, ON NE DÉBAT PAS avec les harceleurs et les militants
d'extrême-droite : on les signale et on les dégage


https://mastodon.top/@Milady_Oscar/109366587225647236
======================================================

- https://mastodon.top/@Milady_Oscar/109366587225647236

Voir un compte du RN être créé sur une instance.
Le signaler avec d'autres.
Voir qu'il n'a pas tenu plus de 14 minutes.

Mais que j'aime Mastodon 😍

https://mastodon.top/@Milady_Oscar/109366633972429583
=========================================================

- https://mastodon.top/@Milady_Oscar/109366633972429583

- Ah bah si cest ça on créera notre instance
- Ah bah faites mais on n'y sera pas.
- vous nous verrez sur Mastodon !
- Ah on t'a pas dit, les instances choisissent les instances avec
  lesquelles elles veulent être reliées ou pas. Bah pour vous c'est "ou pas"
- mais on va être qu'entre nous ?!??
- bah oui. Comme dans la vie.  Tu peux dire ce que tu veux, mais t'obligeras
  jamais quelqu'un à t'écouter et c'est ça que les gens de Twitter avaient oublié

Mais quel bonheur, putain, quel bonheur


https://mamot.fr/@p4bl0/109366588957551955
=============================================

- https://mamot.fr/@p4bl0/109366588957551955

Sur Twitter, on signalait des tweets vraiment problématiques et 3 jours
après au mieux on avait un email disant que le tweet antisémite / transphobe / raciste
(au choix non exclusif) n'enfreint pas les règles de la plateforme.

Le capitalisme de surveillance, c'est sa plateforme, c'est ses règles…

🐦 vs :mastodon:

Sur Mastodon, y a un compte RN qui a essayé de s'inscrire sur Piaille
il s'est aussitôt fait dégagé en deux temps trois mouvements.

Notre réseau, nos règles. :antifa: :antifa2: ✊🔴⚫

https://piaille.fr/@Romain_Carayol/109366239545658512
==========================================================

- https://piaille.fr/@Romain_Carayol/109366239545658512

Pour info les règles de modération de cette instance sont là : https://piaille.fr/about
Il y a des fachos qui s’installent visiblement sans les avoir lues -> blocage
