.. index::
   pair: instances ; mastodon

.. _intances_mastodon:

======================================================================================================
**instances mastodon**
======================================================================================================

- https://vis.social/api/v1/instance/peers
- https://mastodon.help/instances/fr
- https://github.com/glitch-soc/mastodon/
- https://github.com/hometown-fork/hometown
- https://joinmastodon.org/servers
- https://joinmastodon.org/fr
- https://instances.social/list#lang=fr&allowed=&prohibited=nudity_nocw,nudity_all,pornography_nocw,pornography_all,illegalContentLinks,spam,advertising,spoilers_nocw&min-users=&max-users=

.. toctree::
   :maxdepth: 3


   amicale_net/amicale_net
   babka_social/babka_social
   boitam/boitam
   chapril/chapril
   climate_justice_social/climate_justice_social
   emacsen/emacsen
   fedi_aeracode_org/fedi_aeracode_org
   fediscience/fediscience
   fosstodon/fosstodon
   framapiaf/framapiaf
   hostux_social/hostux_social
   infosec_exchange/infosec_exchange
   jasette_facil_services/jasette_facil_services
   josh_tel/josh_tel
   journa_host/journa_host
   kolektiva-social/kolektiva-social
   mamot_fr/mamot_fr
   mapstodon_space/mapstodon_space
   mas_to/mas_to
   masto_glorious_space_eu/masto_glorious_space_eu
   mastodon_cloud/mastodon_cloud
   mastodon_gougere_fr/mastodon_gougere_fr
   mastodon_design/mastodon_design
   mastodon_green/mastodon_green
   mastodon_nu/mastodon_nu
   mastodon_online/mastodon_online
   mastodon_mim_libre/mastodon_mim_libre
   mastodon_partiepirate_org/mastodon_partiepirate_org
   mastodon_social/mastodon_social
   mastodon_technology/mastodon_technology
   mastodon_zaclys/mastodon_zaclys
   mastodon_top/mastodon_top
   mastouille_fr/mastouille_fr
   mstdn_fr/mstdn_fr
   mstdn_social/mstdn_social
   mstdn_science/mstdn_science
   m_instigation_fr/m_instigation_fr
   octodon_social/octodon_social
   piaille_fr/piaille_fr
   qoto_org/qoto_org
   re_lire_im/re_lire_im
   respublicae_eu/respublicae_eu
   scholar_social/scholar_social
   simon_willison_net/simon_willison_net
   social_jacobian_org/social_jacobian_org
   social_jacklinke_com/social_jacklinke_com
   social_sciences_re/social_sciences_re
   todon/todon
   toot_aquilenet_fr/toot_aquilenet_fr
   toot_cat/toot_cat
   toot_fedilab/toot_fedilab
   vis_social/vis_social
   zaclys/zaclys
   sur_chatons/sur_chatons
