.. index::
   pair: serveur ; amicale.net

.. _serveur_amicale_net:

======================================================================================================
**https://amicale.net**
======================================================================================================

https://amicale.net/@cquest
=====================================

- https://amicale.net/@cquest
- https://amicale.net/@cquest.rss


https://amicale.net/@xkcd
============================

- https://amicale.net/@xkcd
- https://amicale.net/@xkcd.rss
