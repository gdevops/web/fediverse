.. index::
   pair: serveur ; octodon.social

.. _serveur_octodon_social:

======================================================================================================
**octodon.social**
======================================================================================================


https://octodon.social/@cwebber
=================================

- https://octodon.social/@cwebber
- https://octodon.social/@cwebber.rss

CTO at @spritelyinst. I'm here to fix the Internet.

ActivityPub co-author, co-host of @fossandcrafts. Nonbinary trans-femme,
she/they. https://dustycloud.org/


https://octodon.social/@spacekookie
============================================

- https://octodon.social/@spacekookie
- https://octodon.social/@spacekookie.rss
