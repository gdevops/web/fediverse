.. index::
   pair: serveur ; qoto.org (Freemo Antisémite)
   pair: Freemo ; Antisémite (qoto.org)

.. _serveur_qoto_org:

======================================================================================================
**qoto.org (Question Others to Teach Ourselves)** ATTENTION freemo est un antisémite
======================================================================================================

- :ref:`antisem:freemo`

.. warning:: J'apprends que freemo est un antisémite
   A éviter donc !

QOTO: Question Others to Teach Ourselves. A STEM-oriented instance
======================================================================

An inclusive free speech instance.

All cultures and opinions welcome.

Explicit hate speech and harassment strictly forbidden.

We federate with all servers: we don't block any servers ??!!.


TL;DR: Be respectful, and don't harass other Fediverse users.

Unpopular opinions are acceptable, explicit hate-speech is not. Respect
others users right to disengage. CW nudity, and spoilers.
You must be 16 years or older and a S.T.E.M. student or professional
to join our server. Membership is a privilege, not a right.

We won’t censor unpopular ideas and statements and welcome people from
across the political spectrum, but hate-based speech such as sexist,
racist, or homophobic speech will not be tolerated, be kind to each other.

All discussions between mods are public, as we believe that transparency
is the best way to show everyone how we put words into practice.
Check them out on discourse, and jump in the conversation if you feel like.

Additional services
---------------------

- QOTO Groups - https://groups.qoto.org, a server devoted to moderated
  groups on the Fediverse.
- NextCloud - https://cloud.qoto.org
- GitLab - https://git.qoto.org
- GitLab pages (including user hosted pages) - https://.qoto.io
- FunkWhale - https://audio.qoto.org
- PeerTube - https://video.qoto.org
- Discourse - https://discourse.qoto.org
- Matrix chat - https://element.qoto.org


Unique Features
===================

- Inline math Latex support - Use \ ( and \ ) for inline LaTeX, and \ [ and \ ]
  for display mode.
- 65,535 character limit for toots (usually 500)
- 65,535 character limit for profile bio (usually 160)
- Full text searches - usually you can only search hashtags and usernames
- Groups Support - Accounts which represent groups (such as those from Guppe)
  are now tagged with a group badge and are specially rendered so the messages
  relayed by a group appear to be originating from the user.
- Dedicated Groups Server - The fediverses only moderated groups server open to the public.
- Group Directory - Groups are federated in the group directory which, like the federated timeline, will list all groups followed by anyone on our server.
- Markdown Posts - You now have the option to select markdown formatting when posting.
- Circles - You can now create circles with followers in it. When you make a post you can select one or more circles to privately post to and only users of those circles can see your post.
- Per-toot option allowing for local-only toots that wont federate with other servers
- Local-only toot default - You can now optionally set all toots to be local-only and prevent all your posts from federating outside of the local server. Without this setting you can still select local-only on a per-toot basis.
- Domain Subscriptions - Bring another instance's local timeline as a feed in Qoto, no need to have an account on another server again, just import their timeline here!
- Favorite Domains - Our last release allowed us to add remote timeline as a feature where you could pull up the local timeline of a remote instance as its own column. Now you can also add domains to a favorite domain list which makes it easier to access and bring up the timelines by listing them directly in the navigation panel.
- User Post Notification - Users now have a bell next to their name where the follow button is. This will send you a notification every time they makes a new post.
- Keyword Subscriptions - Create custom timelines based off keywords not just hashtags, you can even use regex for advanced matching
- Account Subscriptions - This allows you to follow a remote accounts public toots in your Home timeline without actually following them. NOTE: This will not circumvent a user's privacy settings. It only delivers the same notifications as following someone via existing RSS feeds would allow, that is, it brings the RSS feed of their public posts into your home timeline this features behaves the same as the default functionality on non-Mastodon instances such as Misskey, Friendica, and Pleroma.
- Misskey compatible quoting of toots
- Colored follow button - The follow button seen in the feed and in profiles (the one that can be optionally turned off in settings) will now be colored to indicate follower status. Gray if not followed, yellow if followed, blue if following, green if mutual follow
- Bookmarking of toots
- Support for WebP and HEIC media uploads
- Optional instance ticker banners under usernames in the timelines that
  tell you what server a user is on and what software it is running
  (ie pleorma, mastodon, misskey, etc) at a glance.
- Optionally display subscribe and follower buttons directly in your timeline feeds.
- Remote Timeline Quickview - From any post or profile there is a link to bring up the remote timeline locally, right in QOTO without needing to open a new window.
- Professionally hosted with nightly backups
- light modern theme with full width columns (not fixed)
- Several extra themes - including mastodon default and mastodon default
  with full width columns (not fixed)
- Read/unread notifications - Unread notifications now have a subtle blue indicator. There is also a “mark all as red” button added.
- Rich-text rendering - We now render rich-text from other servers, so servers which post in a formatting that includes things like bolding, quotes, headers, italics, underlines, etc, will render in the statuses.
- Rich-text Stripping - The user option to partially or fully turn off the rendering of rich-text.
- Registration Captcha - A captcha is now required to signup for a new account, reducing spam.
- Quote feature opt-out - As introduced earlier you can quote a toot rather than just reboost it. This is an additional button on every toot. This can now be turned off in your settings and removes the button.
- Bookmark feature opt-out - As with the quote feature we could previously bookmark posts. You can now turn off the bookmark button.
- Follow button opt-out - As mentioned earlier our added feature allowing you to add a follow button, optionally colored, into the feed can now be turned off.
- Subscribe button opt-out - As before the subscribe feature that lets you watch a users public toots without following them can now be optionally turned off.
- Favorite Tags - Similar to favorite domains this allows you to create a list of your favorite tags and have them easily accessible in the navigation panel.

Message to New Members
=======================


Welcome to the QOTO Mastodon instance, its like twitter but better!

Please don't hesitate to Direct Message me ( @freemo@qoto.org ) or tag
me in Toots (what we call tweets here) if you have any questions or
even if you just want to chat.

If you are new to Mastodon you may want to check out this official getting
started guide: https://docs.joinmastodon.org/

We are a free speech, no censorship zone. Feel free to talk about whatever
you want, say whatever you want, as long as it is legal and you engage
respectfully you won’t ever get banned from the server.

With that said we do have a few rules: No spam, and no using multiple
accounts to circumvent personal bans, and a few others.

**This server isnt a typical mastodon server either**.

I have personally modified the code on this server to give it several
unique features you won't find on other servers.

If you are new to Mastodon here is some useful info.

Basically Mastodon is a decentralized service similar to twitter.

That means anyone can run a server (like QOTO) that people can sign up
with and use just like twitter. The unique part is how the servers
talk to each other.

If you want to tag someone on the same server as you then you can just
do @user however if you want to specify someone on a different server
you can do @user@server.

If someone follows you or mentions you on any server on the internet
you'll get notified right here. So my full tag is @freemo@qoto.org.

So now a bit about the timelines, there are three (unlike twitter there
is just one): Home, Local, Federated.

The Home timeline is just like what you'd expect on twitter, that is,
everyone you are following.

The local timeline is any post made by anyone on your local server.

The federated timeline is what is cool. It combines the timeline of
everyone on the server into one. This way you can see the posts of
everyone that is followed by anyone on the server (assuming the post is
public).

So this timeline is a close representation of everyone on the internet
who has Mastodon (though not really). Moreover we have a bot here that
goes around and federates with everyone it can find, so our Federated
Feed is rather robust covering thousands of instances.

Only other thing that is unique is listed vs unlisted posts. You have
normal privacy settings like on twitter but you also can set a post to
be listed or unlisted.

If it is listed it acts just as i described. If it is unlisted then it
acts just as a normal tweet would but it doesn't show up on local or
federated timelines anywhere.

Also this goes without saying, but don't be racist, sexist, hateful, and
don't harass people. It's a pretty shitty thing to do, don't be shitty!

The following is the list of QOTO administrators/moderators.

- @freemo@qoto.org
- @Khird@qoto.org
- @trinsec@qoto.org

Note: The account @QOTO@qoto.org listed as the administrator is NOT
an actively monitored account.

It was selected so as not to show favoritism among our moderator group,
we are a democracy and even our administrators and moderators are held
to the same high standards of decency as the rest of the community.

***We will never advertise on QOTO or sell your information to third-parties***


https://qoto.org/@s_gruppetta
==============================

- https://qoto.org/@s_gruppetta
- https://qoto.org/@s_gruppetta.rss

Rethinking how to teach #coding • I write about #Python on The Python
Coding Book blog and on Real Python • Former Physicist •

#programming #fedi22 #LearnToCode #LearnPython
