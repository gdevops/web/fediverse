.. index::
   pair: Tutorial ; framapiaf

.. _tuto_framapiaf:

======================================================================================================
**Framapiaf et Mastodon, Découvrir Framapiaf en 5 minutes**
======================================================================================================

- https://docs.framasoft.org/fr/mastodon/5min.html


Rappelons d'abord que Framapiaf est une "instance" du logiciel libre
Mastodon.

Et que Mastodon est un "réseau social libre et fédéré de micro-bloggage".

Hein ?

Pour résumer, Framapiaf est une alternative à Twitter, avec quelques
différences :

- les utilisateurs de Framapiaf peuvent interagir avec les utilisateurs
  et utilisatrices d'autres instances Mastodon.
  Par exemple, un utilisateur de Framapiaf.org peut échanger avec une
  utilisatrice de Mamot.fr (exactement comme un utilisateur avec un
  mail @free.fr peut écrire à quelqu'un ayant une adresse @yahoo.com) ;
- les messages ne sont pas limités à 140 caractères, mais à 500 caractères ;
- Framapiaf n'affiche pas de publicités : le site est maintenu, grâce à
  vos dons, par l'association Framasoft, dont le but est de promouvoir
  la culture libre et le logiciel libre ;
- Framapiaf ne s'intéresse pas à votre vie privée et n'exploite pas vos
  données personnelles ;
- Framapiaf est géré et modéré par des membres de Framasoft, ce qui a
  des avantages (les modérateurs sont des êtres humains, et bénévoles),
  et des inconvénients (les modérateurs sont des êtres humains, et bénévoles) ;
- Sur Framapiaf, on ne "tweete" pas, on "pouet", et rien que pour ça,
  ça vaut bien la peine de nous rejoindre !

Au cas où, voici quelques pointeurs qui pourront vous aider à en savoir
plus sur ce qu'est (et ce que n'est pas) Mastodon :

- `"9 questions pour tout comprendre au réseau Mastodon" par Numérama <http://www.numerama.com/tech/246684-debuter-sur-mastodon-9-questions-pour-tout-comprendre-au-reseau-social-decentralise.html>`_
- `"Les CHATONS s’attaquent à l’oiseau Twitter grâce à Mastodon" <https://framablog.org/2017/04/07/les-chatons-sattaquent-a-loiseau-twitter-grace-a-mastodon/>`_ sur le Framablog ;
- `"Mastodon, qu’est-ce que c’est ?" <https://pixellibre.net/2017/04/mastodon-quest-cest/>`_ par Numendil, sur Pixel Libre.

