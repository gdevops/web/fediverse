.. index::
   pair: serveur ; framapiaf

.. _serveur_framapiaf:

======================================================================================================
**https://framapiaf.org/**
======================================================================================================

- https://framapiaf.org/@FramasoftStatus
- https://docs.framasoft.org/fr/mastodon/5min.html
- https://framagit.org/framasoft/framapiaf/mastodon
- https://fediverse.space/instance/framapiaf.org


Charte de modération
=======================

.. toctree::
   :maxdepth: 3

   rules/rules


Tutorial
===========

.. toctree::
   :maxdepth: 3

   tutorial/tutorial


https://framapiaf.org/@abkgrenoble
========================================

- https://framapiaf.org/@abkgrenoble
- https://framapiaf.org/@abkgrenoble.rss

https://framapiaf.org/@bibliofab66
=====================================

- https://framapiaf.org/@bibliofab66
- https://framapiaf.org/@bibliofab66.rss


https://framapiaf.org/@framasky
==================================

- https://framapiaf.org/@framasky
- https://framapiaf.org/@framasky.rss

https://framapiaf.org/@Framasoft
===================================

- https://framapiaf.org/@Framasoft
- https://framapiaf.org/@Framasoft.rss


https://framapiaf.org/@goofy
==============================

- https://framapiaf.org/@goofy
- https://framapiaf.org/@goofy.rss

https://framapiaf.org/@grenoble
==================================

- https://framapiaf.org/@grenoble
- https://framapiaf.org/@grenoble.rss


https://framapiaf.org/@Pouhiou
=================================

- https://framapiaf.org/@Pouhiou
- https://framapiaf.org/@Pouhiou.rss

https://framapiaf.org/web/@pvergain
=====================================

- https://framapiaf.org/web/@pvergain
- https://framapiaf.org/@pvergain
- https://framapiaf.org/@pvergain.rss
- https://framapiaf.org/users/@pvergain

https://framapiaf.org/@zwindler
====================================

- https://framapiaf.org/@zwindler
- https://framapiaf.org/@zwindler.rss


https://framapiaf.org/@ZesteDeSavoir
=====================================

- https://framapiaf.org/@ZesteDeSavoir
- https://framapiaf.org/@ZesteDeSavoir.rss


https://framapiaf.org/@zem
=============================

- https://framapiaf.org/@zem
- https://framapiaf.org/@zem.rss
