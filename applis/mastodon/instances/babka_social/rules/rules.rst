.. index::
   pair: Rules ; babka.social

.. _babka_social_rules:

======================================================================================================
**babka.social rules**
======================================================================================================

- https://babka.social/about

1. No hate speech of any kind
==============================

- No hate speech of any kind

2. No spiritual bullying, or Jewish gatekeeping
================================================

- No spiritual bullying, or Jewish gatekeeping.
- No supersession or supremacy

3.No incitement of violence or promotion of violence, including violent images
=================================================================================

- No incitement of violence or promotion of violence, including violent images

4. No spread of harmful misinformation
========================================

- No spread of harmful misinformation


5. Nudity or violent media must be marked as sensitive when posting
====================================================================

- Nudity or violent media must be marked as sensitive when posting

6. No spam or unsolicited advertising
=======================================

- No spam or unsolicited advertising


7. No harassment, including dogpiling, or doxxing
===================================================

- No harassment, including dogpiling, or doxxing

8. No illegal content
========================

- No illegal content
