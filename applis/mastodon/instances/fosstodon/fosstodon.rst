.. index::
   pair: serveur ; fosstodon

.. _serveur_fosstodon:

======================================================================================================
**https://fosstodon.org**
======================================================================================================

- https://fosstodon.org


https://fosstodon.org/@brettcannon
==================================

- https://fosstodon.org/@brettcannon
- https://fosstodon.org/@brettcannon.rss


https://fosstodon.org/@django
===============================

- https://fosstodon.org/@django
- https://fosstodon.org/@django.rss

https://fosstodon.org/@gaborbernat
=========================================

- https://fosstodon.org/@gaborbernat
- https://fosstodon.org/@gaborbernat.rss


https://fosstodon.org/@gsainsbury86
=====================================

- https://fosstodon.org/@gsainsbury86
- https://fosstodon.org/@gsainsbury86.rss

https://fosstodon.org/@l_avrot
================================

- https://fosstodon.org/@l_avrot
- https://fosstodon.org/@l_avrot.rss


https://fosstodon.org/@mattbert
==================================

- https://fosstodon.org/@mattbert
- https://fosstodon.org/@mattbert.rss


https://fosstodon.org/@pandoc
==============================

- https://fosstodon.org/@pandoc
- https://fosstodon.org/@pandoc.rss


https://fosstodon.org/@pgstef
=================================

- https://fosstodon.org/@pgstef
- https://fosstodon.org/@pgstef.rss



Hi everyone! 👋

Following the good habits, here's my #introduction 😊

I'm a #PostgreSQL fan :postgresql:, living in #Belgium 🇧🇪 , interested in
backups and high-availability, contributing to @pgBackRest.
Working as Database Backup Architect for @EDBPostgres. Member of the
PostgreSQL CoC Committee.

Proud father of 2 awesome young boys. Consuming passion for #badminton 🏸



https://fosstodon.org/@ThePSF
==================================

- https://fosstodon.org/@ThePSF
- https://fosstodon.org/@ThePSF.rss

https://fosstodon.org/@paulox
=================================

- https://fosstodon.org/@paulox
- https://fosstodon.org/@paulox.rss
