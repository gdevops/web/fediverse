.. index::
   pair: serveur ; mastodon.mim-libre

.. _serveur_mim_libre:

======================================================================================================
**mastodon.mim-libre**
======================================================================================================


https://mastodon.mim-libre.fr/@lelibreedu
===========================================

- https://mastodon.mim-libre.fr/@lelibreedu
- https://mastodon.mim-libre.fr/@lelibreedu.rss

https://mastodon.mim-libre.fr/@drane_aclyon
================================================

- https://mastodon.mim-libre.fr/@drane_aclyon
- https://mastodon.mim-libre.fr/@drane_aclyon.rss


https://mastodon.mim-libre.fr/@apps
======================================

- https://mastodon.mim-libre.fr/@apps
- https://mastodon.mim-libre.fr/@apps.rss
