.. index::
   pair: serveur ; mastodon.social

.. _serveur_mastodon_social:

======================================================================================================
**mastodon.social**
======================================================================================================

- https://mastodon.social


https://mastodon.social/@ascherbaum
=======================================

- https://mastodon.social/@ascherbaum
- https://mastodon.social/@ascherbaum.rss

https://mastodon.social/@tuxom
==================================

- https://mastodon.social/@tuxom
- https://mastodon.social/@tuxom.rss


https://mastodon.social/@Gargron
==================================

- https://mastodon.social/@Gargron
- https://mastodon.social/@Gargron.rss


https://mastodon.social/@chexum
=================================

- https://mastodon.social/@chexum
- https://mastodon.social/@chexum.rss

https://mastodon.social/@dunglas
====================================

- https://mastodon.social/@dunglas
- https://mastodon.social/@dunglas.rss

https://mastodon.social/@GaelVaroquaux
=========================================

- https://mastodon.social/@GaelVaroquaux
- https://mastodon.social/@GaelVaroquaux.rss


https://mastodon.social/@hynek
================================

- https://mastodon.social/@hynek
- https://mastodon.social/@hynek.rss

https://mastodon.social/@jpargudo
====================================

- https://mastodon.social/@jpargudo
- https://mastodon.social/@jpargudo.rss

https://mastodon.social/@laurentabbal
======================================

- https://mastodon.social/@laurentabbal
- https://mastodon.social/@laurentabbal.rss

Laurent Abbal @laurentabbal@mastodon.social

Professeur de NSI / Physique-Chimie au Lycée Français International de Tokyo.

* Organisateur de la Nuit du c0de (https://www.nuitducode.net)
* Créateur de :

  - https://www.mon-oral.net
  - https://www.codepuzzle.io
  - https://www.ateliernumerique.net
  - https://www.edupyter.net
  - https://www.easyphp.org
  - https://www.txtpuzzle.net
  - https://www.dozo.app
  - https://www.portabledevapps.net

* GitHub : https://github.com/laurentabbal
* GitLab : https://forge.aeif.fr/laurentabbal


https://mastodon.social/@jsoldeville
=====================================

- https://mastodon.social/@jsoldeville
- https://mastodon.social/@jsoldeville.rss

https://mastodon.social/@NathalieRaoux
========================================

- https://mastodon.social/@NathalieRaoux
- https://mastodon.social/@NathalieRaoux.rss


https://mastodon.social/@marie_peltier
===========================================

- https://mastodon.social/@marie_peltier
- https://mastodon.social/@marie_peltier.rss


https://mastodon.social/@nicolasvivant
=========================================

- https://mastodon.social/@nicolasvivant
- https://mastodon.social/@nicolasvivant.rss


https://mastodon.social/@Sustainable2050
==============================================

- https://mastodon.social/@Sustainable2050
- https://mastodon.social/@Sustainable2050.rss

https://mastodon.social/@pixelfed
===================================

- https://mastodon.social/@pixelfed
- https://mastodon.social/@pixelfed.rss

https://mastodon.social/@theodorajewell
==========================================

- https://mastodon.social/@theodorajewell
- https://mastodon.social/@theodorajewell.rss

https://mastodon.social/@treyhunner
=======================================

- https://mastodon.social/@treyhunner
- https://mastodon.social/@treyhunner.rs


https://mastodon.social/@dansup
===================================

- https://mastodon.social/@dansup
- https://mastodon.social/@dansup.rss
