.. index::
   pair: serveur ; mas.to

.. _serveur_mas_to:

======================================================================================================
**mas.to**
======================================================================================================

- https://mas.to/about

https://mas.to/@Chanimal
==================================================

- https://mas.to/@Chanimal
- https://mas.to/@Chanimal.rss



#introduction

I’m Chanda Prescod-Weinstein, a theoretical physicist and feminist theorist
with research specialties on dark matter, neutron stars, and Black
feminist science, technology, and society studies.
I’m a columnist for New Scientist and also author of an award-winning
book The Disordered Cosmos: A Journey into Dark Matter, Spacetime, and
Dreams Deferred.

I can be found here and @chanda, and I am Vulcan at heart 🖖🏽

Interests: #BlackMastodon #startrek #Astrodon #anarchism #BLACKandSTEM

