
.. _about_piaille_fr:

======================================================================================================
**https://piaille.fr/about**
======================================================================================================

- https://piaille.fr/about

1
==

Conformément aux lois françaises et à la jurisprudence, les contenus
pédopornographiques, y compris sous forme dessinée, sont illégaux et
seront supprimés, et les comptes concernés bloqués (si distants) ou
supprimés (si local).


2
==

Les contenus pornographiques ou visiblement violents doivent être marqués
avec le drapeau "sensible / sensitive".

Les comptes ne respectant pas cette règle seront supprimés


3
===

La publicité, sous forme répétée et/ou intrusive (faisant mention d'autres
utilisateur·ices), n'est pas acceptée.

Les comptes concernés seront rendus invisibles aux flux locaux et fédérés,
ou supprimés, selon la gravité de la situation.

4
==

Approchez les personnes avec précaution et politesse.

Si une personne vous demande d'arrêter de la mentionner ou qu'elle
exprime ne plus vouloir participer à une discussion, arrêtez immédiatement.

5
==

Sont interdits les propos, images ou vidéos racistes, sexistes, homophobes
ou transphobes.

6
==

Sont interdits les avances ou propositions à caractères sexuels non-sollicitées.

7
==

Sont interdit toute conduite, en public ou en privé, ayant pour but de
traquer, harceler ou intimider d'autres personnes.

8
==

Il est interdit de rassembler, publier, disséminer les informations
personnelles d'une autre personne sans son autorisation explicite.


9
===

Il est interdit de poursuivre une conversation ou inciter une personne
à poursuivre une interaction avec une personne ayant réclamé la cessation
de ladite interaction.

Cela peut être considéré comme du harcèlement, indépendamment de la
confidentialité employée.

10
====

Est interdit toute tentative de retournement d’une oppression systémique.
Par exemple : « racisme anti-blanc », « sexisme anti-homme », « hétérophobie »,
« cisphobie ».

11
===

Il est interdit de publier ou diffuser intentionnellement des propos
diffamatoires, calomnieux, ainsi que toute désinformation avec intention
de tromper (le gorafi ou the onion sont donc autorisés).

Les profils satiriques doivent être indiqués comme tels dans leur nom et
description publique.

