.. index::
   pair: serveurs ; piaille.fr

.. _serveur_piaille_fr:

======================================================================================================
**https://piaille.fr/**
======================================================================================================


.. warning::

    Piailleuses, piailleurs sachez cette instance #mastodon  gérée par la
    société https://www.octopuce.fr/ est pour le moment gratuite.

    Quand j'ai demandé aux admins (@thibault et @benjamin) comment contribuer
    ils m'ont encouragés à faire des dons à d'autres associations libristes
    comme #framasoft par exemple.

    J'ai cru comprendre que framasoft en avait bien besoin en plus : https://framasoft.org/fr/

    Source: https://piaille.fr/@Martoni/109319699738671341

About
=========

.. toctree::
   :maxdepth: 3

   about/about


https://piaille.fr/@AggiornamentoHG
======================================

- https://piaille.fr/@AggiornamentoHG
- https://piaille.fr/@AggiornamentoHG.rss

https://piaille.fr/@benjamin
======================================

- https://piaille.fr/@benjamin
- https://piaille.fr/@benjamin.rss


https://piaille.fr/@clemrenard
=================================

- https://piaille.fr/@clemrenard
- https://piaille.fr/@clemrenard.rss

https://piaille.fr/@cmoreldarleux
======================================

- https://piaille.fr/@cmoreldarleux
- https://piaille.fr/@cmoreldarleux.rss

Il y a des pas-de-côté qui se font avec beaucoup de légèreté...
Ravie de vous retrouver ici !

Envie de littérature, d'écosocialisme et de réflexions sur le monde dit
civilisé, de refus de parvenir et de refuges de papier, de moments de
grâce et d'insouciance volés, d'autonomie politique et matérielle,
de sauvage, d'une belle dose de lucidité et de messages buissonniers.

Et toujours de respect.

Le temps de prendre mes marques et celui de nécessaires pauses
déconnectées... A vite.

#présentation

https://piaille.fr/@pr_logos
=============================

- https://piaille.fr/@pr_logos
- https://piaille.fr/@pr_logos.rss

https://piaille.fr/@marinetondelier
======================================

- https://piaille.fr/@marinetondelier
- https://piaille.fr/@marinetondelier.rss


https://piaille.fr/@MathildeLarrere
========================================

- https://piaille.fr/@MathildeLarrere
- https://piaille.fr/@MathildeLarrere.rss

https://piaille.fr/@santolini
================================

- https://piaille.fr/@santolini
- https://piaille.fr/@santolini.rss

https://piaille.fr/@thibault
===============================

- https://piaille.fr/@thibault
- https://piaille.fr/@thibault.rss


