

.. _description_mastodon:

======================================================================================================
**Description de mastodon**
======================================================================================================


https://fr.wikipedia.org/wiki/Mastodon_%28r%C3%A9seau_social%29
===================================================================

- https://fr.wikipedia.org/wiki/Mastodon_%28r%C3%A9seau_social%29

.. figure:: ../logo_mastodon.png
   :align: center


Mastodon est un réseau social et logiciel de microblog auto-hébergé, libre,
distribué et décentralisé via ActivityPub au sein du Fediverse.

Il permet de partager des messages (« pouets » ou toots dans d'autres
langues dont l'anglais), images et autres contenus.

Des instances sont publiquement mises à disposition afin de faciliter
son utilisation.

Il a été créé en octobre 2016 par l'Allemand Eugen Rochko (de),
alors âgé de 24 ans.
Par la suite, en 2022, il crée une application éponyme, un logiciel pour
mobile, client officiel de ce réseau.

Description
--------------

Mastodon peut être fédéré à un réseau d'instances capables de communiquer
entre elles.
Les comptes sont liés à une instance (@utilisateur@instance) choisie par
l'utilisateur comme pour les autres protocoles fédérés (tels que les
adresses de courriel) et les réseaux sociaux utilisant ActivityPub.

Le réseau est le plus souvent présenté dans les médias par ses différences
vis-à-vis de Twitter.
Alors que sur Twitter, les messages sont limités à 280 caractères, ils
peuvent atteindre 500 caractères sur Mastodon (ou plus selon la configuration
de l'instance).

L'absence de publicité est aussi mise en avant. Le fait de pouvoir créer
son instance du service, pouvant se connecter à l'ensemble du réseau,
permet également de garder le contrôle de ses données.

Mastodon propose deux fils d'actualités : un fil public local qui présente
les messages des utilisateurs de l'instance de l’utilisateur, un fil public
global qui présente les messages des utilisateurs de toutes les instances
fédérées.

Des journaux généralistes tels que Le Monde et Le Télégramme sont présents
sur le réseau.



https://eyssette.github.io/mindmap/mastodon
=============================================


.. figure:: images/mindmap.png
   :align: center

   https://eyssette.github.io/mindmap/mastodon

Features explained
=====================

.. toctree::
   :maxdepth: 3

   features_explained/features_explained
