.. index::
   pair: Features ; Mastodon

.. _mastodon_features:

======================================================================================================
**Mastodon features explained**
======================================================================================================

- https://joinfediverse.wiki/Mastodon_features_explained


.. figure:: ../images/chouette_explain.png
   :align: center

   Let's go through every Mastodon feature...

