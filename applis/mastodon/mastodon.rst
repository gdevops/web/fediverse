.. index::
   pair: Fediverse ; mastodon
   ! mastodon


.. figure:: logo_mastodon.png
   :align: center
   :width: 200

.. _mastodon:

======================================================================================================
**mastodon**
======================================================================================================

- https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)
- https://github.com/mastodon/documentation
- https://docs.joinmastodon.org/
- https://gofoss.net/fr/fediverse/#mastodon
- https://tutox.fr/wp-content/uploads/conferences/mastodon_conference.html


.. toctree::
   :maxdepth: 3

   people/people
   installation/installation
   introductions/introductions
   description/description
   regles/regles
   instances/instances
   clients/clients
   community/community
   tips/tips
   tools/tools
   follow_packs/follow_packs   
   tutorials/tutorials
   versions/versions
