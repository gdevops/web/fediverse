.. index::
   pair: Guide ; Louis Derrac

.. _guide_louis_derrac:

======================================================================================================
**Conseils transition Twitter vers Mastodon et le Fédivers** par Louis Derrac
======================================================================================================

- https://url.derrac.com/masto
- https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed
- https://publish.obsidian.md/louisderrac/Alternatives+num%C3%A9riques/Conseils+transition+Twitter+vers+Mastodon+et+le+F%C3%A9divers


Une grande partie de ce travail provient d'un `pad collaboratif <https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed>`_ hébergé
sur Framapad.

Il s'agit donc d'un travail collectif, que je sauvegarde (en l'éditant légèrement)
et continue d'alimenter ici.

Licence CC0
===============

- https://fr.wikipedia.org/wiki/Creative_Commons

.. figure:: images/cc_0.png
   :align: center

   https://fr.wikipedia.org/wiki/Licence_CC0

La licence CC0 (Creative Commons Zero) est une licence libre Creative
Commons permettant au titulaire de droits d’auteur de renoncer au maximum
à ceux-ci dans la limite des lois applicables, afin de placer son œuvre
au plus près des caractéristiques du domaine public.

La licence CC0 concerne tous ceux qui mettent à disposition du contenu.
Elle autorise toute personne à réutiliser librement ses travaux, les
améliorer, les modifier, quel que soit le but et sans aucune restriction
de droit, sauf celles imposées par la loi.

La licence CC0 a été lancée officiellement le 11 mars 2009 par
l’organisation `Creative Commons <https://fr.wikipedia.org/wiki/Creative_Commons>`_.


.. _comment_ca_marche_le_cw:

Comment ça marche le "CW (content warning") ?
==================================================

- :ref:`content_warning`
- https://publish.obsidian.md/louisderrac/Alternatives+num%C3%A9riques/Conseils+transition+Twitter+vers+Mastodon+et+le+F%C3%A9divers#Comment+%C3%A7a+marche+le+CW+content+warning


Sur Twitter, il est d'usage de commencer son post par la mention
«TW: Raison pour laquelle je fais un Trigger Warning», puis de mettre
la suite de son message.

Il est possible depuis plusieurs mois de définir ses médias comme
« Sensibles » (ainsi que « Violence » et « Nudité »).

**Sur Mastodon, il existe une fonctionnalité appelée Content Warning (CW)**.

C'est le 4° bouton sous la fenêtre d'édition des pouets. Si vous cliquez
dessus, une ligne apparait au dessus de votre message.

Dans cette ligne, vous indiquerez les raisons pour lesquelles vous
considérez votre message sensible (ou pertinent de masquer sous un
"avertissement", sans forcément que cela soit "sensible" même si
c'est l'intérêt principal).

Quand les autres utilisateur·ices de la fédiverse tomberont sur votre pouet,
iels ne verront que cette ligne, accompagné d'un bouton «Déplier».

Il leur faudra cliquer sur ce bouton pour afficher le contenu du pouet.

Ne censurez pas les mots dans votre ligne de CW. Par exemple si vous
voulez signaler «Attention, banane», n'écrivez pas «bnne» mais «banane»,
car certain·es utilisateur·ices utilisent des fonctions de filtre afin
de ne pas être exposé·es aux mentions «banane».

Si vous essayer de masquer le mot, vous les exposez à votre pouet,
tout en perdant en clarté. Les images peuvent également être masquées
de la même manière avec la case à cocher «Contenu sensible».

Il n'y a pas de "règle" universelle des sujets à mettre en CW.

Votre instance peut déterminer dans ses règles de fonctionnement une liste
de sujet à mettre (préférentiellement) sous CW.

Au delà de cela, on rentre dans «les us et les coutumes» d'usages dans
les communautés dans lesquelles vous êtes et avec lesquelles vous
interragissez (ou êtes visible). Comme dans tout groupe, il y a des
règles de fonctionnement explicites et implicites, et des comportements
« qu'il est de bon ton » d'adopter. Il en va de même pour les CW :)

Par exemple, un fil à ce sujet recensant les sujets qu'il est d'usage
assez courant de mettre sous CW.

Comme toute règle de fonctionnement de communauté, elle est sujette à
débats, à des évolutions et à des positionnements divers selon avec
qui on parle.

Vous êtes libre de suivre ces façons de fonctionner en société (qui comme
"dans la vraie vie" ne sont pas les mêmes partout) ou non.

Les autres sont également libres de considérer que vous n'avez pas un
mode de fonctionnement qui leur convient, et de ne pas interragir avec
vous (notamment de vous bloquer/masquer).

.. note:: La fonctionnalité CW de Mastodon est un détournement de l’attribut
   "Titre" d’un pouet. Les autres logiciels ne l’interprètent pas forcément
   de la même manière.

   Par exemple, le comportement par défaut de Pleroma est d’afficher le
   titre au dessus du message, sans bloc à dérouler.

