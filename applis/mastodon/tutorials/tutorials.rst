.. index::
   pair: tutorials ; mastodon

.. _tutorials_mastodon:

======================================================================================================
**Tutorials mastodon**
======================================================================================================

- https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed
- https://github.com/joyeusenoelle/GuideToMastodon
- https://tuxicoman.jesuislibre.net/2022/01/tutoriel-mastodon.html


.. toctree::
   :maxdepth: 3

   mastodon-documentation/mastodon-documentation
   clement_renard/clement_renard
   fedizine/fedizine
   getting_started/getting_started
   intro_mastodon_gutierez/intro_mastodon_gutierez
   guide_louis_derrac/guide_louis_derrac
   saimyx/saymix
   un_guide_rapide/un_guide_rapide
   what_i_know_about_mastodon/what_i_know_about_mastodon
   wordsmith/wordsmith
   xkr47/xkr47
