.. index::
   pair: tutorial mastodon ; Saimyx

.. _tutorial_saimyx:

======================================================================================================
**Guide de découverte de Mastodon (et du Fédivers)** par Saimyx
======================================================================================================

- https://wiki.saty.re/mastodon-decouverte

Le contenu de ce document est un travail collectif partagé à l'origine
sous sous licence Creative Commons 0 que je repartage sous ma licence
habituelle la BY-NC-SA (voir en bas de page).

Merci à toutes les personnes y contribuant !

Le super résumé que si t'as que 2 min à y consacrer

Mastodon est un logiciel de réseau social, parmi d'autres, au sein du
Fédiverse.
Ces logiciels peuvent s'installer sur un serveur (un ordinateur quelque
part sur le réseau), on parle alors d'instance.

Les différentes instances communiquent entre elles et forment un réseau : le fédivers.

Voir cette vidéo de présentation du Fédivers (EN avec sous titres français) : https://framatube.org/w/4294a720-f263-4ea4-9392-cf9cea4d5277
