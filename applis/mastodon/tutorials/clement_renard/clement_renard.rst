.. index::
   pair: tutorial mastodon ; Clément Renard

.. _tutorial_clement_renard:

======================================================================================================
**Fediverse : une Révolution politico-numérique en marche** par Clément Renard
======================================================================================================

- https://nitter.hu/clemnaturel/status/1589175354115362816


Migrer sur #Mastodon, c’est reprendre collectivement le contrôle sur nos
données personnelles et sur internet. C’est œuvrer pour un monde libertaire,
égalitaire et solidaire.

Les grandes plateformes capitalistes d’aujourd’hui (comme les Gafam)
recréent artificiellement un modèle de réseau centralisé par-dessus
le web pour se rendre indispensables, forcer les échanges à passer par
elles, et ainsi en tirer profit.


À l’inverse de l’architecture de ces plateformes, il existe une architecture
totalement décentralisée où chaque personne héberge ses données sur son
ordinateur et dispose d’un logiciel pour se connecter au réseau correspondant.
C’est le principe des réseaux P2P (comme BitTorrent)

En opposition à ces deux modèles se sont développées des plateformes
acentrées, basées sur des logiciels libres non-commerciaux tels que
Mastodon et PeerTube (respectivement des alternatives à Twitter et YouTube)
joinpeertube.org/fr_FR/

our faciliter l’accessibilité, le choix retenu a été celui d’une architecture
fédérée où existent une multitude de serveurs (ou instances) à but
non-lucratif et reliés entre eux, qui permettent d’héberger les données
personnelles et d’accéder au réseau via une simple inscription

C’est en quelque sorte le modèle des communes insurrectionnelles de 1871
appliqué à Internet. Le communalisme instaurait une nouvelle organisation
de la République française basée sur la démocratie directe et le fédéralisme.
https://www.revue-ballast.fr/la-commune-et-ses-usages-libertaires/

C’est un modèle également très proche du municipalisme de Murray Bookchin
https://www.revue-ballast.fr/le-moment-communaliste/

Comme le logiciel est libre, n’importe qui disposant des connaissances pour
le faire peut créer une instance et ainsi permettre aux citoyens•nes de
se connecter au réseau et d’échanger avec l’ensemble des utilisateurs•trices de Mastodon.

Avec le modèle fédéré, aucune instance ne peut se rendre indispensable
et contrôler le réseau à elle seule tant qu’il n’y a pas d’instance
incontournable car devenue trop grosse (en terme de nombre d’utilisateur•trices).

Les instances ayant atteint un grand nombre d’utilisateurs, comme mastodon.social,
framapiaf.org ou marmot.fr, ont tendance à fermer leurs inscriptions,
tandis que d’autres instances ouvrent au fur et à mesure que de nouveaux
utilisateurs arrivent.

Le modèle non marchand de ces plateformes alternatives ne les incite pas à
rendre captifs leurs utilisateur et utilisatrices. Au contraire, gérer
une énorme instance est particulièrement couteux, et sans source de revenu
autre que des dons, elles sont incitées à rester petit

Le choix d’une instance détermine l’identifiant complet (sous la forme @nom@instance),
les règles auxquelles vous êtes soumis, et les messages qui apparaissent
sur le fil local (il y aussi un fil perso (vos abonnés) et un fil global
(utilisateurs de toutes les instances))

Votre instance peut choisir avc qui elle se fédère et peut bannir des
instances qui ne conviennent pas au groupe. D’où l’importance de bien
choisir une instance adaptée à ses idéaux et à ses intérêts, ce qui permet
d’évoluer dans un environnement à la fois ouvert et sécurisé.

#Mastodon

- :ref:`Tuto <guide_louis_derrac>` s’inscrire (autogéré) https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed,
- Tuto s’inscrire https://www.blog-nouvelles-technologies.fr/244863/comment-utiliser-mastodon-une-alternative-twitter/
- Tuto changer d’instance https://fediverse.blog/~/BlogDHeyla/Changer%20d'instance%20Mastodon/


Quelques instances francophones ouvertes aux inscriptions :

- piaille.fr (par @vincib)
- social.sciences.re (universitaires)
- eldritch.cafe (anarchiste)
- pipou.academy (queer)
- mastodon.mim-libre.fr (Agents de l’Etat)


A l’intérieur de Mastodon, les instances sont donc reliées entre elles.

De plus, contrairement aux logiciels des Gafam, Mastodon est relié à
d’autres logiciels libres : il est par exemple possible de suivre une
chaine PeerTube depuis un compte Mastodon.

La galaxie d’instances fédérées et l’univers des logiciels libres
intercompatibles est appelée le fédiverse (de l’anglais fediverse, mot-valise
pour federated universe). fediverse.party


Dans le fediverse, il n’y a ni pub, ni recommandations dictées par des
algorithmes, ni business avec vos données personnelles. On y trouve :

- Mastodon (alternative à Twitter)
- Peertube (alternative à YouTube)
- Pixelfed (alternative à Instagram)
- Diaspora (alternative à Facebook)

Les logiciels libres sont conçus pour défendre le libre accès à la connaissance,
le respect de la vie privée ainsi que la neutralité et la diversité du web.
Ils court-circuitent le penchant naturel du Capital et de l’État à profiter,
à contrôler et à valoriser le fascisme

Sources
========

- https://www.unioncommunistelibertaire.org/?Reseau-Comprendre-le-fediverse
- https://basta.media/Mastodon-Diaspora-PeerTube-Qwant-framasoft-logiciels-libres-open-street-map-alternatives-aux-Gafam

