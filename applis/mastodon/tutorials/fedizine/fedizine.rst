.. index::
   pair: tutorial mastodon ; fedizine

.. _tutorial_fedizine:

======================================================================================================
**An anarchist introduction to federated social media**
======================================================================================================

- https://distro.f-91w.club/fedizine/


TLDR
====

For better or worse, anarchists are using social media like Twitter,
Instagram, and Facebook.

This sucks. Enter the Fediverse, an alternative, open-source social
media network that aligns with anarchist values.

Rather than being a thinly-veiled attention and data gathering capitalist
vortex, the Fediverse is an actual social network, built out of a multitude
of federated, autonomous, and decentralized instances.

On the Fediverse, we control the infrastructure, we moderate ourselves,
and we can gather and share based on our affinities and desires rather
than being guided by addictive algorithms.

Many anarchists who have dodged the traps of corporate social media are
already here, sharing their projects, art, and ideas. Join us!

