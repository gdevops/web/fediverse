
.. _everything_i_know_mastodon:

======================================================================================================
**Everything I know about Mastodon** Danielle Navarro 2022-11-03
======================================================================================================

- https://blog.djnavarro.net/posts/2022-11-03_what-i-know-about-mastodon/
- https://publish.obsidian.md/louisderrac/Alternatives+num%C3%A9riques/Conseils+transition+Twitter+vers+Mastodon+et+le+F%C3%A9divers


.. _why_are_cw_everywhere:

Why are content warnings (CW) everywhere ?
============================================

- :ref:`content_warning`
- https://blog.djnavarro.net/posts/2022-11-03_what-i-know-about-mastodon/#why-are-content-warnings-everywhere

One huge – and hugely important – difference between twitter and mastodon
is that mastodon has a system that allows users to mask their posts behind
content warnings.

Now… if you’re coming from twitter you might be thinking “oh that doesn’t
apply to me I don’t post offensive content”.

If that’s what you’re thinking, allow me to disabuse you of that notion
quickly. Content warnings are not about “hiding offensive content”, they
are about being kind to your audience.

This `thread by Mike McHargue <https://robot.rodeo/@mike/109270985467672999>`_ is a very good summary.
The whole thread is  good, but I’ll quote the first part here:

    If you’re part of the #twittermigration, it may seem strange the people
    use CWs so much here. But, CWs are really helpful. So much of our
    world is overwhelming, and feed presentation can bombard our nervous
    systems with triggers.

    CWs give people time and space to engage with that they have the resources
    to engage with. It gives them agency.
    I follow news and politics AND it’s helpful for my PTSD to have the
    chance to take a deep breath before I see a post.

If you’re posting about politics, that should be hidden behind a content warning.

If you’re posting about sexual assault, definitely use a content warning.

If you’re posting about trans rights, again put it behind a content warning.

You should use the content warning even – or perhaps especially – when
you think your post is this is an important social justice issue that
other people need to see, because there is a really good chance that
people whose lives are affected by it will be part of the audience… and
yes, some of us have PTSD.

Content warning: trans rights, sexual assault

So if you’re thinking about posting about these topics, the question of
“should I attach a content warning?” isn’t a matter of “is this important?”
it’s a matter of “could I be causing distress to people?”

When you answer that question, don’t think about the typical case, think
about that 1% of people who might be most severely affected and the reasons why.

Please, please, please… take content warnings seriously. Even if you’re
“just posting about politics” or “venting some feelings”.

**It’s a kindness and courtesy to your audience.**

**Mastodon isn’t twitter**.
