.. index::
   pair: tutorial mastodon ; Grégory Gutierez
   ! Grégory Gutierez

.. _intro_mastodon_gregory:

======================================================================================================
**Savoir parler le Mastodon** par Grégory Gutierez (@Greguti@pouet.chapril.org)
======================================================================================================

- https://gregorygutierez.com/doku.php/linux/intro-mastodon
- https://pouet.chapril.org/@Greguti


Introduction
=============

Depuis quelques mois que j'utilise désormais quotidiennement Mastodon,
je commence à m'y sentir à l'aise avec ses pratiques et son vocabulaire
spécifiques. Voici donc un petit lexique pour s'y retrouver.

Les instances
================

Un serveur sur lequel est installé Mastodon, avec sa propre communauté
de comptes créés (le “fil local”).

Les instances trop “grosses” sont assez mal vues car difficiles à
administrer au quotidien.

Une “grosse” instance (jusqu'à 80 000 comptes) ne tardera pas à bloquer
les créations de nouveau compte et son équipe pourra décider de créer
d'autres instances si elle en a la capacité.

Ou bien encourager les nouveaux venus à créer leur compte sur une
instance plus jeune (par exemple piaille.fr est une instance récente sur
laquelle beaucoup d'écologistes ont créé leur compte Mastodon à partir
de fin octobre 2022).


Le Fédiverse
================

La fédération des instances Mastodon qui communiquent entre elles
(environ 2 500 à ce jour, on peut retrouver les principales sur https://joinmastodon.org).

L'activité commune de toutes ces instances fédérées compose le “fil global”
que vous voyez dans votre compte (et dans la ou les langues que votre
compte accepte de voir).

Début novembre 2022, on compte un peu plus de 1 000 000 de comptes
créés dont environ 280 000 comptes actifs.

Le Pouet
============

Un post sur Mastodon, l'équivalent d'un “tweet” de l'oiseau bleu - donc
on va “pouéter” sur Mastodon…

En anglais on dira un “toot” et donc les usagers de Mastodon deviennent
des “tooters” (des “pouéteurs”, en français ? mmm…).

Par défaut un pouet comprendra 500 signes, c'est nettement plus
confortable que 280 :)

Mais une instance Mastodon peut proposer encore plus de signes par pouet,
si elle le juge utile, et ses “longs” pouets pourront ensuite se retrouver
dans votre fil global (ou local, si vous vous êtes inscrit via l'instance
en question).

Boost, booster
===================

Le fait de partager un pouet. Un pouet très boosté sera plus vu sur le
fil local ou global… Mais on ne peut pas “partager en commentant”,
contrairement à l'habitude prise chez l'oiseau bleu.

C'est voulu : le but est de ne pas favoriser les réactions épidermiques
en un clic ni les phénomènes de harcèlement en meute.

Si vous souhaitez réagir à un pouet, vous devez participer à la discussion
sous ce pouet… ainsi la discussion reste organisée avec au départ le
pouet d'origine.

Ou bien vous pouvez choisir de publier votre propre pouet sur le sujet
abordé, en prenant la peine de copier le lien vers le pouet qui vous
à faire réagir.

Puisqu'il y a 500 signes, c'est plus facile et ça n'a finalement rien
de compliqué… mais certes, cela oblige à faire un (petit) effort de
rédaction, plutôt que de réagir dans l'instant.

Favori
=========

L'équivalent du “like”, mais visible seulement de vous-même, de la personne à l'origine du pouet et des personnes qui vous suivent… Du coup c'est moins la foire à la popularité. On “favorise” un pouet au lieu de “l'aimer” ('like“), la différence est subtile mais pertinente… ! Les pouets avec beaucoup de favoris et/ou beaucoup boostés apparaitront dans la partie Explorer de votre compte Mastodon, même quand ils proviennent de comptes que vous ne suivez pas (encore).

Marque-page
=============

Le pouet est conservé uniquement pour vous, afin de le retrouver facilement.
L'équivalent de “ajouter aux signets” chez l'oiseau bleu. ça peut être
pratique pour garder à portée de doigt un pouet à partager plus tard ou
en dehors du réseau social Mastodon.

Recherche et mots-dièses (hashtags)
=======================================

Sur Mastodon, le module de recherche intégré ne permet pas la recherche
par mot-clé, mais elle permet de retrouver des personnes (quelle que soit
leur instance) ou des mots-dièses (oui je sais, le mot hashtag existe
aussi, mais je trouve moins joli… what you gonna do about it?).

D'où l'importance d'intégrer des mot-dièses dans vos propres pouets
ainsi que dans votre bio.

Quelques mots-dièses populaires (ou qui mériteraient de l'être !) :

- #introduction ou #introductionfr : il est d'usage, après avoir créé
  son compte sur le réseau, de consacrer quelques lignes à se présenter.
  C'est l'occasion d'y glisser les mots-dièses de vos sujets d'intérêt
  principaux et de trouver des ami·es sur le réseau, qui ne tarderont
  pas à vous “favoriser” voire à vous “booster” ;
- #twittermigration : pour expliquer pourquoi comment on quitte le réseau
  Twitter et poser des questions pendant ses premiers pas sur Mastodon ;
- #VendrediLecture : chaque vendredi les mastonautes qui le souhaitent
  sont invités à parler de leur lecture du moment ;
- #MastoArt ou #MastoArts : les artistes et artisans qui exposent leurs
  productions sur le réseau ;
- #MastoGreensFr ou #TeamEELV : pour les écolos qui veulent se retrouver
  sur le réseau :)


Quelques principes et choses à savoir sur Mastodon
====================================================

Les filtres
-------------

Capture d'écran : un clic sur les options d'un pouet permet de bloquer
le compte éméteur ou même toute l'instance dont il est issu !

Il y a de nombreuses occasions de filtrer son flux Mastodon.

Cela peut dérouter au début mais c'est très pratique pour “tailler”
le poil de son mammouth à sa convenance.

Dans les Notifications, on peut déjà décider quelles alertes on souhaite
recevoir (toutes les alertes, les gens qui s'abonnent, les favoris,
les boosts, les résultats des sondages auxquels on a participé, les
mentions de son propre compte dans des pouets des autres, etc.).

Je vous conseille d'ailleurs de décocher les favoris pour avoir des
alertes un peu moins fréquentes… et donc un peu plus de sérénité.

Une fonction spécifique au pachyderme et à son fonctionnement en instances
fédérées : vous pouvez décider de bloquer un compte particulier bien
entendu, mais aussi toute une instance (voir capture d'écran ci-joint).

Des algorithmes ?
------------------

Il n'y a pas d'algorithme sur Mastodon, les timelimes sont strictement
cela : des pages mises à jour de façon chronologique (les plus récents en premier).

Les pouets les plus boostés et/ou ceux étant le plus favorisés seront
les plus vus à un moment donné. Un pouet qui continuera à être boosté
longtemps après sa publication restera visible dans les timelimes
(parce que justement partagé par de nouveaux comptes à ce moment-là)
et sera aussi visible dans la partie Explorer de votre Mastodon.

Modération
--------------

Chaque instance dispose de ses propres règles de modération et de sa
propre équipe de modération qui a toute latitude pour supprimer
éventuellement des pouets jugés inconvenants sur cette instance particulière.

Le fondateur de Mastodon, un jeune ingénieur allemand (29 ans en 2022),
expliquait récemment au Time que : « Tout serveur qui souhaite qu’on lui
offre de la visibilité doit accepter un certain nombre de règles de base,
à savoir qu’aucun discours de haine n’est autorisé, ni le sexisme, ni le
racisme, ni l’homophobie, ni la transphobie. »

Sur l'instance https://pouet.chapril.org par exemple, les CGU de
l'association Chapril précisent, entre autres clauses :

- vous devez respecter la loi (que celle-ci soit bien faite ou idiote),
  sinon votre contenu, voire votre compte, seront supprimés ;
- vous devez respecter les autres personnes qui utilisent les services
  en faisant preuve de civisme et de politesse, sinon votre contenu,
  voire votre compte, pourront être supprimés ;
- si vous abusez du service, par exemple en monopolisant des ressources
  machines partagées ou en publiant des contenus qui nous posent problème,
  vos contenus, voire votre compte, pourront être supprimés sans
  avertissement ni négociation.


Un réseau social inclusif
-------------------------------

Capture d'écran : choisir sa langue au moment de poster un pouet sur
MastodonUne dernière chose, Mastodon se veut un réseau social inclusif,
et de ce fait quelques bonnes pratiques y sont encouragées.

On prendra soin en particulier de préciser :

- Dans quelle langue on publie. Pour ma part j'y publie en français et
  c'est la langue précisée par défaut.
  Mais parfois je “pouette” aussi en anglais et dans ce cas je prends
  soin de le préciser dans la configuration ;
- De saisir un texte descriptif quand on ajoute des images à son pouet,
  afin de faciliter l'expérience des personnes mal-voyantes qui utilisent
  le réseau ;
- D'utiliser l'option CW (content warning) si le contenu du pouet est
  sensible, c'est-à-dire s'il fait état de, ou montre, un contenu explicite.

  Le fait qu'un contenu soit effectivement explicite est soumis à débat
  dans certaines conditions.

  Par exemple des usagers de Mastodon considèrent qu'émettre une opinion
  politique, c'est déjà une raison pour mettre son pouet derrière un CW…

  Pour ma part, je n'en suis pas vraiment convaincu.




