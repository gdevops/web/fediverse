.. index::
   pair: Mastodon ; documentation

.. _mastodn_documentation:

======================================================================================================
**Mastodon documentation** 
======================================================================================================

- https://docs.joinmastodon.org/
- https://github.com/mastodon/documentation

