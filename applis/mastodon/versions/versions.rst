
.. _mastodon_versions:

======================================================================================================
**mastodon versions**
======================================================================================================

- https://github.com/mastodon/mastodon
- https://github.com/mastodon/mastodon/graphs/contributors

.. toctree::
   :maxdepth: 3

   4.3.0/4.3.0
   4.2.0/4.2.0
   4.0.0/4.0.0
   3.5.3/3.5.3
