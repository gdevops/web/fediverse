
.. _sword_tenir_charge:

======================================================================================================
**Faire tenir la charge à une instance mastodon malgré Musk** par  @alarig@hostux.social
======================================================================================================

Description
================

Si vous lisez ce blog, vous savez sûrement qu’un riche mégalomane étasunien
a racheté twitter, et qu’on en attendait pas moins de son mode de gestion.

Cela a entraîné un exode vers mastodon, qui ronronnait tranquillement
dans son coin jusqu’ici.

Je vais ici partager les différentes actions que j’ai dû entreprendre
ces derniers jours afin de maintenir hostux.social à flots.

Avant toutes choses, il convient de remercier `Leonora Tindall <https://nora.codes/post/scaling-mastodon-in-the-face-of-an-exodus/>`_ d’avoir
écrit Scaling Mastodon in the Face of an Exodus, car cet article m’a
beaucoup aidé, pour la configuration sidekiq notamment.


Afin de contextualiser ce billet, hostux.social hébergeait environ
4000 personnes (pour moins de 1000 actives) avant la vague d’arrivées.

Le tout tenait sur une VM avec 8 vCPU et 12 Go de RAM.

Je précise également que gérer des serveurs n’est pas mon métier,
je suis dans le réseau à la base. Alors même si ça reste « de l’informatique »,
ce n’est pas quelque chose avec lequel je suis forcément à l’aise, ni
qui me fait particulièrement plaisir.

(Proposer une alternative libre et décentralisée, même à mon échelle,
ça me fait plaisir, par contre.)

La première action afin de gérer l’afflux de personnes a simplement été
de fermer les inscriptions, et ce pour une raison totalement non technique,
mais par manque de temps pour traiter toutes les demandes.

Nous réouvrions les vannes de temps en temps, quand la vague précédente
était passée.

