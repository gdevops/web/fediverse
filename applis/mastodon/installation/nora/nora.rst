

.. _mastodon_installation_nora:

======================================================================================================
**Scaling Mastodon in the Face of an Exodus**  par Leonora Tindall
======================================================================================================

- https://nora.codes/post/scaling-mastodon-in-the-face-of-an-exodus/


TL;DR
========

Mastodon’s Sidekiq deferred execution jobs are the limiting factor for
scaling federated traffic in a single-server or small cluster deployment.

Sidekiq performance scales poorly under a single process model, and can
be limited by database performance in a deployment of the default
Dockerized configuration.

If you are an embattled instance admin, go `here <https://nora.codes/post/scaling-mastodon-in-the-face-of-an-exodus/#lessons-learned>`_

I recently moved to a well-established Mastodon (Hometown fork) server
called weirder.earth, a wonderful community with several very dedicated
moderators and longstanding links to other respected servers in the
fediverse.

Overall, it’s been lovely; the community is great, the Hometown fork
has some nice ease-of-use features, and the moderators tend to squash
spam and bigotry pretty quickly.

Then Elon Musk bought Twitter.

The Setup
=============

Just as tens of thousands of people1 fled Twitter to join the fediverse,
Weirder Earth had a storage migration pending.

It did not go well, for reasons unrelated to this post, and the instance
was down for several hours.

Normally, this would not be a problem; ActivityPub, the protocol that
powers Mastodon and other fediverse software, is very resilient to outages,
and most implementations have plenty of built-in mechanisms like exponential
backoff and so forth to get their neighbors up to date when they come
back online.

This particular outage, however, coincided with a huge amount of traffic,
so when the server did come back online, there were many tens of thousands
of messages waiting for it. Each of these messages necessitates a Sidekiq
task which integrates it into the database, users’ timelines and
notification lists, and so forth.

Sidekiq did not succeed in handling this load.



The Solution
==================

Ultimately, the real solution as a combination of three things:

- Increasing the memory available to the database for shared buffers
- Increasing the number of threads available to Sidekiq (and, accordingly,
  the database connection pool size and maximum number of connections
  via DB_POOL and max_connections)
- Splitting Sidekiq’s queues off into their own processes.

See, Mastodon uses 5 Sidekiq queues: default, push, pull, mailers, and
scheduler.

For us, push was conquered early on, and mailers and scheduler were never
an issue, having very low volume. pull, for getting incoming federation
events, and default, for building notifications and timelines, were the
real trouble.

So, we split them into their own Sidekiq processes.

We had gone up to 200 max_connections and a concurrency of 150, so I
guesstimated that the following split would work well:

- a single container/process for the push, mailers, and scheduler queues with 25 threads
- a container/process each for pull and default with 60 threads each

This worked great for pull. We went from holding steady to dropping about
500 events every minute, and rapidly cleared the entire pull queue – but
the default queue was still holding on. 4

This was harder to diagnose, but eventually we realized that the Sidekiq
instance working on the default queue wasn’t at its full potential.

Looking at the PgHero connection status dashboard, we could see that many
of our 200 connections were held by inactive or low-activity Sidekiq
processes with names like sidekiq 6.2.1 mastodon [0 of 25 busy] while
the active ones were only holding a few connections.

But that should be okay, right? 60 + 60 + 25 is much less than 200, so
there should be plenty of connections to go around.

As it turned out, Puma was also configured to use lots of database
connections (MAX_THREADS = 150), so it was hogging many that it didn’t
need.

We sorted that out, refined our Postgres config to allow even more
connections, split the Sidekiq processes up even more, with fallbacks
and various different ordered combinations of queues.

We all went to sleep and woke up to an empty queue!

We’ve been experimenting and optimizing since, but the server has been
coping with the load handily since.


...

Into the Future
===================

These numbers may not work perfectly for you, and you may have to tweak
things as workloads change.

There is more work left to do, and we’ll only know if this is successful
once the next big wave hits, but it’s definitely better than it was
before.

We also have not figured out how to monitor the instance for future events,
though the top candidate is, of course, Prometheus and Grafana.

Good luck. May your timelines run smoothly and your processes never be
OOM killed.



