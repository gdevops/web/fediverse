
.. _mastodon_big_versus_small:

======================================================================================================
**Big versus small instances**
======================================================================================================



https://lgbtqia.space/@mia/109365864493517783, Eugen wants to have big instances, not many small ones
=========================================================================================================

Eugen wants to have big instances, not many small ones.

Thats why only 2 Instances are available for "all regions" in the LGBT
section on joinmastodon.

Crumbling under the load of 10000 new users a day.

Let's see how this will play out...


https://berk.es/2022/11/08/fediverse-inefficiencies/
=====================================================

- https://berk.es/2022/11/08/fediverse-inefficiencies/


Let's address the mammoth in the room: the fediverse, the network of
mastodon servers, is very inefficient.

In this post I'll show why it is inefficient and why that isn't a problem.

A great analogy to explain this with is growing food
