.. index::
   pair: Mastodon ; Installation

.. _mastodon_installation:

======================================================================================================
**Installation de mastodon**
======================================================================================================

.. toctree::
   :maxdepth: 3

   big_versus_small/big_versus_small
   nora/nora
   rixx/rixx
   simon_willison/simon_willison
   tenir_la_charge/tenir_la_charge
   ubuntu/ubuntu
