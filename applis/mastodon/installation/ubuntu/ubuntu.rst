

.. _mastodon_installation_fossa:

=================================================================================================================
**Comment installer une communauté Mastodon sur une instance virtuelle fonctionnant sur Ubuntu Focal Fossa**
=================================================================================================================

- https://blog.scaleway.com/fr/comment-installer-une-communaute-mastodon-sur-une-instance-virtuelle-fonctionnant-sur-ubuntu-focal-fossa/

Mastodon est une application de gestion communautaire entièrement gratuite
et libre sur vos instances virtuelles Scaleway Elements.

L'outil permet de créer un réseau social privé, sécurisé, et chiffré
permettant de partager des photos, des messages et bien plus encore,
sans mobiliser de prestataire externe.

À l’époque où le Projet “Ensemble”, en place depuis le début du Covid,
nous a permis de collaborer étroitement avec le gouvernement dans le
cadre de leurs offres de télétravail, nous nous sommes rendu compte à
quel point Mastodon gagnait en traction auprès de la communauté libriste
française.

Mastodon est un logiciel populaire, libre et open-source, qui vous
permet d'héberger votre réseau social sur vos instances virtuelles.

Libre à vous de configurer votre propre code de conduite, vos propres
conditions de service et vos propres politiques de modération.

L’outil offre la possibilité d'utiliser le service Object Storage
compatible S3 pour stocker les contenus multimédia téléchargés sur
la plateforme, ce qui la rend flexible et évolutive.

Les instances Mastodon sont connectées en tant que réseau social fédéré
en utilisant le protocol standardisé ActivityPub, sans serveur central.
Cela permet aux utilisateurs de différents réseaux et services d'interagir entre eux.

Comme il n'y a pas de serveur central, un utilisateur peut choisir de
rejoindre ou de quitter un serveur Mastodon spécifique, selon la politique
du serveur, sans pour autant quitter le réseau social Mastodon.

