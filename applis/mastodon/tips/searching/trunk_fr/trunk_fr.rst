
.. index::
   pair: Searching ; Trunk Fr

.. _trunkfr:

==========================
Trunk Fr
==========================

- https://trunk.whidou.fr/
- https://github.com/lots-of-things/pytrunk

Trunk vous permet de découvrir plein de personnes à suivre pour débuter
sur le Fédiverse, le plus grand réseau social décentralisé.

En cliquant sur un des liens ci-dessous, vous pourrez consulter une liste
de personnes écrivant régulièrement au sujet de ce centre d'intérêt.

J'insiste sur personnes car le but est ici de faire entrer les nouveaux
venus en contact avec des personnes.
La seul exception est la liste d'organisations. Toutes les autres listes
concernent des pesonnes.

Cliquez pour voir leur flux et, si vous le souhaitez, les suivre.
J'espère que cela vous aidera à trouver de nouvelles personnes à suivre.

Si vous souhaitez suivre un grand nombre de comptes d'un coup, jetez un
œil à pytrunk, un outil écrit par @will qui utilise l'API de Trunk pour
présenter une interface simple vous laissant décider si vous souhaitez
suivre ou non le compte qui s'affiche dans votre navigateur.

Ce petit programme permet de vérifier rapidement quels sont les comptes
postant régulièrement, quels sont leurs sujets de prédilection…

Vous pouvez filtrer par nombre d'abonnés et tout plein d'autres choses !

