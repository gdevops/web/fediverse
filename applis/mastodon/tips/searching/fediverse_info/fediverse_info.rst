.. index::
   pair: Searching ; Fediverse.info

.. _fediverse_info:

==========================
fediverse.info
==========================


- https://fediverse.info/explore/people


A human-curated small selection of interesting accounts to help you get
started, or just to spice up your timeline.


Can’t find what you want? Fedi.Directory generally lists public accounts
about specific topics.

If you’re looking for more personal accounts that cover a wider range of
topics, try the excellent Trunk ⧉ and Fediverse.info ⧉ instead!
