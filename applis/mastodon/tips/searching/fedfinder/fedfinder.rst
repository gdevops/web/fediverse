.. index::
   pair: Searching ; Fedfinder

.. _fedfinder:

==========================
fedfinder
==========================


- https://fedifinder.glitch.me/
- https://fedifinder-backup.glitch.me
- https://fedifinder-backup2.glitch.me
- https://glitch.com/edit/#!/fedifinder


Extract the fediverse handles of your Twitter followings or list members
and import them on Mastodon to follow them all at once.


Made by @Luca@vis.social.
Source code in the fish menu (top right) and on `Github <https://github.com/lucahammer/fedifinder>`_.
Discuss the tool in the fediverse.

Other options
================

- debirdify by @pruvisto@graz.social works very similar.
- twitodon needs you and your followers to be signed up there to match you.


Luca Hammer @Luca@vis.social
=================================

- https://vis.social/@Luca/109253908218072543


I wrote a nano tool that tries to extract the #Fediverse accounts of
your #Twitter followings: https://fedifinder.glitch.me/

It searches for the patterns @user@host.tld, user@host.tld and host.tld/@user
in the screen name, description, location and URL field. It displays
them to you in the correct format for easy copying as well as a CSV
download that can be imported to #Mastodon.

Known issue: Mail addresses are included as false positives because
people write their handle without a leading @: name@host.tld

