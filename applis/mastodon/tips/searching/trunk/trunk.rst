
.. index::
   pair: Searching ; Trunk

.. _trunk:

==========================
Trunk
==========================



Trunk allows you to mass-follow a bunch of people in order to get started
with Mastodon or any other platform on the Fediverse.

Mastodon is a free, open-source, decentralized microblogging network.

