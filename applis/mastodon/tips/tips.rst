.. index::
   pair: Tips ; Mastodon


.. _mastodon_tips:

======================================================================================================
**Tips**
======================================================================================================

- https://help.x.com/en/managing-your-account/how-to-download-your-twitter-archive

.. toctree::
   :maxdepth: 3

   alt_text/alt_text
   at_username_at_server/at_username_at_server
   camel_case/camel_case
   choose_an_instance/choose_an_instance
   move_instance/move_instance
   content_warning/content_warning
   display_name/display_name
   emojos/emojos
   favorites_boosts/favorites_boots
   groups/groups
   introduction/introduction
   searching/searching
   to_follow_someone/to_follow_someone
