.. index::
   ! CW
   ! Content Warning

.. _content_warning:

===================================================================
**CW** (Content Warning) **one of the best features mastodon has**
===================================================================

- :ref:`why_are_cw_everywhere`
- :ref:`comment_ca_marche_le_cw`
- https://blog.djnavarro.net/posts/2022-11-03_what-i-know-about-mastodon/#why-are-content-warnings-everywhere
- https://publish.obsidian.md/louisderrac/Alternatives+num%C3%A9riques/Conseils+transition+Twitter+vers+Mastodon+et+le+F%C3%A9divers#Comment+%C3%A7a+marche+le+CW+content+warning

::

    okay more new user tips

    content warnings are extremely important here

    they're ingrained in our culture

    they're not censorship, but rather consent

    they allow someone to decide if they wanna engage with your post, and if so on what terms

    they're a courtesy, and you have legiterally no reason to not use them as much as possible

    they're one of the best features mastodon has

    think of it like a subject line or a title for your post

    you're MORE LIKELY to get "engagement" if you use them right
