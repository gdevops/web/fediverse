.. index::
   ! Favorites
   ! Boosts

.. _favorites_boots:

======================================================================================================
Favorites ⭐ Boosts 🔁
======================================================================================================


::

    KEEP IN MIND! #Mastodon doesn't work like #Twitter!

    Have you just made the #Twexit or the #TwitterMigration?

    Favorites ⭐ - they're not "likes". They don't "elevate" posts, they
    just the poster know you liked their post.

    Boosts 🔁 - these push someone's post into your friends feeds (and help discovery).

    So if you ⭐ my posts, that's great (and I love you too)!

    But if you 🔁 posts, then more people see them!

    OH! - #Hashtags are how you search!

    #mastodon #MastodonTips
