.. index::
   ! Emojos

.. _emojos:

======================================================================================================
**emojos (custom emoji for Mastodon/Pleroma instances)**
======================================================================================================

Recherche par instance
======================

- https://emojos.in/

Source: https://framapiaf.org/@celfred/109309352962000008

Astuce pour voir la liste des emojis personnalisés (aussi appelés emojos)
sur une instance :

aller sur https://emojos.in/

puis taper le nom de l'instance.

par exemple, mon instance est : "social.nah.re"
La liste complète incluant ceux animés de mon instance est là :

https://emojos.in/social.nah.re?show_all=true&show_animated=true

Et oui, il y en a un paquet…



Framapiaf.org
-----------------

- https://emojos.in/framapiaf.org?show_all=true&show_animated=true
- https://emojos.in/framapiaf.org

.. figure:: images/emojos_framapiaf.png
   :align: center


ACCESSIBILITY TIP: Adding emojis to your display name is terrible for people who use screen readers
=======================================================================================================

- https://mastodon.social/@danyork/109348466692294642


ACCESSIBILITY TIP: Adding emojis to your display name is terrible for
people who use screen readers
I was just reminded by someone that when you do this (as I had done),
then they have to listen to the screen reader read out all the emojis/icons
BEFORE getting to your actual post.


So my name came across as::

    ----
    dan york satellite graphic satellite studio mic graphic studio mic curling stone graphic curling stone
    ----

Thank you to the person who reminded me of this!

#accessibility
