.. index::
   ! Alt text

.. _alt_text:

======================================================================================================
**alt-text, caption images**
======================================================================================================

- https://gomakethings.com/how-to-write-good-alt-text/

.. figure:: images/bernie_sanders.png
   :align: center

   https://fosstodon.org/@ru/109362703044991267


https://mastodon.art/@ritarenoir/109349092959041253, pour décrire les images, on peut s'abonner au bot @PleaseCaption
==========================================================================================================================

- https://botsin.space/@PleaseCaption


Quand on n'a pas trop l'habitude d'utiliser le texte alternatif pour
décrire les images, on peut s'abonner au bot @PleaseCaption.

Celui-ci vous envoie un message privé dès que vous avez oublié de le faire.

Et vous n'avez plus qu'à éditer votre pouet pour rajouter la description
manquante.

Grâce à cette petite astuce, on acquiert très vite de bonnes pratiques 😉

