.. index::
   pair: mastodon; Groups

.. _mastodon_groups:

===========================================
Groups
===========================================

- https://a.gup.pe/


https://fenetre.dev/@rochelle/109365711165197392, For #FollowFriday, I'm recommending GROUPS!
===================================================================================================

- https://fenetre.dev/@rochelle/109365711165197392

❓ How do groups work ?
-------------------------

Follow the group to receive the posts.

Mention the group's @ when writing a post to have your post shared with
the group.

Groups I'm in:

- https://a.gup.pe/u/yoga
- https://a.gup.pe/u/mazeldon
- https://a.gup.pe/u/plants
- https://a.gup.pe/u/3goodthings

Lots more groups here, including directions on how to form your own
group: https://a.gup.pe/

