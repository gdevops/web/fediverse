.. index::
   ! Display name

.. _display_name:

======================================================================================================
**mastodon display name**  #accessibility
======================================================================================================

- https://gomakethings.com/how-to-write-good-alt-text/

.. figure:: images/accessibility.png
   :align: center


ACCESSIBILITY TIP: Adding emojis to your display name is terrible for
people who use screen readers.

I was just reminded by someone that when you do this (as I had done),
then they have to listen to the screen reader read out all the emojis/icons
BEFORE getting to your actual post.

So my name came across as::

    ----
    dan york satellite graphic satellite studio mic graphic studio mic curling stone graphic curling stone
    ----

Thank you to the person who reminded me of this!

#accessibility
