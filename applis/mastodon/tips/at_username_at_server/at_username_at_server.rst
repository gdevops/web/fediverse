
.. _at_user_at_server:

======================================================================================================
**@username@server**
======================================================================================================

- https://mstdn.social/@feditips/109247976374998487

You know how most people just have one phone number ?

That's possible because all phone providers in the world federate together.
No matter which provider you use, you can call anyone anywhere.

Well, Mastodon (and the Fediverse it's a part of) is the same thing but
for social media.

The Fediverse address on your profile is enough for people on other servers
to follow you and interact with you. It looks like this: **@username@server**

There's a video about it here: https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD
