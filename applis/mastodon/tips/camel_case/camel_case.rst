.. index::
   ! CamelCase

.. _camel_case:

======================================================================================================
**CamelCase**
======================================================================================================



suggestion: hashtags are important here. please use “Camel Case” so
screen readers understand what the words are and can pronounce
them properly.

example::

    #DogsOfMastodon not #dogsofmastodon

make sense??
