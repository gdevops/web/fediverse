
.. _move_an_instance:

======================================================================================================
**Move to an other instance**
======================================================================================================

https://tchafia.be/@owl/109364616524371661
=============================================

Comment transférer son compte  Mastodon d'un serveur à un autre sans
perdre ses contacts :

1. s'inscrire sur le nouveau serveur
2. sur le NOUVEAU serveur: Modifier le Compte -> Déménager DEPUIS un autre Compte
   Entrer l'ancien compte
3. sur l'ANCIEN serveur:
   - Import et export : télécharger les abonnement (csv)
4. Compte -> Déménager VERS un autre compte
   Entrer le nouveau compte et votre mot de passe actuel
5. sur le NOUVEAU serveur: Compte : Import et export : importer les abonnement (csv)
