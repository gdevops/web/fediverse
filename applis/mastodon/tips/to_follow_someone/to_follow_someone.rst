.. index::
   ! To follow someone

.. _to_follow_someone:

======================================================================================================
**To follow someone**
======================================================================================================


https://joinfediverse.wiki/Beginners_Fedi_Workshop#What_is_Mastodon?
=======================================================================

- https://joinfediverse.wiki/Beginners_Fedi_Workshop#What_is_Mastodon?


**To follow someone from a different instance you'll probably need to enter
their full handle (username@instance.tld) or their profile's url in the
search field.**


.. figure:: images/following.png
   :align: center
