
.. _eugene_rochko_against_quoting:

======================================================================================================
**2018-03-11 Against quoting**
======================================================================================================

- https://mastodon.social/@Gargron/99662106175542726


I've made a deliberate choice against a quoting feature because it
**inevitably adds toxicity to people's behaviours**.

You are tempted to quote when you should be replying, and so you speak
at your audience instead of with the person you are talking to.

It becomes performative.

Even when doing it for "good" like ridiculing awful comments, you are
giving awful comments more eyeballs that way. **No quote toots. Thank's**

