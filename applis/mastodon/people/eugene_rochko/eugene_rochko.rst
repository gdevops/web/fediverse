.. index::
   pair: People ; Eugène Rochko


.. figure:: images/eugene_rochko.png
   :align: center


.. _eugene_rochko:

======================================================================================================
**Eugène Rochko (Gargron)**
======================================================================================================

- https://en.wikipedia.org/wiki/Eugen_Rochko
- https://mastodon.social/@Gargron
- https://github.com/Gargron
- https://github.com/mastodon/mastodon
- https://github.com/mastodon/mastodon/graphs/contributors

Announce
=========

- https://mastodon.online/@hildabast/109266412786470666

And ta da - sorted!!! There's now a little English-language Wikipedia
page for Eugen Rochko, founder of Mastodon:


Quite a few myths already out there but not much verifiable info.
Very interesting. I didn't know there'd been a grant from Samsung in
the mix till I dug around today.
I've also included a couple of the controversies along the way.

Bio
=====

Eugen Rochko (born 1993) is a German software developer, best known as
the original developer of the distributed microblogging service Mastodon.

Life and education
====================

Rochko was born in a Jewish family of Russian origin and came to
Germany at the age of eleven and attended the grammar school in Jena.

In Jena he was active on networks such as MySpace, schülerVZ,
Facebook, Twitter and ICQ.

Rochko studied computer science at Friedrich-Schiller University, a
public university in Jena.

Career
========

In early 2016 while still studying for his degree, Rochko started working
on the Mastodon software.[6]
Wanting to exclude commercialization from social media was a goal, Rochko
said in an interview in 2018.In 2022, he told an interviewer he was
motivated by rumours that a billionaire could buy Twitter, and his
dissatisfaction with some of the social media platform's functions.

Rochko said that a platform like Twitter plays an important role in
democracy and should not be controlled by a US corporation.
He developed Mastodon with crowdfunding through Patreon and OpenCollective,
and an open-source development grant from Samsung

He published it in early October 2016 after completing his computer science
studies at the University of Jena. He later implemented the ActivityPub
protocol for Mastodon.

As of July 2017, Rochko had a project manager for Mastodon, with the
rest of the work undertaken by volunteers.

He has been criticised by some as a Benevolent Dictator For Life (BDFL),
a title given to some open source software developers who retain a
final say.
In August 2021 he announced that he had incorporated Mastodon as a
non-profit company, as founder and sole shareholder.

In 2019, when the far-right Gab moved to using Mastodon's open source
software, Rochko argued that the Mastodon server covenant mandating
“active moderation against racism, sexism, homophobia, and transphobia”
isolates any such use of the open source software.

He said Gab's developers had severed ties with the Mastodon development
process.

Contre les nazis
====================

- https://piaille.fr/@mart1oeil/109301609987105460

La principale instance de #Mastodon (gérée par le créateur du logiciel)
bloque les intéractions avec les instances qui se revendiquent de l'alt-right.

Sa réponse est claire : Les nazis sont mauvais et je ne veux pas leur
donner la possibilité de recruter.

RT @breakdecks
Mastodon may not replace Twitter, but at least it has better leadership.


.. figure:: images/contre_les_nazis.png
   :align: center


.. toctree::
   :maxdepth: 5

   actions/actions
