

.. _introductions_mastodon:

======================================================================================================
**Exemples d'introduction  #introduction**
======================================================================================================

Mon introduction en Français
===============================


Mon #introduction

En tant qu'humain je suis particulièrement sensible à la #Liberté, l'#Egalité
la #Justice pour tous les êtres humains et au respect des non humains
Je suis assez inquiet de l'avenir de notre planète (#ClimateEmergency)

Je suis #antifasciste et je ne supporte pas le racisme; je lutte en particulier
contre  l'antisémitisme.

Je m'intéresse à l'#anarchisme, le #judaisme

En tant que développeur de logiciels, je porte un grand intérêt au #LogicielLibre.
Je suis un passionné de #Python, je travaille sous #Debian.


Mon introduction en anglais
===============================

I think it's time for a new pinned #introduction.

So here are some of my interests, in rough groupings:

- #Justice #Equality #HumanRights #Antifascism
  Fights against #Antisemitism #AntiRom #Transphobia

- #ClimateJustice #ClimateEmergency #EmergencyMode #ClimateAction
  #StopEcocide #EndFossilFuelProtection #NoECT #ExitECT

- #History, #Geography #Architecture

- #Judaism, #Anarchism

- #Programming #Python #Django #PostgreSQL #Rust #SQL
  #FreeSoftware #Fediverse

RIP #AaronSwartz



Autres exemples d'introduction
=================================


https://mas.to/@jackofalltrades/109315418346578381
=====================================================

#introduction

I enjoy conversations about:

💻 #programming (especially #elixir , #elm , #lisp , #clojure or #ruby ),
🎮 #games (#videogames like #guiltygear , #darksouls , #finalfantasy , #nier as well as #ttrpg and #go) and #gamedev ,
🤔 #philosophy ,
🎸 #music ,
🚀 #scifi ,
🔥 #climatechange ,
⚛️ #science ,
📜 #history ,
📈 #economics , and more.

I am inspired by #AlbertCamus , #NoamChomsky and #UrsulaKLeGuin .



::

    Hi everyone!

    It's time for a proper #introduction. I am a full-time #geographer,
    #geochronologist, and #data scientist working as a researcher at
    the #luminescence laboratory at the Ruprecht-Karls University #Heidelberg.

    My work focuses on developing and applying new methods in #geochronolgy
    (#luminescence_dating) to constrain rapid environmental changes throughout
    the #Quaternary.


::

    #introduction #histodon ancienne #prof d’HG et militante #ÉcoleEmancipée
    membre de @AggiornamentoHG mes recherches portent sur les #musiquespopulaires
    l’histoire de la #pressemagazine et des #mediationslusicales => les #inrockuptibles 1984/2010.
    Je suis aussi sur le #blog #podcast #histgeobox.
    On y parle #chanson #musiquespopulaires et #histoire. Celles qui la
    disent ou la font.



::

    Humaniste, apartisan pas apolitique, centralien, psychosociologue,
    diplomate… Expert en (presque) rien mais résolument poly-disciplinaire,
    je pouette ici pendant que #MonMari cuisine.

    Mes principaux sujets d'intérêt :
    - #Science (#Maths, #Physique, etc.)
    - #Sciences humaines (#Socio, #Psy et #Éco)
    - #DroitsHumains (team #Universalisme)
    - #Politique française (team #Apartisan)
    - #Géopolitique (européenne et mondiale)
    - #MonMari (qui mérite une médaille)
    - #MonChat (on habite chez lui, paraît-il)


::



    I think it's time for a new pinned intro post. So here are some of my
    interests, in rough groupings:

    - #baking #sourdough #bread and #cooking generally
    - #tea

    - #FountainPens
    - #JaneAusten and #Regency
    - #sailing and #TallShips
    - #drawing

    - #RPGs and #games and #GameDesign
    - #programming (especially with #Rust and #Python)
    - #urbanism

    - #WeightLifting and #archery
    - #Judaism, #Quakerism, and #Buddhism
    - #singing and #FolkMusic

    Also I'm always interested in whatever you're geeking out about!


::


    🌲 #Introduction 🌲

    🐺 Hello! I'm an #OSS/#Technology hobbyist who spends a little too much
    time looking inwards and thinking about things.

    💧 I haven't been on a social network in years now— but after reading
    more and more about the #Fediverse, I figured it was time that I give
    it a whirl and network a bit.

    ☀️ So far lots and lots of my posts have been about #Vegan food,
    sometimes with #Recipes.

    #Music #Technology #Cycling #Cooking #Hiking #UrbanPlanning #AntiFascism
    #Philosophy #AnimalRights

::

    Have you started using hashtags in your posts? It's a best practice on Mastodon! Here are some options:
    #introduction , #ai , #NewPaper , #PaperThread , #SOTA , #NLProc ,
    #CV , #RL , #reviews , #conference , #deadline , #aiart , #academia ,
    #industry , #PetPics , #question , #discussion , #announcement ,
    #hiring , #JobHunt , #code , #news , #interesting , #concerning ,
    #cool , #lame , #funny , #ethics , #fun , #serious , #random , #til ,
    #scifi , #media , #SigmoidSocial

::

    By way of #introduction, I am a #MastodonWorld mod, father of three
    fantastic teenage daughters, spouse to a real hero, and ethics and
    compliance professional who has been lucky enough to live and work
    in 9 countries on 5 continents over the last 22 years.
    Very happy to have found this alternative to Twitter and so many of
    my favorite people from Twitter who have already arrived here.

    I will boost interesting content and add some of my own from time
    to time. Thanks for saying "hi".

::

    At last, #introduction time:

    I'm a geographer working as a research data manager & web dev for
    the archaeology. I'm a longtime @qgis user, which I teach.
    I started learning & using #python and #django 2 years ago.

    Beside geospatial and web, I've worked before as a performer & artist.

    I'm also into #sailing, and I've been #livingaboard for a couple
    of years. I'm fluent in French, English and Spanish.

    Last but not least, I'm the admin of mapstodon.space - quite happy
    to see y'all around here!

::


    🌏 Introducing https://mapstodon.space, a new Mastodon instance for
    the mapping & geospatial community 🚀

    Mapstodon is intended to be a gathering space for GIS, mapping,
    geospatial & cartography professionals and enthusiasts, regardless
    of the tools you use. Ask questions.
    Share your maps. And above all: enjoy the @qgis emoji :qgis: 👀


::



    Hi there,
    #introduction

    This is the official Mastodon account for the Python Software Foundation (PSF.)
    We're the non-profit home of the Python programming language and
    our mission is to to promote, protect, and advance the Python programming
    language, and to support and facilitate the growth of a diverse and
    international community of Python programmers.

    We also run PyCon US, https://us.pycon.org/2023/

    We're excited to be here. :D


::



    Since it seems to be the #mastodon tradition ...

    #introduction

    I live in #Vancouver , #canada (on unceded territory; https://vancouver.ca/people-programs/land-acknowledgement.aspx).

    I am a core developer on the #python programming language (19 years and counting).

    Spare time is:

    - #opensource (https://github.com/brettcannon/ , https://snarky.ca/)
    - #movies (https://letterboxd.com/nothingbutsnark/)
    - #tv shows (https://trakt.tv/users/brettcannon)
    - #gaming (mostly Destiny 2 and Marvel Snap ATM)
