.. index::
   ! IFFT

.. _ifft:

===================================================================================================================
**IFFT (If This Then That) Pour rappeler qu'on n'utilise plus twitter mais mastodon ou pleroma par exemple**
===================================================================================================================

- https://ifttt.com/applets/DrVfp3Ri-every-day-at-same-time-post-an-automatic-tweet


Every day at same time, post an automatic tweet.

Pour rappeler qu'on n'utilise plus twitter mais mastodon ou pleroma
par exemple.
