.. index::
   pair: mastodon; twitter

.. _twitodon:

=======================================================
Twitodon Find your Twitter friends on Mastodon
=======================================================

- https://twitodon.com/

When you login to Twitodon with both your Twitter and Mastodon accounts
it will memorise the association between your two accounts.

Twitodon will then match any Twitter users you follow that have previously
loged into Twitodon thereby providing their own association information.

Unfortunately, there is no way of discovering, or guessing, Twitter users
on Mastodon without them logging into Twitodon previously.

Because of this, it is recommended to check back again regularly to keep
up to date as more users use Twitodon.

