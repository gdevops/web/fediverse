.. index::
   pair: Firefox ; libredirect

.. _libredirect:

========================================================================================================================================================================================================
L'extension **libredirect (firefox)** +  *Fritter (Android)** (A web extension that redirects YouTube, Twitter, Instagram, and other requests to alternative privacy friendly frontends and backends)
========================================================================================================================================================================================================


Description
===============

Installez l’extension libredirect dans votre navigateur, de manière à
pouvoir continuer à consulter Twitter à travers l’interface Nitter
(vous pouvez même suivre les comptes Twitter dans votre lecteur RSS).

Sur Android, utilisez Fritter.

Chrome/Firefox
================

Libredirect pour Chrome/Firefox.

- https://libredirect.codeberg.page/

Android
==========

Fritter pour Android.

- https://f-droid.org/en/packages/com.jonjomckay.fritter/





