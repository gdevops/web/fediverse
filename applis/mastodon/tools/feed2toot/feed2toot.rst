.. index::
   ! Feed2toot

.. _Feed2toot:

===================================================================================================================
**Get Your RSS Feeds To Mastodon With The Feed2toot bot**
===================================================================================================================

- https://carlchenet.com/get-your-rss-feeds-to-mastodon-with-the-feed2toot-bot/


A lot of people migrates from Twitter to the decentralized an free Mastodon
social network and it’s a great thing. You would like to offer great content
on Mastodon from your RSS feeds, maybe the new post of your own blog?

Just use the new Feed2toot bot!

Feed2toot is a powerful RSS to Mastodon bot with lots of great features like:

- manage multiple RSS feeds
- pattern matching to select entries from the RSS feeds
- smart management of hashtags from the RSS feeds
