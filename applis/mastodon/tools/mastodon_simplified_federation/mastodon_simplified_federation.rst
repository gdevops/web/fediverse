.. index::
   pair: Firefox extension ; Simplified Federation!

.. _mastodon_simplified_federation:

======================================================================
**Mastodon – Simplified Federation !**  by https://chaos.social/@rugk
======================================================================


- https://addons.mozilla.org/en-US/firefox/addon/mastodon-simplified-federation/
- https://github.com/rugk/mastodon-simplified-federation#how-does-it-work


Author
==========

- https://github.com/rugk


Description
=================

Simplifies following or interacting with other users on remote Mastodon 
instances in the Fediverse. 

It skips the “Enter your Mastodon handle” popup and takes you to your 
own “home” instance, without entering your Mastodon handle on remote instances.


🤯 Why? 🤯
================

You may wonder why to use this browser add-on. But actually, it's easy!

You do not need to enter your Mastodon account handle anymore! (except for 
login 😉) This makes interacting with remote instances in the Fediverse much simpler.

Additionally, this add-on makes sure to keep your Mastodon handle private. 
It will never expose it to any third-party site. 

Therefore it does not – literally – enter your Mastodon ID into the input 
field you normally see, but basically "skips" this page. 

For the technical details, on how this works, see the detailed explanation on GitHub.


🐤 How to use? 🐤
After you have installed the add-on, you need to go into the settings, enter your Mastodon handle (that is username@server). After this is done, there is nothing you need to do anymore. The add-on will continue to work in the background and you do not need to do anything.
Just enjoy the simplified federation when you are getting redirected automatically when you want to follow or interact with a user.

