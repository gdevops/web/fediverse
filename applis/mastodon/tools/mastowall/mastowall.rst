.. index::
   ! mastowall

.. _mastowall:

===================================================================================================================
**masotowall** (Simple implementation of a Twitter-Wall like grid of postings from Mastodon )
===================================================================================================================

- https://github.com/rstockm/mastowall



Examples
==========

hashtags=israel,gaza,hamas&server=https://mastodon.social
-------------------------------------------------------------

::

- https://rstockm.github.io/mastowall/?hashtags=israel,gaza,hamas&server=https://mastodon.social


hashtags=python&server=https://framapiaf.org
------------------------------------------------

- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org
