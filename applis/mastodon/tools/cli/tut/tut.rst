.. index::
   pair: CLI ; Tut

.. _mastodon_tut_cli:

==========================================================================================================
**Mastodon tut CLI** (TUI for Mastodon with vim inspired keys)
==========================================================================================================

- https://github.com/RasmusLindroth
- https://www.rasmus.xyz/
- https://github.com/RasmusLindroth/tut
