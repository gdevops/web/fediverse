.. index::
   pair: Mastodon API ; Python

.. _mastodon_api_python:

=======================
mastodon-api-python
=======================

- https://opensource.com/article/23/1/mastodon-api-python

The federated Mastodon social network has gotten very popular lately.

It's fun to post on social media, but it's also fun to automate your
interactions. There is some documentation of the client-facing API,
but it's a bit light on examples.
This article aims to help with that.

You should be fairly confident with Python before trying to follow along
with this article.
If you're not comfortable in Python yet, check out `Seth Kenlon's Getting
started with Python article <https://opensource.com/article/17/10/python-101>`_ and my Program a simple game article.


The first step is to go to Preferences in Mastodon and select the Development category.
In the Development panel, click on the New Applications button.

After creating an application, copy the access token. Be careful with it.
This is your authorization to post to your Mastodon account.

There are a few Python modules that can help.

- The httpx module is useful, given that it is a web API.
- The getpass module allows you to paste the token into the session safely.
- Mastodon uses HTML as its post content, so a nice way to display HTML inline is useful.
- Communication is all about timing, and the dateutil and zoneinfo modules will help deal with that.


