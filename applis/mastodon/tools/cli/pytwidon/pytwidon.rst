.. index::
   pair: pytwidon ; Mastodon

.. _pytwidon:

======================================================================================================
**pytwidon** par Laurent Abbal
======================================================================================================


- https://github.com/laurentabbal/publication-sur-twitter-et-mastodon-avec-python
- https://forge.aeif.fr/laurentabbal/publication-sur-twitter-et-mastodon-avec-python
- https://github.com/halcy/Mastodon.py

Annonce
=======

- https://mastodon.social/@laurentabbal/109302780245339945

Publication sur Twitter et Mastodon avec Python
===================================================

Code #Python pour publier simultanément sur #Twitter et #Mastodon (texte + image).

Avantage: plus besoin de passer par http://crossposter.masto.donte.com.br
(ou équivalent) et donc plus besoin de partager des paramètres de
connexion avec des sites tiers.

Documentation en cours de rédaction.

Code : https://github.com/laurentabbal/publication-sur-twitter-et-mastodon-avec-python/blob/main/pytwidon.py

Pour pouvoir utiliser ce code, il faut tout d'abord créer les clés Twitter
et Mastodon.

Modules Python
===============


pytwidon.py
============

- https://github.com/laurentabbal/publication-sur-twitter-et-mastodon-avec-python/blob/main/pytwidon.py

.. literalinclude:: pytwidon.py
   :linenos:
