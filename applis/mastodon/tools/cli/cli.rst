.. index::
   pair: CLI ; Mastodon

.. _mastodon_cli:

======================================================================================================
**Mastodon CLI**
======================================================================================================

.. toctree::
   :maxdepth: 3

   mastodonpy/mastodonpy
   pytwidon/pytwidon
   python/python
   toot/toot
   tut/tut
