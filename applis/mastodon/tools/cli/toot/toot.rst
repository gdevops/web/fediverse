.. index::
   pair: CLI ; Toot

.. _mastodon_toot_cli:

==========================================================================================================
**Mastodon toot CLI** (a CLI and TUI tool for interacting with Mastodon instances from the command line)
==========================================================================================================

- https://lists.sr.ht/~ihabunek
- https://github.com/ihabunek/toot
- https://lists.sr.ht/~ihabunek/toot-discuss

.. figure:: toot_logo.png
   :align: center

Resources
============

* Homepage: https://github.com/ihabunek/toot
* Issues: https://github.com/ihabunek/toot/issues
* Documentation: https://toot.readthedocs.io/en/latest/
* Mailing list for discussion, support and patches:
  https://lists.sr.ht/~ihabunek/toot-discuss
* Informal discussion: #toot IRC channel on `libera.chat <https://libera.chat/>`_

Features
=============

* Posting, replying, deleting statuses
* Support for media uploads, spoiler text, sensitive content
* Search by account or hash tag
* Following, muting and blocking accounts
* Simple switching between authenticated in Mastodon accounts
