.. index::
   ! crossposter

.. _mastodon_twitter_poster:

======================================================================================================
**Crossposter to post statuses between Mastodon and Twitter**
======================================================================================================

- https://github.com/renatolond/mastodon-twitter-poster

https://crossposter.masto.donte.com.br/
=========================================

- https://crossposter.masto.donte.com.br/


Articles
==========


- https://framapiaf.org/web/statuses/109273767719786945
- https://blogs.mediapart.fr/mollo/blog/010121/configurer-la-publication-de-vos-pouets-mastodon-vers-twitter
