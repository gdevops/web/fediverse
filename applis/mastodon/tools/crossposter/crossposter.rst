.. index::
   ! crossposter

.. _crossposter:

======================================================================================================
**crossposter**
======================================================================================================

- https://github.com/felx/mastodon-documentation/blob/master/Using-Mastodon/Apps.md#bridges-fromto-other-platforms

.. toctree::
   :maxdepth: 3

   mastodon_twitter_poster/mastodon_twitter_poster
