.. index::
   pair: Firefox extension ; Mastodon4 Redirect

.. _mastodon4_redirect:

======================================================================
**Mastodon4 Redirect** by raikasdev
======================================================================


- https://addons.mozilla.org/en-US/firefox/addon/mastodon4-redirect/
- https://addons.mozilla.org/en-US/firefox/user/17013625/



Redirects you from other Mastodon instances to your own instances equilevant 
page, so you don't have to manually go to your instance and paste the URL in 
the search bar.
