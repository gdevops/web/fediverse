
.. _shkspr_api:

======================================================================================================
**Mastodon API** by shkspr
======================================================================================================

- https://shkspr.mobi/blog/2022/11/getting-started-with-mastodons-conversations-api/
- https://shkspr.mobi/blog/tag/mastodon/


Introduction
==============

The social network service "Mastodon" allows people to publish posts.

People can reply to those posts. Other people can reply to those replies
and so on.

What does that look like in the API ?

Here's a quick guide to the concepts you need to know - and some code to
help you visualise conversations.


Concepts
============

In Mastodon's API, a post is called a status.

Every status on Mastodon has an ID. This is usually a Snowflake ID which
is represented as a number.

When someone replies to a status on Mastodon, they create a new status
which has a field called in_reply_to_id.

As its name suggests, has the ID of the status they are replying to.

Let's imagine this simple conversation::

    Ada: "How are you?"
    Bob: "I'm fine. And you?"
    Ada: "Quite well, thank you!"

Message 2 is in reply to message 1. Message 3 is in reply to message 2.

In Mastodon's jargon, message 1 is the ancestor of message 2. Similarly,
message 3 is the descendant of message 2.
