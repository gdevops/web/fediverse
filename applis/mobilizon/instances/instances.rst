.. index::
   pair: instances ; Mobilizon


.. _instances_mobilizon:

======================================================================================================
**Instances mobilizon**
======================================================================================================

- https://joinmobilizon.org/fr/


.. toctree::
   :maxdepth: 3

   chapril/chapril
   manifs.fr/manifs.fr
