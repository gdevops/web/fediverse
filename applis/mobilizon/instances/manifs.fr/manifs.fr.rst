.. index::
   pair: manifs.fr ; Mobilizon


.. _manifs_fr:

======================================================================================================
**manifs.fr** #logiciellibre #mobilizon #fediverse #FOSS
======================================================================================================

- https://manifs.fr/


2023-07-28 Une nouvelle instance mobilizon dédiée aux luttes éco-sociales a ouvert récemment
=============================================================================================

Salut à tous·tes !

Une nouvelle instance mobilizon dédiée aux luttes éco-sociales a ouvert récemment.

Elle vise à contribuer à la décentralisation et à la résilience des réseaux
des instances libres et émancipatrices...

https://manifs.fr/

Lutte · Transformation sociale · Émancipation · Liberté

"Partage ton événement militant en protégeant les libertés fondamentales."

#logiciellibre #mobilizon #fediverse #FOSS
