
.. index::
   pair: Mobilizon; Chapril

.. _mobilizon_chapril:

======================================================================================================
**mobilizon.chapril**
======================================================================================================

- https://mobilizon.chapril.org
- https://mobilizon.chapril.org/about/instance

Introduction
=============

Rejoignez mobilizon.chapril.org, une instance Mobilizon

L'instance généraliste mobilizon.chapril.org est un service proposé par
le Chapril permettant d'organiser des évènements, préparer l'avant et
l'après, et de publier des infos pour les groupes que vous souhaitez
gérer à plusieurs.

Du plus petit anniversaire familial ou la sortie entre amis à la grande
manifestation internationale, le Chapril vous permet de réaliser vos
rassemblements comme vous le souhaitez tout en préservant la confidentialité
des participantes et participants.

