.. index::
   pair: Hébergeur ; Chapril
   ! Chapril


.. _chapril:

===========================================================================================
Chapril (CHATONS + April)
===========================================================================================

.. seealso::

   - https://www.chapril.org/
   - https://chatons.org/fr/chaton/chapril
   - https://agir.april.org/projects/chapril
   - https://www.april.org/groupes/chapril
   - https://x.com/aprilorg
   - https://wiki.april.org/w/Chapril


.. figure:: Chapril-logo-x90.png
   :align: center
   :width: 300

Mise à jour mai 2017 : le chaton April s'appelle `chapril.org <https://www.chapril.org/>`_.
Chapril pouvant signifier :

- **CHaton Aprilien Pour le Respect de votre Informatique et de vos Libertés**
- **Centre d'Hébergement Aprilien Pour le Respect d'une Informatique Libre**
- **Chaton Aprilien Pour le Respect d'une Informatique Libre**

Ou c'est simplement un jeu de mot/mot valise de **CHATONS + April**.


.. toctree::
   :maxdepth: 3

   definition/definition
   services/services
   wiki/wiki




