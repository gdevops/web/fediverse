
.. _def_chapril:

===========================================================================================
Chapril
===========================================================================================

.. seealso::

   - https://www.chapril.org/


.. contents::
   :depth: 3

Présentation
===============

Le site `chapril.org <https://www.chapril.org/>`_ est la contribution de l’April au Collectif des
Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires (CHATONS).

Le projet vise à rassembler des acteurs proposant des services en ligne
libres, éthiques, décentralisés et solidaires afin de permettre aux utilisateurs
de trouver - rapidement - des alternatives aux produits des GAFAM
(Google, Apple, Facebook, Amazon, Microsoft), entre autres, mais
respectueux de leurs données et de leur vie privée.

L'April a la volonté de s'engager dans le projet Chatons en proposant des services.

Mise à jour mai 2017 : le chaton April s'appelle chapril.org.
Chapril pouvant signifier :

- CHaton Aprilien Pour le Respect de votre Informatique et de vos Libertés
- Centre d'Hébergement Aprilien Pour le Respect d'une Informatique Libre
- Chaton Aprilien Pour le Respect d'une Informatique Libre

Ou c'est simplement un jeu de mot/mot valise de CHATONS + April.

Si la participation à ce projet vous intéresse :

    la liste de discussion dédiée : https://listes.april.org/wws/subscribe/chapril
    le salon IRC : #april-chapril sur irc.freenode.net
    la documentation d'adminsys Chapril : https://admin.april.org/
    la page wiki April : https://wiki.april.org/w/Chapril

    Site web: https://wiki.april.org/w/Chapril
