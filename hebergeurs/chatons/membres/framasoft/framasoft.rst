.. index::
   pair: Hébergeur ; Framasoft
   ! Framasoft


.. _framasoft:

===========================================================================================
Framasoft
===========================================================================================

.. seealso::

   - https://framasoft.org/fr/
   - https://chatons.org/fr/chaton/framasoft
   - https://framapiaf.org/@Framasoft
   - https://stats.chatons.org/framasoft.xhtml






