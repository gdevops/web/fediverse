.. index::
   ! CHATONS
   ! Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires


.. _chatons:

===========================================================================================
CHATONS (Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires)
===========================================================================================


.. seealso::

   - https://chatons.org/
   - https://forum.chatons.org/
   - https://framagit.org/chatons
   - https://framagit.org/chatons/CHATONS
   - https://framapiaf.org/@ChatonsOrg
   - https://framasphere.org/people/00cc6a302c16013640d42a0000053625
   - https://x.com/ChatonsOrg
   - https://stats.chatons.org/index.xhtml
   - https://entraide.chatons.org/fr/
   - https://chatons.org/fr/find-by-localisation


.. figure:: logo_chatons_v2.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   presentation/presentation
   wiki/wiki
   forum/forum
   logiciels/logiciels
   services/services
   membres/membres


