

.. _presentation_chatons:

===========================================================================================
Présentation
===========================================================================================


.. seealso::

   - https://chatons.org/fr/presentation_v2


.. contents::
   :depth: 3


Introduction
==============

CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts,
Neutres et Solidaires.

Ce collectif vise à rassembler des acteurs proposant des services en
ligne libres, éthiques, décentralisés et solidaires afin de permettre
aux utilisateur⋅ices de trouver rapidement des alternatives (respectueuses
de leurs données et de leur vie privée) aux produits des GAFAM (Google,
Apple, Facebook, Amazon, Microsoft).

CHATONS est un collectif initié par l'association Framasoft suite au
succès de sa campagne `«Dégooglisons Internet» <https://degooglisons-internet.org/>`_.


Je n'ai pas compris...
=========================

CHATONS est un collectif regroupant des petites structures proposant des
services en ligne (par exemple du mail, de l'hébergement de sites web,
des outils collaboratifs, des outils de communication, etc.).


On appelle "hébergeurs" ces structures pour plusieurs raisons :

- elles gèrent des serveurs (ordinateurs sur lesquels sont installés des
  programmes permettant de faire fonctionner des services en ligne),
- elles proposent aux internautes le stockage de leurs données et leur
  diffusion sur le web.


La particularité de CHATONS est que les membres de ce collectif s'engagent notamment :

- à n'utiliser que des logiciels libres ;
- à ne pas exploiter les données des bénéficiaires de leurs services
  (= ne pas transmettre ou exploiter vos données) ;
- à ne pas utiliser de régies publicitaires (ou autres services de pistage) ;
- à proposer régulièrement des rencontres "physiques" avec leurs
  bénéficiaires, afin de réduire la fracture numérique.

Je n'ai toujours pas compris...
===================================

Vous connaissez le principe des AMAP ? Ces associations qui proposent
aux habitant⋅es des villes des paniers de légumes (ou autres denrées)
issus de la production agricole locale (et souvent biologique) ?

Eh bien vous pouvez voir CHATONS comme « un réseau d'AMAP du service en ligne ».

Là où Google, Facebook ou Microsoft représenteraient l'industrie
agro-alimentaire, les membres de CHATONS seraient des « paysans informatiques »
proposant des services en ligne « bio » :

- sans OGM (= sans logiciels privateurs ne garantissant pas vos libertés),
- sans pesticide (= sans baser leurs revenus sur la vente de vos données personnelles),
- sans marketing agressif (= sans régie publicitaire pistant vos comportements),
- sans une « course au pouvoir d'achat », qui en réalité porte tort aux
  petits agriculteurs (= prix équitables entre les chatons et leurs bénéficiaires)


CHATONS est donc tout à la fois une organisation de membres partageant
les mêmes valeurs et une forme de "label".

Pourquoi ce collectif ?
============================

Les objectifs sont multiples :

- proposer aux bénéficiaires une offre de services libres respectueux
  de leurs vies privées/professionnelles
- donner une visibilité aux structures partageant des valeurs communes
- partager de l'information entre membres

**Plus globalement, il s'agit de résister à la « googlisation » d'Internet**.

Les gigantesques réservoirs de données gérés par les géants du web nuisent
non seulement à la vie privée (en rendant la surveillance de masse possible,
et en rendant les failles de leurs systèmes catastrophiques pour des millions,
voir des milliards d'utilisateur⋅ices), mais posent un problème plus global,
politique et citoyen : **quels pouvoirs confions-nous entre les mains de quelques sociétés ?**

De nombreuses structures indépendantes promouvant une vision du libre
existent depuis des années : https://www.lautre.net/, https://www.web4all.fr/,
https://www.marsnet.org, https://ouvaton.coop/, https://www.infini.fr/,
https://www.toile-libre.org/ (liste évidemment non exhaustive...)

Cependant, la plupart ne proposaient pas d'applications en ligne et se
concentraient sur des services plus techniques (web/FTP, email, etc.),
laissant le champ libre aux GAFAM pour attirer les utilisateur⋅ices sur
leurs services collectant massivement les données personnelles.

Plus récemment, des structures proposant des applications libres sont
apparues, comme https://zaclys.com ou https://framasoft.org.

Le collectif CHATONS vise donc à proposer une structuration des ces
acteurs (anciens comme nouveaux).

Un manifeste et une charte
===============================

Le collectif repose sur l'engagement d'adhérer sans retenue au Manifeste
et à la Charte du collectif.

Ces documents sont les fondements sur lesquels le collectif s'appuie
pour sa réussite. Ils sont rédigés et amendés collaborativement par
ses membres.

Que peut m'apporter un chaton ?
====================================

Les membres du collectif vous apportent la garantie que vous contrôlez
vos données et utilisez un hébergeur proche de chez vous.

En effet, chaque chaton a accepté les conditions du collectif.

Ce sont des hébergeurs prêts à aller vers du 100% libre, à renoncer à la
publicité, à prioriser le respect des données personnelles de leurs
hébergé·es et à rencontrer physiquement ces dernier⋅es.

À l'instar des AMAP, trouvez des chatons proches de chez vous !

Quels sont les services proposés par les chatons ?
======================================================

Les services des chatons sont multiples :

- Outils web (moteur de recherche, sites web et blogs, raccourcisseur d'URL, etc.)
- Outils de communication (mail, liste de diffusion, visioconférence, etc.)
- Outils de travail collaboratif (agenda, gestion de projet, rédaction
  collaborative, etc.)
- Services de partage de fichiers (stockage permanent ou temporaire,
  plateformes de diffusion audio et vidéo, etc.)

Vous pouvez les découvrir en vous rendant sur https://chatons.org/fr/find-by-services.

Comment trouver mon futur chaton ?
===================================

Il y a plusieurs façons de trouver son chaton :

- en cherchant par service souhaité
- en cherchant par localisation du chaton (grâce à la carte)
- en combinant différents critères (type de structure, modèle économique
  de la structure, etc.) via notre moteur de recherche

Qui dirige le collectif ? Quelle est sa gouvernance ?
=========================================================

La forme et la gouvernance du collectif ne sont pas figées, et même
volontairement floues.

Comme CHATONS est directement inspiré du mouvement du logiciel libre,
il n'est pas très étonnant que certaines caractéristiques soient communes.

D'abord, les grandes lignes du projet ont été définies par l'association
Framasoft. Ainsi, il faut voir la mise en place de CHATONS comme la toute
première version d'un logiciel libre, son premier commit.

En conséquence, c'est bien Framasoft qui oriente, dans un premier temps,
le collectif, tout en étant à l'écoute des souhaits des membres.

Cela n'est pas sans rappeler l'analogie du dictateur bienveillant ou du
chef de projet.

Cependant l'association Framasoft n'ayant aucun intérêt, ni financier,
ni politique, à gérer les orientations du collectif, il est très vraisemblable
que la gouvernance évolue à terme vers une forme différente, que les
membres devront définir ensemble.

Comment sont évalués les critères des engagements des CHATONS ?
==================================================================

Nous allons vous faire peur : il n'y a pas de « grand comité de vérification
et de conformité » (ou équivalent) ! :-)

**Le contrôle se base sur un principe vieux comme le monde : la confiance**.

Nous pensons en effet que **l'engagement moral à respecter l'ensemble des
points de la Charte est un bon point de départ**.

Le meilleur, même. Peut-être que, plus tard, le collectif évoluera vers
d'autres formes de contrôle.

Mais pour commencer, un fonctionnement basé sur la confiance :

- évite des lourdeurs administratives ;
- évite les failles techniques (s'il fallait déléguer un accès "root"
  aux machines, cela serait une faille de sécurité) ;
- n'empêche pas un contrôle par les pairs (un membre du collectif peut
  interroger un autre membre sur les solutions mises en place) ;
- n'empêche pas un retour des bénéficiaires (par exemple en commentaire
  d'une fiche sur le site chatons.org, pour indiquer un dysfonctionnement).

Par ailleurs, en cas de litige réel et profond, un groupe de médiation
n'ayant pas d'interêts directs dans l'affaire pourra intervenir.

CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts,
Neutres et Solidaires.


