.. index::
   ! United Federation Of Instances


.. _ufi:

===================================================================
**United Federation Of Instances (UFI)**
===================================================================

- https://qoto.org/@freemo.rss
- https://qoto.org/@freemo/109429264207428585
- https://gitlab.com/ufoi/constitution
- https://gitlab.com/ufoi/constitution/-/issues
- https://ufoi.gitlab.io/constitution/united_federation_of_instances_proposal.pdf

A current version of the proposal PDF can be found at the link below.

It automatically updated when this repo updates, so it will always reflect
the content in the repo.

https://ufoi.gitlab.io/constitution/united_federation_of_instances_proposal.pdf

If you want access to create a merge request please just accept it, All
requests will be approved.

Feel free to discuss changes, concerns or anything else in the issues section.

Find us on the fediverse either at the group account here: @ufoi@a.gup.pe
or by searching for the #UFoI hashtag.

