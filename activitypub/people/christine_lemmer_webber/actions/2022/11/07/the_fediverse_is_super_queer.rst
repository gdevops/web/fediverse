

.. _christine_lemmer_webber_superqueer_2022_11_07:

======================================================================================================
**2022-11-07 The fediverse is super queer, and it was made by super queer people**
======================================================================================================

- https://octodon.social/@cwebber/109304135272848513

@JohannaMellis @Quatrus @Literally @histodons

The fediverse is super queer, and it was made by super queer people.
Cishet it ain't.

Super white, that's true though.

But that's challenging too, I don't want to say that in such a way that
erases the hard work of the people who *are* here and *have* done things.

But piecing that apart isn't my place.  What I will say is, we can, and
must, do better.  The rest I will leave to those affected.

I will highlight some people of color whose writings and thinking about
that I have liked and thought a lot about: @Are0h, @jalcine, and @brainblasted
come to mind... not a comprehensive list, of course, and hopefully they
don't mind being tagged.  I recommend listening and following them tho.

I'm interested in hearing more recommendations, and also hearing what I
can do to boost and support voices of people of color on the fediverse
in general as well.

Mostly I've tried to listen, but I feel I could do a lot better there.


