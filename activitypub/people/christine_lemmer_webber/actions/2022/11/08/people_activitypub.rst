

.. _christine_lemmer_webber_2022_11_09:

======================================================================================================
**2022-11-08 A lot of people contributed to ActivityPub**
======================================================================================================

- https://www.w3.org/TR/activitypub/#Overview
- https://octodon.social/@cwebber/109307961236854695


A lot of people contributed to ActivityPub, but I'm going to give a
breakdown of the authors and what they did:

- Evan Prodromou designed the core prototol, derived from the federation
  API of Pump.IO (itself a "lessons learned" from his experiences
  co-authoring OStatus, ActivityPub's predecessor)
- @erincandescent (https://queer.af/@erincandescent) converted Evan's
  set of notes into the spec format and language style
- @tsyesika (https://mastodon.social/@tsyesika) was the first major
  editor of the spec once it was being standardized at the W3C
- @rhiaro (https://toot.cat/@rhiaro) separated out client-to-server
  support from server-to-server support and made many, many edits
- And I carried the spec through probably the majority of the bureaucratic
  process and **tried really hard to get inter-project buy-in on its ideas**.

And most importantly, I wrote that `Overview tutorial <https://www.w3.org/TR/activitypub/#Overview>`_ at the top of the
document, which is the part I'm most proud of !

