
.. _activitypub_description:

======================================================================================================
**ActivityPub description**
======================================================================================================

.. figure:: ../images/logo_activitypub.png
   :align: center

https://fr.wikipedia.org/wiki/ActivityPub
=============================================

- https://fr.wikipedia.org/wiki/ActivityPub


ActivityPub est un standard ouvert pour réseaux sociaux décentralisés
basé sur le format ActivityStreams 2.0.

Il a été officiellement publié comme recommandation du W3C le 23 janvier 2018.

Il fournit une API allant d'un client vers un serveur pour la création,
la mise à jour et la suppression de contenu, ainsi qu'une API entre
serveurs afin de permettre la fédération de notifications et de contenus.

Cette norme est une évolution de Pump.io et est proposé comme remplacement
d'OStatus par le groupe de travail sur les web social fédéré du W3C2,
lancé en juillet 2014, pour le Fediverse.

