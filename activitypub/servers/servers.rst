.. index::
   ! Activitupub servers

.. _activitypub_servers:

======================================================================================================
Activitypub servers
======================================================================================================

.. toctree::
   :maxdepth: 3

   gotosocial/gotosocial
   takahe/takahe
