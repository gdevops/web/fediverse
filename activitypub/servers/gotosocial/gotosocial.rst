.. index::
   pair: Activitupub server; gotosocial

.. _activitypub_server_gotosocial:

======================================================================================================
gotosocial Fast, fun, ActivityPub server, powered by Go
======================================================================================================

- https://github.com/superseriousbusiness/gotosocial/
