.. index::
   pair: Activitupub server; takahe
   ! takahe

.. _activitypub_server_takahe:

======================================================================================================
**takahe** An ActivityPub/Fediverse server
======================================================================================================

- https://github.com/andrewgodwin/takahe


Annonce
========

- https://fedi.simonwillison.net/@simon/109299909072384991

Very exciting to poke around in @andrew's new (extremely early/experimental)
project: an ActivityPub server in Django that works with SQLite and
uses Django's async support

Python asyncio feels like a very clear win for all of the incoming and
outgoing HTTP request involved in this stuff, especially having watched
how backed up my Mastodon Sidekiq queues have been getting firing off
all those webhooks

