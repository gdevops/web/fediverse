.. index::
   ! ActivityPub python library

.. _activitypub_python:

======================================================================================================
**A general Python ActivityPub library**
======================================================================================================

- https://github.com/dsblank/activitypub


Abstractions
==================

This module is designed to be a generally useful ActivityPub library in
Python. It targets three different levels of use:

- ActivityPub object API
- ActivityPub database API
- Webserver API
