.. index::
   ! ActivityPub

.. _activitypub:

======================================================================================================
**ActivityPub**
======================================================================================================

- https://github.com/w3c/activitypub
- https://fr.wikipedia.org/wiki/ActivityPub
- https://www.w3.org/TR/activitypub/#Overview

.. figure:: images/logo_activitypub.png
   :align: center


.. toctree::
   :maxdepth: 3

   description/description
   extensions/extensions
   people/people
   servers/servers
   libs/libs
