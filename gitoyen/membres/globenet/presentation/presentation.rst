
.. _presentation:

===========================================================================================
Présentation
===========================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Globenet
   - https://www.globenet.org/

.. contents::
   :depth: 3

Définition wikipedia
=======================


Globenet est une association loi de 1901 agissant dans le domaine de
l'Internet et des communications numériques.

L'association a été fondée en 1995 afin de fédérer les projets de plusieurs
associations et ONG s’intéressant à Internet.

Elle s'adressait alors aux « individus, professionnels et organismes
travaillant dans les domaines de l’énergie, de l’environnement, de la
justice sociale et économique, soutenant le développement technologique
et économique des pays en développement ou en voie de développement,
des droits de l’homme, de la paix, de l’art et des échanges culturels ».

Elle se définit depuis 2004 comme « une association militante, au service
de la liberté d’expression, proposant des services internet ».

Globenet propose :

- un service payant d'hébergement web, courriel et listes de diffusion
  à destination des associations;
- un service gratuit d'accès à internet par modem et de courriel pour
  le grand public sous le nom No-log.

Globenet est membre fondateur du GIE Gitoyen, opérateur de communications
sur Internet.
