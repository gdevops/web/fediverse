.. index::
   ! Globenet

.. _globenet:

===========================================================================================
Globenet
===========================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Gitoyen

.. toctree::
   :maxdepth: 3

   presentation/presentation
   reunions/reunions
   services/services




