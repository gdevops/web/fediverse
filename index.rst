.. Documentation tutorial documentation master file
   2019-04-27


.. https://framapiaf.org/web/tags/qualite.rss
.. https://framapiaf.org/web/tags/softwareQuality.rss


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://climatejustice.social/@climatejustice_fr"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

.. figure::  images/fediverse_applis.png
   :align: center
   :width: 800

   https://axbom.com/fediverse/, Image sescription https://axbom.com/fediverse/#longdesc


.. note::

    Image description of the diagram

    I encourage using this description as ALT-text for the image when
    publishing it in other places.

    A large tree grows from a green platform labeled “ActivityPub + more”.
    ActivityPub is a protocol for communicating between different applications
    in the Fediverse.

    The trunk of the tree is labeled The Fediverse, to indicate that all
    the applications within the tree crown are part of the The Fediverse.

    The crown is made up of circles that intersect with each other.

    Circle 1: Multimedia  (streaming, video, photos, podcasting, images, files).
    Apps: Downcast, PeerTube, Pixelfed, Castopod and Nextcloud.
    Circe 2: Networking.
    Apps: Friendica, Mastodon, Misskey, Pleroma, Diaspora, GnuSocial, Hubzilla, Socialhome, kbin and Lemmy.
    Circle 3: Music.
    Apps: Funkwhale.
    Circle 4: Books.
    Apps: Bookwyrm.
    Circle 5: Writing.
    Apps: Write Freely, Plume, Drupal (via plugins) and Wordpress (via plugins).
    Circle 6: Events.
    Apps: Mobilizon and Bonfire.

    Growing out of the trunk is a separate branch labeled “Paid services”.

    Apps here are micro.blog and write.as.

    Smaller trees around the main tree indicate other protocols for federated social networks.

    Off to the right are 3 small trees, one rooted in diaspora protocol,
    one in the OStatus protocol, and one in the Zot protocol.

    The two applications Hubzilla and Friendica connect with Diaspora and
    OStatus using dotted lines. Socialhome connects to Diaspora.
    GnuSocial connects to OStatus. Hubzilla connects to Zot.

    Off to the left are two small trees, one with its roots in the Matrix
    protocol and the other with its roots in the XMPP protocol.

    Nextcloud connect to XMPP and Matrix. Drupal connects to XMPP.

|FluxWeb| `RSS <https://gdevops.frama.io/web/fediverse/rss.xml>`_

.. _tuto_fediverse:

==========================
**Fediverse tutorial**
==========================

- https://the-federation.info/
- https://gofoss.net/protect-your-freedom/
- https://joinfediverse.wiki/Main_Page
- https://joinfediverse.wiki/The_Fediverse_in_the_Fediverse
- https://alternativeto.net/feature/fediverse/
- https://forum.chatons.org/
- https://fr.wikipedia.org/wiki/Fediverse
- https://framablog.org/2019/03/07/la-fee-diverse-deploie-ses-ailes/

.. figure:: images/fediverse.png
   :align: center
   :width: 200

.. figure:: images/fediverse_diverse.png
   :align: center
   :width: 300

   You can't spell #Fediverse without #diverse
   (https://climatejustice.social/@PaulaToThePeople/109261446652648751)


.. toctree::
   :maxdepth: 3

   people/people
   fedidevs/fedidevs   
   coc/coc
   global-switch-day/global-switch-day   
   formations/formations
   hebergeurs/hebergeurs
   gitoyen/gitoyen
   applis/applis
   activitypub/activitypub
   rss/rss
   bluesky/bluesky   
   tools/tools
   tutorials/tutorials
   faq/faq
   glossaire/glossaire

.. toctree::
   :maxdepth: 5

   news/news
