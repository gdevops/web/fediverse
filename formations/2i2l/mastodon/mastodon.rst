.. index::
   pair: Mastodon ; 2i2L

.. _2i2l_mastodon:

==========================
Formation mastodon 2i2l
==========================

- https://www.2i2l.fr/formation-mastodon-decouvrir-un-reseau-social-different

Mastodon
=========

Formation Mastodon : découvrir un réseau social différent
Se lancer dans un réseau social alternatif par la pratique
Réseaux sociaux & Protection des sources


Politique de modération insuffisante ou incohérente, accumulation des données,
publicité omniprésente : les exemples de problèmes posés par les grands réseaux
sociaux se multiplient.

Depuis maintenant près de dix ans, Mastodon, un réseau social libre et fédéré
propose une alternative non-marchande.

Nous proposons une journée de formation pour comprendre comment fonctionne
ce réseau, commencer à publier et plonger dans l’univers de Mastodon.

Présentation
=================

Mastodon est un réseau social décentralisé qui repose sur l’interconnexion
de serveurs (appelés instances).

Ce logiciel est souvent comparé à Twitter dans ses fonctionnalités (partage
de messages courts, avec ou sans médias, à une audience), mais sans la
recommandation algorithmique.

- `La page de présentation officielle de Mastodon <https://joinmastodon.org/fr>`_
- `L’espace de développement du projet <https://github.com/mastodon/mastodon>`_
- développé sous licence GNU Affero GPL v3
- `La page Wikipédia de Mastodon <https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)>`_

Programme
==================

Ce programme est un original, comme tous les programmes de formation de ce
catalogue ; ils sont écrits par les formateurs 2i2L.

Les autres programmes très proches, que vous trouverez, sont des copies de notre catalogue

Notions clés de Mastodon et du fediverse
- notion d’instance et de fédération
- historique d’ActivityPub et du projet Mastodon
- gouvernance de Mastodon

Choisir son instance
- découvrir les différentes instances Mastodon
- pourquoi le choix d’instance est important ?
- différentes version de Mastodon
- se créer un compte

Découvrir et comprendre le fil d’actualité
- rechercher des comptes
- suivre des comptes
- timeline locale, timeline fédérée
- hashtags

Publier sur Mastodon
- découverte de l’interface de publication
- comprendre les différents niveaux de confidentialité possibles
- téléverser un média
- les limites de caractères
- les Content Warning, qu’est-ce que c’est ?
- alt-texts et accessibilité
- les applications pour mobile
