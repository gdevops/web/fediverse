.. index::
   pair: FAQ; Axbom

.. _faq_axbom:

================================
Axbom Questions and answers
================================


- https://axbom.com/fediverse/


What, if anything, is the advantage of having accounts on multiple (micro)blog  sides like mastodon ?
========================================================================================================

What, if anything, is the advantage of having accounts on multiple (micro)blog
sides like mastodon ? I get the purpose of the separate video, music and
photo ones, but many of these look at first glance like alternatives to
mastodon.

Is that the case and it’s just user’s choice/where someone feels most comfortable?
Yes I would say it's more often a user preference. They have different
interfaces, features and integrations.

I understand Friendica for example has a Twitter add-on, and supports
the diaspora protocol. Some people may also want several accounts for
redundancy, or for talking about completely different topics, e.g. one
for golf and one for knitting.

