.. index::
   pair: Tutorial; Mastodon Quick Start Guide for Humanities Scholars

.. _humanities_2023:

======================================================================================================
**Mastodon Quick Start Guide for Humanities Scholars**
======================================================================================================

- https://hcommons.org/docs/mastodon-quick-start-guide-for-humanities-scholars/
- https://religion.masto.host/@amv/109293911728956360


Articles
==========

- https://religion.masto.host/@amv/109293911728956360

I don't know how useful this will be, but I've compiled a quick start
guide, with lots of screenshots, aimed at #humanities #academics who are
#NewToMastodon, trying to cover some of the things I see people most
frequently getting stuck on in some less technical language than existing
documentation.

I've put it on #HumanitiesCommons open for edits and comments so that
other folks can help fill out things like useful hashtags.
