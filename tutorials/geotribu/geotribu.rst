.. index::
   pair: Géotribu ; Embarquer dans Mastodon : guide pour les géo
   pair: Tutoriel ; Embarquer dans Mastodon : guide pour les géo

.. _geotribu_fediverse:

======================================================================================================
2024-02-16 Géotribu **Embarquer dans Mastodon : guide pour les géo** (fait avec mkdocs)
======================================================================================================

- https://geotribu.fr/articles/2024/2024-02-16_de-twitter-a-mastodon-guide-geo-import-liste-comptes/


Bonjour.  Nous sommes en 2024 et il est temps de faire de X (Twitter) un
réseau social secondaire.  Peur de sauter par-dessus bord ? Je comprends.
Voici un guide pour atterrir en douceur sur le réseau décentralisé Mastodon.
Peur d'un énième guide ? Je comprends.  Ce guide est fait sur-mesure pour les
cartographes, géomaticiennes, géomaticiens et même pour les sigistes ! Oui,
même pour les sigistes. Impressionné/e ? Je comprends.

Je comprends sincèrement que :


- l'inertie est forte,
- que sortir de sa zone de confort avec les habitudes et followers acquis
  ces dernières années est contre-nature,
- que maintenir une présence sur les réseaux sociaux est déjà suffisamment
  chronophage pour ne pas s'en rajouter,
- que l'émiettement des plateformes est douloureux et également chronophage,
- qu'il est tentant de faire de LinkedIn sa page d'accueil malgré la part
  toujours plus importante de contenus bullshités à l'IA,
- que les concepts sous-jacents à Mastodon et tous ces machins de Fediverse
  font plus penser à un kif de geeks qui ont transposé leurs fantasmes
  des univers de super-héros dans leurs outils du quotidien mais qu'on n'a
  pas que ça à faire de configurer ceci, paramétrer cela, lancer telle commande, etc.


Je comprends. Vraiment. Oui, même si c'est contre ma propre nature de géogeek
justement. 😉

Mais je suis persuadé qu'il y a un intérêt à peupler ce réseau social de
gens plus diversifiés que ceux qui vont presque naturellement sur Mastodon
au risque d'en faire justement une bulle d'écho entre technophiles
plutôt de gauche ; comme BlueSky semble l'être pour le monde universitaire /
CSP+... et tXitter celle des réactionnaires.

Essayez. Ça n'engage à rien. Vraiment.  Laissez vous guider, ça va bien
se passer.
