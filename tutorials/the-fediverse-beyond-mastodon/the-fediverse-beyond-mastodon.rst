.. index::
   pair: fedi.tips; The Fediverse beyond Mastodon

.. _fediverse_beyond_mastodon:

======================================================================================================
**The Fediverse beyond Mastodon**
======================================================================================================

- https://fedi.tips/the-fediverse-beyond-mastodon/



The Fediverse beyond Mastodon
================================

Most new users join the Fediverse via Mastodon, but that’s just one part
of a much wider Fediverse with many different kinds of servers.

This section explores the non-Mastodon kinds of servers out there.

Because Fedi servers use a common technical standard to talk to each other,
people on different kinds of servers can usually interact with each other,
and many of the accounts you follow on Mastodon aren’t actually on
Mastodon servers!

