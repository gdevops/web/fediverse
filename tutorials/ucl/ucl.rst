.. index::
   pair: Fediverse; Union Communiste Libertaire

.. _raison_fediverse_ucl:

======================================================================================================
**Réseau : Comprendre le fédiverse** par Pablo (UCL Saint-Denis) 🇫🇷
======================================================================================================

- https://www.unioncommunistelibertaire.org/?Reseau-Comprendre-le-fediverse


Introduction
=============

Centralisé, fédéré, acentré, décentralisé, ces qualificatifs reviennent
souvent dans les discussions concernant les libertés numériques, mais
que veulent-ils dire exactement ?

Un réseau est un ensemble de nœuds pouvant être liés deux à deux.

Un chemin sur un réseau est une suite de liens permettant d’aller d’un
nœud à un autre. Typiquement, en informatique, les nœuds sont des ordinateurs
et les liens des câbles réseaux.

Réseaux centralisés, décentralisés, fédérés
===============================================

Dans un système centralisé comme celui du Minitel, il y a un nœud spécial,
le système central, dont tout le réseau dépend.

Chacun des autres nœuds du réseau est connecté uniquement au système
central. Pour se parler, deux nœuds doivent donc nécessairement passer
par le système central : il n’y a pas d’autre chemin possible.

Cela fait du système central un maillon critique du réseau, avec une
position d’autorité absolue et un contrôle total.
Le réseau Internet a été conçu au contraire de façon décentralisée : il
peut exister des liens entre n’importe quelle paire de nœuds du réseau.
Cela le rend plus robuste : l’absence de système central permet l’existence
de chemins alternatifs en cas de panne d’un système donné.

**Mais cela empêche au passage qu’un nœud se retrouve en situation de contrôle
monopolistique du réseau**.

**C’est pourquoi les grandes plateformes capitalistes d’aujourd’hui (comme les Gafam)
recréent artificiellement un modèle de réseau centralisé par-dessus le web
pour se rendre indispensables, forcer les échanges à passer par elles et
ainsi en tirer profit**.

En opposition à ce modèle se sont développées des plateformes alternatives
acentrées (sans centre), basées sur des logiciels libres non-commerciaux,
tels que PeerTube ou :ref:`Mastodon <mastodon>` (respectivement des alternatives à YouTube
et Twitter, voir AL n°288).

Si l’architecture de ces plateformes était totalement décentralisée,
alors chaque personne devrait lancer sur son ordinateur le logiciel
afin de se connecter au réseau.


Ce n’est pas inenvisageable, c’est le principe des réseaux pair-à-pair
(exemple : le partage peer to peer de fichiers par BitTorrent).

Cependant, pour rendre réaliste la concurrence avec les plateformes
capitalistes centralisées, le choix retenu a été celui d’une architecture fédérée.

Dans un réseau fédéré, il y a des nœuds un peu spéciaux  : ce sont les
serveurs qui font tourner une instance du logiciel pour plusieurs
utilisateurs et utilisatrices.

On appelle justement ces nœuds des instances (exemple : :ref:`Mamot.fr <serveur_mamot_fr>` est une
instance de Mastodon, gérée par **La Quadrature du Net**).

Comme le logiciel est libre, n’importe qui peut faire tourner une instance
sur son nœud et la connecter aux autres instances pour ainsi permettre
à ses utilisateurs et utilisatrices d’échanger avec le reste du réseau.

La dimension anti-autoritaire
================================

En pratique, cela va même plus loin puisque l’utilisation de protocoles
ouverts, en l’occurrence ActivityPub, permet à des logiciels différents
de parler entre eux  : il est par exemple possible de suivre une chaine
PeerTube depuis un compte Mastodon.

Cette galaxie de logiciels intercompatibles et d’instances fédérées est
appelée le fédiverse (de l’anglais fediverse, mot-valise pour federated universe).

Avec le modèle fédéré, aucune instance ne peut se rendre indispensable
et contrôler le réseau. Cela reste en tout cas vrai tant qu’il n’y a
pas d’instance incontournable car trop grosse (en terme de nombre
d’utilisateur et utilisatrices).

C’est là que l’aspect politique est important : le modèle non marchand
de ces plateformes alternatives ne les incite pas à rendre captifs leurs
utilisateur et utilisatrices.

Au contraire, gérer une énorme instance est particulièrement couteux, et
sans source de revenu on est donc plutôt incité à garder son instance
petite.

Le contraste avec les plateformes à vocation commerciale est ici évident.

Pablo (UCL Saint-Denis)

