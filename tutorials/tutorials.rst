.. index::
   pair: tutorials ; fediverse

.. _tutorials_fediverse:

======================================================================================================
**Tutorials fediverse**
======================================================================================================

- https://blog.professeurjoachim.com/billet/2018-12-05-un-guide-rapide-de-mastodon

.. toctree::
   :maxdepth: 3

   axbom/axbom
   fedi_workshop/fedi_workshop
   geotribu/geotribu
   the-fediverse-beyond-mastodon/the-fediverse-beyond-mastodon
   gofoss_net/gofoss_net
   mastodon-quick-start-guide-for-humanities-scholars/mastodon-quick-start-guide-for-humanities-scholars
   ucl/ucl
   petit-guide-du-fediverse-2025/petit-guide-du-fediverse-2025   
