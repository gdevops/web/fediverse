.. index::
   pair: Fediverse ; Tutorial Gofoss.net

.. _gofoss_fediverse:

======================================================================================================
**Le guide du voyageur Fediverse** (🇫🇷 , Excellent)
======================================================================================================

- https://gofoss.net/fr/fediverse/
- https://gitlab.com/curlycrixus/gofoss


Introduction
=============

L'univers fédéré, ou « Fediverse », est une alternative libre et open
source aux médias sociaux commerciaux.

Le Fediverse se compose de divers réseaux sociaux et services de microblogage
interconnectés, où les utilisatrices et utilisateurs peuvent librement
communiquer et partager des photos, des vidéos, de la musique et bien
plus encore.

Au moment d'écrire ces lignes, le Fediverse comptait plus de 5 millions
d'utilisateurs et d'utilisatrices. Le Fediverse est :

- Décentralisé : au lieu d'une seule entreprise, plusieurs fournisseurs
  alternatifs offrent un accès au Fediverse.
  Tout le monde peut gérer un serveur, aussi appelé « instance », et ainsi
  fournir des services à la communauté.
  C'est comme si n'importe quel internaute ou organisme pouvait gérer
  son propre serveur Twitter, Facebook, YouTube ou Instagram et inviter
  ses ami·e·s. à le rejoindre.
- Fédéré : les utilisatrices et utilisateurs de différents services peuvent
  communiquer entre eux, grâce au protocole ActivityPub.
  C'est comme si les usagers de Twitter, Facebook, Instagram ou YouTube
  pouvaient interagir librement les un·e·s avec les autres.
- Bienveillant : le Fediverse n'est pas optimisé pour capter l'attention
  des internautes, créer des profils d'usagers et afficher autant de
  publicités et de contenu payant que possible.
  Contrairement à Twitter, Facebook, YouTube, Instagram, Reddit et consorts.
- Proche de l'humain : il n'y a pas de filtres qui décident quels contenus
  les internautes peuvent voir dans leur chronologie et quels contenus
  seront censurés. Aucun algorithme n'impose aux internautes des contenus
  controversés pour les faire « réagir ».
  Il n'y a pas de modèles sombres (en anglais, « dark pattern ») pour
  influencer le comportement des internautes. Une fois de plus, c'est ce
  qui différencie la Fediverse des GAFAM.
- Émancipateur : nul besoin de céder vos droits d'auteur.
  Pas de conditions de service ou de politiques de modération arbitraires
  et opaques. Aucun mur numérique entre les réseaux sociaux.
  Contrairement aux plateformes commerciales gérées par les géants de la tech.


