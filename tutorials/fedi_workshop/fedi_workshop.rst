.. index::
   pair: Beginners; Fedi Workshop

.. _fediverse_beginners_workshop:

======================================================================================================
**Beginners Fedi Workshop** 🇬🇧
======================================================================================================

- https://joinfediverse.wiki/Beginners_Fedi_Workshop


This is the protocol of a (series of) workshop(s) held by @PaulaToThePeople
for Fediverse beginners, especially those who came to Mastodon after
Musk bought Twitter.


What is Mastodon ?
====================

- https://joinfediverse.wiki/Mastodon_features_explained

See also the article `What is Mastodon <https://joinfediverse.wiki/What_is_Mastodon%3F>`_ ? in this wiki.

As well as `Mastodon features explained <https://joinfediverse.wiki/Mastodon_features_explained>`_
for a full review of all Mastodon  features.

Mastodon is a free software microblogging project, so similar to Twitter,
with some features that are the same and some different features.
It is decentral/federated and part of the Fediverse (see below).

When you just created an account you'll see the basic interface with an
empty home timeline. You'll have to fill it by following people.

**To follow someone from a different instance you'll probably need to enter
their full handle (username@instance.tld) or their profile's url in the
search field.**

In the Settings you can change some things about Mastodon's look and feel.

For example you can switch to the advanced interface where you can have
many columns.

You can for example search for a hashtag and then pin that column to the
screen be clicking in the top right corner.

If you have too many columns, they won't fit on the screen and you will
have to zoom out.

You can create lists of people, e.g. people who's posts you don't want
to miss, and also pin that list as a column.

The home timeline shows posts by accounts you follow and boosts by those
accounts. It will look the same no matter if they have a Mastodon account
or use a different Fediverse service.

The local timeline just shows posts by accounts on your server.

No boosts.

The federated timeline shows the whole known Fediverse - known to your
server that is.
An account is known to your server once it is followed or was ever
searched for by someone on your server.

