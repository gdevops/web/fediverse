.. index::
   pair: Axbom; The many branches of the Fediverse

.. _fediverse_branches_axbom:

======================================================================================================
**The many branches of the Fediverse** par @axbom@social.xbm.se, 🇬🇧
======================================================================================================

- https://social.xbm.se/@axbom
- https://axbom.com/fediverse/

.. figure::  ../../images/fediverse_applis.png
   :align: center


As more and more people are asking me about Mastodon I felt a need for a
picture to point at, showcasing how the software known as Mastodon fits
into the much larger concept of the Fediverse.

I made this visualisation to help myself and others explain the many
different use-cases and benefits of different services that can exchange
information.

I am well aware this still doesn't cover all available tools.

The image is a simplification, as most models are. Given that new tools
can be made available regularly it would be an insurmountable task to
pursue a complete mapping. Hopefully this visual is enough to illustrate
the amazing variety of services within the Fediverse, and will spark
curiosity to explore further.

You can download the diagram in PNG format or as a PDF.

