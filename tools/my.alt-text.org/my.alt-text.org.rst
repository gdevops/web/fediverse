.. index::
   pair: Alt; Text
   pair: Tool; Alt-Text

.. _my_alt_text_org:

===============================================================================================
**https://my.alt-text.org/**
===============================================================================================


- https://my.alt-text.org/
- https://social.alt-text.org/@hannah
- https://social.alt-text.org/@hannah.rss
- https://alt-text.org/
- https://github.com/alt-text-org
- https://github.com/alt-text-org/my.alt-text.org


Alt-Text.org
==============

What if descriptions of popular images, written by a human, were available
to anyone who needed them?

What if folks could sign up to effortlessly contribute the alt text they
write on social media?

What if they could come across posts without it and add a description for
the world with a right click?

We believe these are all possible, and we'd like to make them real.

The idea for a shared library of alt text has been around since the alt
attribute was added to the HTML specification.

It's only recently that the technology available to the public has caught up.

The project as it stands is a successful proof of concept.
Building the library as we build a non-profit around it is the next challenge.

We've assembled an incredible team from across the world to build the
foundation for this project. Watch this space for updates, or follow us on Mastodon.

hello@alt-text.org

`GitHub <https://github.com/alt-text-org>`_
