
.. _tools:

==========================
Tools/Tips
==========================

- https://fediverse.info/explore/people

.. toctree::
   :maxdepth: 3

   fediverse_in_the_fediverse/fediverse_in_the_fediverse
   fediplan/fediplan
   invidious/invidious
   mesures/mesures
   ntfy/ntfy
   mastopoet/mastopoet
   my.alt-text.org/my.alt-text.org
