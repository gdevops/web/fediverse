.. index::
   pair: Fediverse; fediplan

.. _fediplan:

===============================================================================================
**fediplan** (A way to safely schedule messages with Mastodon and Pleroma)
===============================================================================================


- https://framagit.org/tom79/fediplan
