.. index::
   pair: Tip; Invidious
   pair: Tip; Invidious
   pair: Tip; #yewtube
   pair: #youtube; #yewtu.be
   ! Invidious

.. _invidious:

===============================================================================================
**Invidious**
===============================================================================================

- https://invidious.io/
- https://en.wikipedia.org/wiki/Invidious
- https://github.com/iv-org
- https://github.com/iv-org/invidious
- https://github.com/iv-org/documentation

Tip Diffuser des liens **#yewtu.be** plutôt que #youtube
=================================================================

- https://docs.invidious.io/installation/
- https://docs.invidious.io/instances/
- https://github.com/yewtudotbe/invidious-custom
- https://yewtu.be/


[ASTUCE!] Diffuser des liens #yewtube plutôt que #youtube


Concrètement, ces deux liens diffusent la même vidéo:

- https://youtube.com/watch?v=mWv2uGeoZY8
- https://yewtu.be/watch?v=mWv2uGeoZY8

Les deux affichent la vidéo hébergée par Youtube (=#Google) mais le second
lien avec une interface plus légère, moins intrusive, et sans coupure
publicitaire (invidious)
