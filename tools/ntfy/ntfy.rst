.. index::
   ! ntfy

.. _ntfy:

======================================================================================================
**ntfy  (pronounce: notify)** is a simple HTTP-based pub-sub notification service
======================================================================================================

- https://github.com/binwiederhier/ntfy
- https://docs.ntfy.sh/
- https://docs.ntfy.sh/faq/

.. figure:: ntfy_logo.png
   :align: center


**ntfy** (pronounce: notify) is a simple HTTP-based pub-sub notification service.

It allows you to send notifications to your phone or desktop via scripts
from any computer, **entirely without signup or cost**.

It's also open source (as you can plainly see) if you want to run your own.

I run a free version of it at ntfy.sh. There's also an open source Android
app (see Google Play or F-Droid), and an open source iOS app (see App Store).

