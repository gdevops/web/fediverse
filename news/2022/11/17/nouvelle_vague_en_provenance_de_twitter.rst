
.. _vague_twitter_2022_11_17:

========================================================
2022-11-17 Nouvelle vague en provenance de twitter
========================================================

- https://www.nytimes.com/2022/11/18/technology/elon-musk-twitter-workers-quit.html
- https://linuxfr.org/users/vendrediouletrollsauvage/liens/effondrement-ou-faillite-de-twitter
- https://www.journaldugeek.com/2022/11/18/twitter-a-feu-et-a-sang-bientot-la-fermeture-du-reseau/

.. figure:: images/refugies_humour.png
   :align: center

   Rare image d’abonnés #Twitter arrivant sur #mastodon, https://piaille.fr/@Digori/109366732722233401


https://www.nytimes.com/2022/11/18/opinion/twitter-yoel-roth-elon-musk.html
===============================================================================

This month, I chose to leave my position leading trust and safety at Elon Musk’s Twitter.

My teams were responsible for drafting Twitter’s rules and figuring out
how to apply them consistently to hundreds of millions of tweets per day.

In my more than seven years at the company, we exposed government-backed
troll farms meddling in elections, introduced tools for contextualizing
dangerous misinformation and, yes, banned President Donald Trump from
the service.

The Cornell professor Tarleton Gillespie called teams like mine the
“custodians of the internet.”
The work of online sanitation is unrelenting and contentious.

Enter Mr. Musk.

In a news release announcing his agreement to acquire the company,
Mr. Musk laid out a simple thesis: “Free speech is the bedrock of a
functioning democracy, and Twitter is the digital town square where
matters vital to the future of humanity are debated.”

He said he planned to revitalize Twitter by eliminating spam and
drastically altering its policies to remove only illegal speech.

Since the deal closed on Oct‌. 27‌‌, many of the changes made by Mr. Musk
and his team have been sudden and alarming for employees and users
alike, including rapid-fire layoffs and an ill-fated foray into
reinventing Twitter’s verification system.

A wave of employee resignations caused the hashtag #RIPTwitter to
trend on the site on Thursday — not for the first time — alongside
questions about whether a skeleton crew of remaining staff members
can keep the service, now 16 years old, afloat.

...

And yet when it comes to content moderation, much has stayed the same
since Mr. Musk’s acquisition. Twitter’s rules continue to ban a wide
range of lawful but awful speech.
Mr. Musk has insisted publicly that the company’s practices and policies
are unchanged. Are we just in the early days — or has the self-declared
free speech absolutist had a change of heart?

The truth is that even Elon Musk’s brand of radical transformation has
unavoidable limits.

Advertisers have played the most direct role thus far in moderating
Mr. Musk’s free speech ambitions. As long as 90 percent of the company’s
revenue comes from ads (as was the case when Mr. Musk bought the company),
Twitter has little choice but to operate in a way that won’t imperil
the revenue streams that keep the lights on. This has already proved
to be challenging.

Almost immediately upon the acquisition’s close, a wave of racist
and antisemitic trolling emerged on Twitter.

Wary marketers, including those at General Mills, Audi and Pfizer,
slowed down or paused ad spending on the platform, kicking off a
crisis within the company to protect precious ad revenue.

