
.. _ucl_news_2022_11_18:

========================================================================
2022-11-18 Comparaison tout à fait partiale entre Twitter et Mastodon
========================================================================


Introduction
===============

Chez Twitter, c’est le chaos.

Depuis le rachat du réseau social par le milliardaire Elon Musk, l’instabilité
de l’entreprise se ressent jusque chez les utilisateur·ices de la plateforme.

En cause, d’une part les licenciements de la moitié des employé·es de
l’entreprise, et d’autre part les lubies et humeurs changeantes de
son nouvel et unique propriétaire.

Alors que Twitter était, tristement, devenu une plateforme incontournable,
notamment dans le monde politico-médiatique, il est aujourd’hui impossible
de prédire si l’entreprise, et donc sa plateforme, survivront à l’année 2023.

En réaction, on observe une arrivée massive de nouveaux et nouvelles
utilisateur·ices sur Mastodon, un réseau social de microblogage libre
et décentralisé alternatif à Twitter.

Il n’est pas encore possible de savoir si on assiste à une migration,
ce qui impliquerait de quitter la plateforme de départ, ou simplement
à la création de comptes sur un service supplémentaire par ces personnes,
ni même de prédire si elles resteront présentes sur Mastodon à plus
long terme.

Mais quoi qu’il en soit, nous, militant·es libristes de l’Union communiste
libertaire, ne pouvons que nous réjouir de la mise en avant de réseaux
sociaux alternatifs aux plateformes centralisées capitalistes.

L’UCL a toujours fait la promotion du logiciel libre et des réseaux
décentralisés, en interne (https://www.unioncommunistelibertaire.org/?Libertaires-libristes-L-UCL-s-empare-des-enjeux-numeriques)
comme en externe (https://www.unioncommunistelibertaire.org/?Contre-le-capitalisme-de-surveillance-et-la-technopolice-le-logiciel-libre)

À ce titre, l’UCL est est présente sur le fédivers (federated universe,
univers fédéré) depuis longtemps, et fait son possible pour sensibiliser
aux enjeux politiques incarnés par ces alternatives, que ce soit par
des articles dans notre mensuel Alternative libertaire (https://www.unioncommunistelibertaire.org/?Reseau-Comprendre-le-fediverse) ou encore
par notre présence aux `Journées du Logiciel Libre 2022 <https://pretalx.jdll.org/jdll2022/talk/QLFHAF/>`_.


La différence concrète entre Twitter et Mastodon ? Le capitalisme de surveillance.
======================================================================================

Si l’UCL est aussi attachée à ces combats pour les libertés numériques,
c’est bien sûr parce que les modèles du logiciels libres, des protocoles ouverts,
des réseaux fédérés et interopérables correspondent mieux à nos aspirations
politiques, mais pas seulement.

Structurellement, les implications de ces choix techniques impactent
directement l’expérience des utilisateur·ices.

Twitter, l’entreprise, a pour objectif de vendre de l’affichage
publicitaire, qui doit pour être rentable être à la fois le mieux
ciblé et le plus vu possible.

Cela implique que Twitter, la plateforme, rende ses utilisateur·ices captifs
pour que d’un côté iels accordent le plus de temps possible à son
utilisation, et de l’autres qu’iels interagissent toujours plus sur
Twitter, le réseau, puisque c’est de cette façon que se fait le travail
d’extraction des données personnelles qui permet d’affiner les profils (https://www.unioncommunistelibertaire.org/?Economie-de-la-donnee-Si-c-est-gratuit-c-est-toi-qui-produis)

**Bien sûr, la qualité de ces interactions n’a aucune importance pour Twitter**.

Au contraire, pour atteindre les objectifs évoqués, les algorithmes de
la plateforme vont mettre en avant le contenu qui fera réagir, en bien
ou en mal, mais surtout en mal puisque les débats qui s’enveniment
durent plus longtemps et impliquent plus de monde, ce qui par ailleurs
favorise notamment la désinformation comme on a pu le
constater (https://www.unioncommunistelibertaire.org/?Reseaux-sociaux-fausse-neutralite-vraie-desinformation)

Tout ça est très différent du côté de Mastodon.

Déjà, il n’est pas anodin que du côté des réseaux sociaux capitalistes,
il soit impossible de discerner l’entreprise, du logiciel (la plateforme),
du réseau lui-même.

**Mastodon est un logiciel libre, pas une plateforme**.

Chaque installation du logiciel correspond à une instance qui participe
à un réseau, le fameux fédivers (qui ne se limite d’ailleurs pas à Mastodon !).

Et du fait de sa décentralisation, le réseau ne dépendra jamais d’une
unique entreprise dont un milliardaire pourrait prendre le contrôle du
jour au lendemain pour changer les règles qui le régissent selon
ses désirs et opinions du moment.

La multitude d’instances qui participent au réseau (plusieurs milliers
à ce jour rien que pour Mastodon) sont gérées indépendamment et par
différents types de structures : individus, collectifs, associations, etc.

Tous les modèles sont possibles et peuvent coexister, ce qui permet aussi
l’expérimentation des méthodes et des règles de modération, par exemple.

De plus, hors du capitalisme de surveillance, nul besoin de provoquer
la réaction à tout prix.

Là où Twitter cherche toujours à grossir plus pour augmenter son nombre
d’utilisateur·ices, les instances de Mastodon n’ont aucun intérêt à
garder ses utilisateur·ices captif·ves, au contraire, et nombreuses sont
les instances de Mastodon à fermer leurs inscriptions pour éviter d’avoir
trop d’utilisateur·ices, pour une question de coût (ressources serveurs,
stockages, réseaux) et d’autre part pour éviter une concentration du
réseau autour de quelques grosses instances à qui cela donnerait trop
de pouvoir [6] (https://www.unioncommunistelibertaire.org/?Decentralisation-Framafin-de-certains-framatrucs)

D’un côté, Twitter a transformé les « fav » marqués par une étoile et ne
notifiant que l’auteurice du message mis en favoris en « like » marqués
par un cœur et rendu public à ses abonné·es, voire plus largement au
bon vouloir des algorithmes de la plateforme, pour générer des notifications
et faire réagir.

**De l’autre, Mastodon préfère en rester à la simple mise en favori des messages.**

Là où Twitter permet les partages avec citation (les quote-retweet, ou « QRT »)
qui en pratique servent très majoritairement à répondre publiquement, le
plus souvent pour exprimer un désaccord, et dans certains cas à déchaîner
ses nombreux·ses abonné·es contre l’auteurice du message cité (au point
de régulièrement provoquer des vagues de harcèlement), Mastodon a choisi
de ne pas mettre en œuvre cette fonctionnalité dans l’objectif d’apaiser
les discussions.

Là où Twitter impose à la vue de ses utilisateur·ices les messages qui
les feront le plus réagir selon ses algorithmes de profilages, Mastodon
non seulement se contente de présenter chronologiquement les messages
postés et partagés par les comptes auxquels on est abonné, mais permet
également l’utilisation d’un avertissement (content warning) quand on
rédige un message dont on estime que le contenu pourrait choquer
certaines personnes, afin de laisser le choix à ses abonné·es de
dévoiler ou non ce contenu [7].

::

    Sur Twitter, la pratique militante est généralement de faire un
    premier message avec l’avertissement puis de faire un fil de message
    avec le contenu à raconter ; mais à coup de partages et d’algorithmes
    de recommandations, la plateforme affiche régulièrement des messages
    du fil, et forcément ceux qui font le plus réagir, chez des personnes
    qui n’ont même pas eu connaissance de l’avertissement…


L’ensemble de ces choix (absence de QRT, de système de cœur, usage du
content warning), vont ainsi à l’encontre de la logique du capitalisme
de surveillance, prêt à tout pour retenir l’attention des utilisateur·ices
et les faire réagir, y compris exploiter leurs traumas ;

**les fonctionnalités de Mastodon invitent au contraire à un cadre d’échange
plus apaisé et respectueux de la disponibilité de l’ensemble de ses utilisateur·ices**.

Voilà quelques exemples de différences tout à fait concrètes dans
l’expérience qu’on peut avoir sur les réseaux sociaux libres et
décentralisés.

Bien sûr, ces différences structurelles nécessitent aussi de faire des
compromis par ailleurs, sur la simplicité d’utilisation par exemple.

Typiquement, pour rejoindre le réseau, il ne suffit pas de s’inscrire
sur une unique plateforme clairement identifiée, et du coup son nom
d’utilisateur·ice n’est pas un simple pseudo : il contient aussi le nom
de l’instance.

Comme pour une adresse de courrier électronique, il faut d’abord choisir
sur quelle instance on s’inscrit (à l’UCL, pour ce qui est des courriels,
on vous recommandera bien plus volontiers RiseUp ou ProtonMail que
Gmail ou Hotmail, par exemple), et ensuite votre nom d’utilisateur·ice
comprend également le nom de l’instance, pour que vos contacts puissent
vous retrouver.

Par exemple, on écrit pas à « bidule » mais à « bidule@protonmail.com »,
du moins tant qu’on a pas « Bidule » enregistré dans son carnet d’adresse.

C’est pareil sur Mastodon : il faudra donc vous abonner à @ucl@todon.nl
et à @alternativelibertaire@framapiaf.org une fois que vous aurez
rejoint le réseau !



