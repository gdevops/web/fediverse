.. index::
   ! Flux web

.. _fed_rss_2022_11_18:

============================================================================
2022-11-18 Le retour des **flux web** (et pas que RSS) par Arthur Perret
============================================================================

- https://www.arthurperret.fr/blog/2022-11-18-le-retour-des-flux-web-et-pas-que-rss.html
- https://www.arthurperret.fr/feed.xml
- https://letab.li/blog/2022_05_outils/
- :ref:`mastodon_rss`

1. Ce billet accompagne la mise en ligne de ma page consacrée aux flux web
===========================================================================

Ce billet accompagne la mise en ligne de ma page consacrée `aux flux web <https://www.arthurperret.fr/cours/flux-web.html>`_

Je serai bref, tout est dans le titre : on parle souvent de **flux RSS**
pour désigner les flux web en général, et il faudrait arrêter.


2. Les flux web sont un ensemble de techniques qui servent à communiquer de l’information et à suivre des sources d’information
================================================================================================================================

Les **flux web** sont un ensemble de techniques qui servent à communiquer
de l’information et à suivre des sources d’information.

RSS est l’un des plus anciens formats de flux web, et certainement le
plus connu.
Mais il est concurrencé depuis des années par Atom, et récemment d’autres
formats sont apparus (le microformat h-feed, JSON Feed).

Parler de « flux RSS » pour désigner tout type de flux web, c’est donc
offrir une représentation faussée de la réalité technique (et faire une
injustice à Atom, qui est un chouette format).

3. Vous allez me dire, c’est un peu comme demander aux gens d’arrêter de dire « coton-tige », « frigo » ou « scotch »
========================================================================================================================

Vous allez me dire, c’est un peu comme demander aux gens d’arrêter de
dire « coton-tige », « frigo » ou « scotch » Entre autres marques
utilisées comme noms.

Je ne suis pas d’accord : combien d’étudiants de premier cycle en
information-communication connaissent l’expression **flux RSS** ?

La question est plutôt de savoir à quoi ça servirait d’insister pour
utiliser plutôt « flux web ».

4. Personnellement, je vois deux raisons
=========================================

5. La première est didactique
==============================


La première est didactique. Quand on aborde un sujet technique, il me
paraît indispensable de parler autant des principes généraux que des
exemples particuliers.

Personnellement, j’aime bien partir des principes::

    Le blog de Kaspar Etter, Explained from First Principles, est un exemple
    extrême de cette approche. Il va beaucoup plus loin sur chaque sujet
    que ce que je fais avec mes pages Cours, mais l’idée est là.

L’objectif est de donner à mes interlocuteurs la possibilité de s’abstraire
du marketing des outils, et de choisir par eux-mêmes les solutions qui
leur conviennent.

C’est la même chose que partir des besoins quand on `explique comment
choisir un outil numérique <https://letab.li/blog/2022_05_outils/>`_

**Parler de flux web obéit à cette logique : cela aide à mieux situer de quoi on parle**.

6. L’autre raison est politique, et conjoncturelle
=====================================================

L’autre raison est politique, et conjoncturelle : avec la multiplication
des critiques à l’encontre des réseaux sociaux, le contexte est favorable
à une résurgence des flux web.

Face à l’économie de l’attention et des plateformes, les flux font partie
des outils qui permettent de décloisonner le Web et de préserver un peu
d’autonomie.

7. Contrairement à ce que beaucoup de gens pensent, la disparition de Google Reader en 2013 n’a pas tué l’écosystème des flux
=================================================================================================================================

Contrairement à ce que beaucoup de gens pensent, la disparition de
Google Reader en 2013 n’a pas tué l’écosystème des flux.

La majorité des outils de création de sites web génèrent automatiquement
des flux, la source ne s’est donc jamais tarie.
Et le vide laissé par Google a été comblé : l’offre logicielle s’est
enrichie et diversifiée. Les flux web sont donc prêts pour un retour en
grâce.

Mais il faut s’efforcer de faire prospérer tout ça par en bas. C’est le
moment d’être à la fois précis et stratégiques dans les mots comme dans
les actes…

8. Récemment, j’ai partagé sur Twitter ma règle n°1
=========================================================

Récemment, j’ai partagé sur Twitter ma règle n°1 pour préserver un usage
professionnel apaisé des réseaux sociaux : **privilégier les flux web
quand ils existent**.

Dans la foulée, j’ai lu un billet fort à propos de Chris Aldrich, qui
suggère d’utiliser le mot-clé #FeedReaderFriday pour animer des échanges
autour de ces questions.

Alors, constatant qu’il me restait une petite réserve de pinaillerie
terminologique après mon billet sur « prépublier », preprint et compagnie, voilà.

À vous !
=========

À vous !

