
.. _vague_twitter_2022_11_18:

========================================================
2022-11-18 Twitter à feu et à sang
========================================================

- https://www.nytimes.com/2022/11/18/technology/elon-musk-twitter-workers-quit.html
- https://www.journaldugeek.com/2022/11/18/twitter-a-feu-et-a-sang-bientot-la-fermeture-du-reseau/


https://framapiaf.org/@Pouhiou/109365908220391280, Seul·es le petit millier d'employé·es qui ont besoin de leur poste pour garder leur visa aurait cliqué "oui"
=================================================================================================================================================================

- https://framapiaf.org/@Pouhiou/109365908220391280

@zaclys @leopnk @bieregougnoux En très très résumé, EM avait envoyé un
ultimatum à ses 3-4000 employé·es restant·es::

    "À partir de maintenant vous allez bosser violent, ou bien vous
    prenez vos trois mois et vous vous barrez. Cliquez ici pour accepter mon défi"

Seul·es le petit millier d'employé·es qui ont besoin de leur poste pour
garder leur visa aurait cliqué "oui".

La rumeur spécule que y'a plus personne pour tenir la baraque.


https://mastodon.online/@EUPASTA/109365847062528179, The man who was in charge of managing badge access to Twitter offices was laid of
==========================================================================================================================================

- https://mastodon.online/@EUPASTA/109365847062528179

OMG #riptwitter #twitterdown 😂
The man who was in charge of managing badge access to Twitter offices was laid of.

Elon the called him and asked if I could come back to help them regain
access to HQ as they shut off all badges and accidentally locked themselves out.


.. figure:: images/em_out_of_twitter.png
   :align: center


https://octodon.social/@cwebber/109365521031839860, "ActivityPunk" technology to build dangerous "fetishverse" network  😂
============================================================================================================================

- https://octodon.social/@cwebber/109365521031839860

FOX NEWS AT 11: Elon Musk's Twitter sabotaged by transgender hacker
activists using "ActivityPunk" technology to build dangerous "fetishverse" network


https://librosphere.fr/objects/d95c5ff1-c4d3-42dd-90ea-cf4bea1258ad, Toute ressemblance avec le piaf bleu serait fortuite…
==============================================================================================================================

- https://librosphere.fr/objects/d95c5ff1-c4d3-42dd-90ea-cf4bea1258ad


.. figure:: images/em_un_genie.png
   :align: center
