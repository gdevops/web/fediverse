
.. index::
   pair: RSS; Cloud
   ! RSS cloud

.. _rss_cloud_2022_11_16:

===================================
2022-11-16 Fluw web RSS cloud
===================================

- https://mastodon.social/@davew/109355827562913842
- http://home.rsscloud.co/


https://mastodon.social/@davew/109355827562913842
==================================================

- https://mastodon.social/@davew/109355827562913842

To all my friends the feed producers in MastodonLand, I want you to consider
adding rssCloud support to your RSS feeds so we can get instant updates.

We have that hooked up with micro.blog and it's wonderful.

My updates are over there before I have a chance to reload the timeline
over there. And it's not a big deal to do.

Here's the spec: http://home.rsscloud.co/

It's part of RSS 2.0. WordPress supports it, and of course so does my new product http://feedland.org
