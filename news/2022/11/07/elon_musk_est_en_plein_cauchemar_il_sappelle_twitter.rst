
.. _musk_news_2022_11_07:

==========================================================================
2022-11-07 Elon Musk est en plein cauchemar : il s’appelle Twitter
==========================================================================

- https://www.numerama.com/tech/1172110-elon-musk-est-en-plein-cauchemar-il-sappelle-twitter.html

Conseil : n'achetez pas un réseau social


Propriétaire de Twitter depuis le 27 octobre, Elon Musk est de plus en
plus controversé. Les polémiques s’enchaînent, sans que le milliardaire
ne fasse rien pour calmer le jeu.

Depuis qu’Elon Musk a annoncé son intention de racheter Twitter, tout le
monde avertit l’homme d’affaires des risques que représente la gestion
d’un réseau social.
Habitué à toucher au domaine du spectaculaire avec Tesla et le SpaceX,
le milliardaire a décidé de jouer avec le feu en s’attaquant à l’une des
zones les moins régulées d’Internet, généralement accusée par tous les
camps de tous les maux du monde.

Malgré cela, Elon Musk est bel et bien allé au bout du rachat de Twitter
le 27 octobre (après avoir tenté de se retirer pendant plusieurs mois, certes).

Comment se sont passés les premiers jours de l’ère 2 de Twitter ?

Les plus optimistes pouvaient imaginer qu’Elon Musk calmerait le jeu en
déléguant, mais le milliardaire semble être en mission destruction.

Même dans les pires scénarios, pas grand monde n’aurait imaginé Elon Musk
créer autant de chaos.

En un week-end (du 4 au 7 novembre), il s’est passé plus de choses chez
Twitter qu’en plusieurs mois habituellement.

Elon Musk découvre l’enfer qu’est Twitter
==========================================

Dès les premiers jours de son rachat, Elon Musk a annoncé la couleur.
En licenciant toute la direction du réseau social et en se nommant à la
tête de l’entreprise, Elon Musk a voulu faire comprendre qu’il était
désormais seul aux commandes.

Il s’est alors mis à interroger ses fans sur ce qu’il fallait faire en
premier, en soumettant plusieurs idées loufoques comme la séparation
de Twitter en deux (version de droite et version de gauche), la fin de
la limite de 280 caractères, le retour de Vine ou la création d’une
commission à la modération, un peu comme Facebook, composée de personnes
avec des opinions diverses (y compris extrêmes, comme il l’a promis à
des militants).

Ces premières initiatives pouvaient sembler confuses, mais elles
n’étaient qu’un apéritif avant le début du vrai chaos.

Le 1er novembre, Elon Musk a annoncé son intention de faire payer les
utilisateurs Twitter 8 dollars par mois, en échange d’une certification
et d’une meilleure mise en avant par l’algorithme.

Son idée est simple : il faudra payer pour être vu sur Twitter, au risque
d’être invisibilisé par l’algorithme.
Cette décision lui a évidemment valu de nombreuses critiques, certains
lui reprochant de vendre un badge qui sert normalement à vérifier la
crédibilité d’une information.
Elon Musk répond généralement par la provocation, au lieu de tenter de dialoguer.

Malheureusement, le débat sur la fin de la gratuité de Twitter n’est pas
la seule chose à poser problème en ce moment.
Elon Musk semble être rentré dans une spirale infernale et provoque
du désordre partout où il passe.

Licenciements, annonceurs, usurpations d’identité, exclusions définitives et critique du journalisme… le week-end fou d’Elon Musk
===================================================================================================================================

Les jours passent et Elon Musk semble de plus en plus dévoré par Twitter,
que l’on a toujours su maudit. Il suffit de voir tout ce qu’il s’est
passé du 4 au 7 novembre (et encore, on en oublie sans doute) pour
comprendre l’ampleur de l’enfer d’Elon Musk :

- Elon Musk a licencié 50 % des effectifs de Twitter par mail, après avoir
  désactivé tous les badges des employés par mesure de sécurité.
- Certains des employés virés ont été rappelés, puisqu’Elon Musk s’est
  rendu compte qu’il avait besoin d’eux pour des fonctions qu’il venait
  juste d’annoncer (comme les tweets plus longs).
- Elon Musk s’est plaint d’une baisse importante des revenus de Twitter
  depuis son arrivée, puisque les annonceurs céderaient à la pression
  des « activistes ».



