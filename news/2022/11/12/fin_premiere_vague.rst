
.. _vague_1_2022_11_12:

================================================================
2022-11-12 Mastodon, fin de (première) partie ? – Framablog
================================================================

- https://framablog.org/2022/11/12/mastodon-fin-de-premiere-partie/


https://pouet.chapril.org/@Greguti/109330066710492608
============================================================

"Ce n’est pas entièrement la faute des personnes de #Twitter.

On leur a appris à se comporter d’une certaine manière.

À courir après les #likes et les #retweets. À se mettre en valeur.
À performer. Tout ce genre de choses est une malédiction pour la plupart
des personnes qui étaient sur #Mastodon il y a une semaine."

Mastodon, fin de (première) partie ? – Framablog

