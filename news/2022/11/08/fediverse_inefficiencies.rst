
.. _fediverse_inefficiencies_2022_11_08:

===========================================================================
2022-11-08 The Fediverse is Inefficient (but that's a good trade-off)
===========================================================================

- https://berk.es/2022/11/08/fediverse-inefficiencies/


Let's address the mammoth in the room: the fediverse, the network of
mastodon servers, is very inefficient.

In this post I'll show why it is inefficient and why that isn't a problem.

A great analogy to explain this with is growing food.

