
.. _takahe_news_2022_11_15:

===================================================
2022-11-15 Twitter, ActivityPub and The Future
===================================================


Introduction
===============

Twitter is - was - such a unique place.

Somewhere where you can have the President of the United States coexist
with teenagers writing fan fiction; where celebrities give personal
insights into their lives while government departments post memes about
public safety; the place that gave us @Horse_ebooks and @dril.

The "Fediverse", with Mastodon at its helm, is not this. It doesn't seem
to want to be, and I honestly think that's fine - as many thinkpieces
have recently said, the age of global social media might just be over.

And given the effect it's had on the world, maybe that's alright after all.

But there is still a void to fill, and as someone who enjoyed Twitter
most at its "medium" size, I think the ActivityPub ecosystem is well-placed
to grow into such a space.

But first, I think there's some important things we have to discuss about it.



Moving Forwards
====================

So, what do I make of all this? Well, I think any effort to make a brand
new social network that isn't based on ActivityPub is probably not going
to work (apart from Cohost, who to their credit seem to have found a
niche and I hope they do well by it - though I'd love them to add AP support).

Instead, I think there's some positive moves that can be made:

- Shore up the current Fediverse as it copes with the current influx
  from Twitter, and a potentially even larger one if it falls over
  (as I write this, it certainly seems like it is about to).
- Find good short-term solutions to pool moderation and defederation
  decisions (as has already been happening with the #fediblock hashtag, for example)
- Get at least one more user-friendly ActivityPub server software off
  the ground that plays well with Mastodon and has low friction for people used to it.
- Think about how to have dedicated nonprofits, with actual staff,
  who can run larger servers and help give governments, schools, etc. easier access.
- Work together to define improvements on top of ActivityPub to share
  moderation and defederation activities between servers, and eventually
  use that to build a v2 protocol that keeps the model, but is more
  efficient to transport and has less undefined parts.

Such a list is as powered by hope as it is by logic, but from my twenty
years maintaining Open Source software, I believe that hope and vision
is an incredibly powerful force that should never be underestimated.

I think the likely endgame for ActivityPub is either a set of very large,
potentially commercial, servers, or to fade into relative irrelevance
like IRC (There are tens of us who use it! Tens!).
There's still a path out of this, though, where we somehow establish a
world where there's several sustainable, medium-scale, social networks,
run for the good of society rather than profit.

We may be seeing the end of the "social media era" (congratulations to
Reddit, by the way, for being the only apparent survivor) - but there
are good things to take from it, as we have with every other era of communication.

For my part, I'm building Takahē as my springboard to try and effect
some change. I doubt it will become nearly as popular as Mastodon or
even the other, less popular server software, but I figure I can at
least push some innovative ideas by showing they're possible, and maybe
inspire others along the way. I'll also be listening to others who have
been working on these spaces for a while - while I'm OK at building
technology, it's been a decade since I moderated and administered a
large public space!

And hey, if it does take off, so much the better. Well, as long as I
find some fellow maintainers earlier on this time.
