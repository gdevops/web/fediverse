
.. _eugene_rochko_2022_11_21:

========================================================================================================
2022-11-21 Qui est Eugen Rochko, le jeune créateur du réseau social Mastodon ? par Marine Bourrier
========================================================================================================

- https://www.lemonde.fr/m-le-mag/article/2022/11/21/qui-est-vraiment-eugen-rochko-le-jeune-createur-de-mastodon_6150831_4500055.html

**La reproduction totale ou partielle d’un article, sans l’autorisation
écrite et préalable du Monde, est strictement interdite**.
