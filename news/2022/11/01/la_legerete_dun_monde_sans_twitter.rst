.. index::
   pair: Ploum ; La légèreté d’un monde sans Twitter

.. _ploum_2022_11_01:

==================================================================================================================
2022-05-11 **La légèreté d’un monde sans Twitter** par Ploum
==================================================================================================================

- https://ploum.net/la-legerete-dun-monde-sans-twitter/


Introduction
============

Il y a un peu moins d’un an, j’ai supprimé mon compte Twitter.

Un compte vérifié avec la célèbre icône bleue, suivi par près de 7000
autres comptes Twitter.

Si ce n’est pas exceptionnel, ce compte n’en était pas moins relativement
« influent » sur l’échelle Twitter.
J’ai pourtant décidé de tenter l’expérience de m’en passer complètement,
pour voir.

Je savais que j’avais un an pour faire marche arrière. Durant un an,
mon compte serait « réactivable » avant d’être définitivement supprimé.
Il me reste donc quelques semaines pour changer d’avis.

Et pourtant, cela ne me viendrait pas à l’esprit.

...

La migration vers Mastodon
=============================

Je souris de la naïveté de certains utilisateurs qui s’indignent de
l’arrivée d’Elon Musk à la tête de Twitter.

C’était pourtant clair depuis le début, non ? Vous êtes des « utilisateurs ».
Vous êtes une marchandise, vous créez la valeur de l’entreprise, que ce
soit pour Elon Musk ou un autre.
Vous êtes les pigeons et votre indignation ne fait qu’alimenter les débats,
les interactions et donc les intérêts publicitaires.

Il n’y a pas de bonne manière d’utiliser un réseau propriétaire.

En créant un compte, nous acceptons d’être utilisés, manipulé et que
chacune de nos interactions y soit désormais monétisée.

Il y a déjà 5 ans, je tentais de promouvoir Mastodon, une alternative libre
et décentralisée à Twitter.
Je lis souvent des remarques comme quoi c’est beaucoup plus compliqué.

Non.

C’est juste différent. Si tu n’arrives pas à utiliser Mastodon, c’est
que tu n’en as tout simplement pas envie.

C’est plus facile de manger un burger au Macdo qu’un plat équilibré avec
des légumes. C’est plus facile de balancer ses déchets dans un parc
plutôt que de faire du tri.
L’argument de la facilité n’en est pas un. Le monde se modèle selon
l’énergie que nous y mettons.

Il faut créer un compte avec un mot de passe et tout ? Sur Twitter aussi.
C’est juste que tu as l’habitude. C’est juste que tu t’es connecté sur
un serveur Mastodon, que tu as découvert que tu avais 0 follower, que
tu n’avais plus la petite icône bleue, que tu ne savais plus « promouvoir
tes tweets », que tu ne voyais plus des likes s’afficher en direct sous
tes messages.
Que tu n’es plus revenu et que donc tu as oublié ton mot de passe.

Que c’est plus facile d’accuser le logiciel libre d’être compliqué plutôt
que d’affronter sa propre vacuité.

Si tu n’as pas l’envie d’apprendre à utiliser Mastodon, c’est compréhensible.
Personne ne te force. Mais n’accuse pas la plateforme.

Twitter est très bon pour te faire croire que tu es un utilisateur important.
Mais sur le Fediverse, le réseau décentralisé auquel participent les
serveurs Mastodon, il n’y a pas d’utilisateurs, encore moins des importants.

**Il y a juste des personnes qui sont toutes sur le même pied d’égalité**.

Tentez l’expérience
=====================

Si votre morale personnelle réprouve ce qu’est ou ce que devient Twitter,
je ne peux que vous inviter à tenter l’expérience de supprimer votre compte.
Rappelez-vous : vous avez un an pour changer d’avis.

Faites fi de ces conversations tellement importantes, de cette communauté
que vous ne pouvez pas « abandonner », de ces ennemis virtuels qui verront
votre départ comme une victoire. Tentez simplement de vous aligner avec
vos propres valeurs morales. Juste pour voir.

Supprimer votre compte est également la seule et unique manière de
protester, de toucher l’entreprise là où ça lui fait mal.

Bien sûr, vous pouvez aussi venir sur Mastodon. Mais ce n’est pas nécessaire.

C’est même peut-être contreproductif. Si vous étiez accroc à Twitter, vous
serez tenté de voir dans Mastodon un substitut.
Il est sans doute préférable de se sevrer de Twitter avant de découvrir
autre chose.

Bonus
======

Installez l’extension libredirect dans votre navigateur, de manière à
pouvoir continuer à consulter Twitter à travers l’interface Nitter
(vous pouvez même suivre les comptes Twitter dans votre lecteur RSS).

Sur Android, utilisez Fritter.

=> https://libredirect.codeberg.page/ Libredirect pour Chrome/Firefox
=> https://f-droid.org/en/packages/com.jonjomckay.fritter/ Fritter pour Android





