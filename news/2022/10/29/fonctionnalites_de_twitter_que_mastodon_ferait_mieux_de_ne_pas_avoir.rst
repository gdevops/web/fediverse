
.. _fed_news_2022_11_04:

=========================================================================================
2022-10-29 4 fonctionnalités de Twitter que Mastodon ferait mieux de ne pas avoir
=========================================================================================

- https://interventions-numeriques.fr/blog/2022/11/01/4-fonctionnalites-de-twitter-que-mastodon-ferait-mieux-de-ne-pas-avoir/


Traduction à partir de l’article de Scott Feeney, le 29 octobre 2022.

Maintenant qu’un milliardaire narcissique a acheté Twitter et qu’il va
probablement le rendre nazi (ou le ruiner d’une autre manière), de
nombreuses personnes se renseignent sur Mastodon, un réseau social
open-source qui ne peut être contrôlé par aucune organisation. Voici mon compte.

Au premier abord, Mastodon ressemble à un clone pur et simple.

Il répond aux Tweets de 280 caractères de Twitter par des Toots de 500
caractères.
Sur Twitter, vous pouvez liker ou retweeter vos abonnés ; sur Mastodon,
vous pouvez liker ou booster vos abonnés. Jusqu’ici, tout est familier.


