.. index::
   pair: Ploum ; L’histoire du logiciel : entre collaboration et confiscation des libertés

.. _ploum_2022_05_11:

==================================================================================================================
2022-05-11 **L’histoire du logiciel : entre collaboration et confiscation des libertés** par Ploum
==================================================================================================================

- https://ploum.net/lhistoire-du-logiciel-entre-collaboration-et-confiscation-des-libertes/

Le concept même de logiciel n’est pas évident. Comme le rappelait Marion Créhange,
la première titulaire d’un doctorat en informatique en France, la manière
d’influencer le comportement des premiers ordinateurs était de changer
le branchement des câbles.

Un programme était littéralement un plan de câblage qui nécessitait de
s’arracher les mains sur des fils.

Petit à petit, les premiers informaticiens ont amélioré la technologie.
Ils ont créé des ordinateurs « programmables » qui pouvaient être modifiés
si on leur fournissait des programmes au format binaire, généralement
des trous sur des cartes en carton qui étaient insérées dans un ordre
précis.
Il fallait bien évidemment comprendre exactement comment fonctionnait le
processeur pour programmer la machine.


Les processeurs ont ensuite été améliorés pour comprendre des instructions
simples, appelées `assembleur <https://asm.developpez.com/actu/338122/Kathleen-Booth-pionniere-de-l-informatique-britannique-inventrice-du-langage-assembleur-nous-a-quitte-a-l-age-de-100-ans-Elle-a-contribue-au-developpement-de-trois-ordinateurs-ARC-SEC-et-APEXC/>`_. Les informaticiens pouvaient réfléchir
avec ce langage qui donnait des instructions directes au processeur.

Cependant, ils utilisaient d’autres formalismes qu’ils appelèrent « langage de programmation ».
Les programmes étaient écrits sur papier dans ce langage puis, grâce à
des tableaux, traduits en assembleur.

...

Techniquement, un logiciel libre est open source et un logiciel open source
est libre. Les deux sont synonymes.
Mais, philosophiquement, le logiciel libre a pour objectif de libérer
les utilisateurs là où l’open source a pour objectif de produire des
meilleurs logiciels.
Le mouvement open source, par exemple, accepte volontiers de mélanger
logiciels libres et propriétaires là où Richard Stallman affirme que
chaque logiciel propriétaire est une privation de liberté.

Développer un logiciel propriétaire est, pour RMS, hautement immoral.

