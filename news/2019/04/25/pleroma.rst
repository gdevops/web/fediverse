
.. _pleroma_2019_04_25:

==========================================================
Le logiciel Pleroma, pour communiquer sur le fédivers
==========================================================


- https://www.bortzmeyer.org/pleroma.html


Le fédivers (ou fediverse, ou fédiverse) est l'ensemble des systèmes qui
communiquent sur l'Internet en échangeant des messages typiquement assez
courts, dans une logique de microblogging et en utilisant en général
le format Activity Streams et le protocole ActivityPub.

Il existe plusieurs logiciels qui permettent de participer au fédivers,
et cet article va parler de mon expérience avec Pleroma.

