.. index::
   ! minions

.. _minions_2025_03_04:

===================================================================
2025-03-04 minions signifie en français  "larbins", "laquais"
===================================================================


- https://framapiaf.org/@millerebonds/114103723633879169

@politipet 

Ma méchanceté envers les gens n'ayant pas quitté X (ou désinvesti, 
ou installé autre chose) ne s'arrête pas, **maintenant je les traite de 
"minions" en vrai**.

Attention en anglais ça signifie pas **"mignon" mais "larbin", "serviteur", "laquais"**

Cette théorie philosophique exagérée du suiveur qui tombe toujours amoureux 
du plus dangereux du plus cruel du plus avide de pouvoir est précieuse 
actuellement, car **on a idiocratie en vrai vs les minions en vrai**...
