

==========================================
2025-03-04 **Bluesky versus mastodon**
==========================================


- https://colter.social/@nicolasvivant/114103204402863538

Rappel #Bluesky : nous sommes sur un serveur américain, propriété d'américains, 
hébergé chez Amazon aux États-Unis et dont la messagerie est gérée par Google.

→ Sur Bluesky, je soutiens Bezos et donc Trump.

Sur #Mastodon, je suis sur un réseau international depuis un serveur hébergé 
par une structure publique à 5km de chez moi.

→ Sur Mastodon, je soutiens le service public et les associations.

Faites vos choix en connaissance de cause.
