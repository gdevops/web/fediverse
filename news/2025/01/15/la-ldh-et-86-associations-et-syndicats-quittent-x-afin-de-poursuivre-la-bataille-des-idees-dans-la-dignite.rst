.. index::
   pair: HelloQuitteX; La LDH et 86 associations et syndicats quittent X afin de poursuivre la bataille des idées dans la dignité (2025-01-15)

.. _hello_quitte_x_ldh_2025_01_15:

=================================================================================================================================
2025-01-15 **La LDH et 86 associations et syndicats quittent X afin de poursuivre la bataille des idées dans la dignité**
=================================================================================================================================

- https://www.ldh-france.org/la-ldh-et-86-associations-et-syndicats-quittent-x-afin-de-poursuivre-la-bataille-des-idees-dans-la-dignite/
- https://www.ldh-france.org/wp-content/uploads/2025/01/CP-LDH-Hello-quitte-X-15-01-2024-1.pdf
- :ref:`hello_quitte_x`

Communiqué LDH
====================

Dans une tribune parue hier dans le journal Le Monde, 86 associations et syndicats
annoncent quitter collectivement le réseau social X (ex-Twitter) le 20 janvier
prochain, date de l’investiture de Donald Trump. En soutenant l’initiative
« Hello quitte X », ils appellent également citoyennes et citoyens à un
départ massif de la plateforme.

La LDH (Ligue des droits de l’Homme) a cofondé le collectif HelloQuitteX en
partenariat avec le Centre national de la recherche scientifique (CNRS) et appelé
à signer la tribune initiée par Emmaüs France. Elle a aussi décidé de ne plus
produire de contenus sur le site X en raison tant du paramétrage des algorithmes,
qui favorisent la prolifération des contenus haineux et la circulation des
théories complotistes et climatosceptiques, que de l’absence de modération.

La LDH estime qu’X n’est plus la rue numérique (même imparfaite) qu’elle
avait pu être, en raison de la manipulation des conditions de possibilités du
débat public par Elon Musk et, de ce fait, de l’invisibilisation des principes
qu’elle défend en matière de défense des droits humains et d’égalité.

Il s’agit aussi d’une mobilisation fondamentale pour la démocratie, qui
implique la solidarité de tous les acteurs de la société civile et de la
politique qui partagent les mêmes valeurs. Cela implique de favoriser et de
plaider pour des espaces numériques qui respectent et assurent la protection
du pluralisme, du débat respectueux et de la raison.

La LDH invite donc toutes celles et tous ceux qui partagent ces valeurs et cette
action à quitter X aussi massivement que possible le 20 janvier 2025 et à les
suivre sur Mastodon ou Bluesky, quelles que soient les modalités de ce départ.
