.. index::
   ! Petit guide du Fediverse 2025 par Alysson

.. _alysson_2025_01_16:

===============================================================
2025-01-16 **Petit guide du Fediverse 2025** par Alysson
===============================================================

- https://alyve.be/blog/petit-guide-du-fediverse-2025/


Auteure
=============

- https://alyve.be/auteure/alysson/


Bonjour à tou·te·s ! 👋

Bienvenue sur ce guide autour du Fediverse. J’imagine que si vous lisez ceci, 
c’est le sujet vous intéresse et, peut-être même, que ça vous intimide. 

C’est totalement compréhensible, ne vous en faites pas. Une fois que vous 
aurez terminé ce guide, vous pourrez l’expliquer à votre tour.

Moi, c’est Alysson, je suis développeuse web. Mon travail consiste à créer 
et/ou à maintenir des applications web. 
Mon expérience avec le Fediverse est plutôt grande ; au tout début de Mastodon 
sur l’instance Framapiaf de Framasoft, ensuite Hostux.social et en janvier 2021, 
migration sur ma propre instance Pleroma. 

Mon travail avec ce guide est de tout vulgariser pour que vous puissiez 
comprendre comment tout ça fonctionne. 💪

