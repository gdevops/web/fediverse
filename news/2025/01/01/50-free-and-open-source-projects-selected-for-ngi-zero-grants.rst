

.. _ngi_zero_grant_2025_01_01:

===================================================================================
2025-01-01 50 Free and Open Source Projects Selected for NGI Zero grants
===================================================================================

- https://nlnet.nl/news/2025/20250101-announcing-grantees-June-call.html


Announce by nlnet
======================

- https://nlnet.nl/news/2025/20250101-announcing-grantees-June-call.html

Happy 2025 everyone! 

On this first day of the fresh new year we are happy to announce 50 project 
teams were selected to receive NGI Zero grants. 

We are welcoming projects from 18 countries involving people and organisations 
of various types: individuals, associations, small and medium enterprises, 
foundations, universities, and informal collectives. 

The new projects are all across the different layers of the NGI technology 
stack: from trustworthy open hardware to services & applications which 
provide autonomy for end-users.

The 50 free and open source projects were selected across two funds. 

19 teams will receive grants from the NGI Zero Commons Fund, a broadly themed 
fund that supports people working on reclaiming the public nature of the internet. 

The other 31 projects will work within NGI Zero Core which focuses on strengthening 
the open internet architecture. 

Both funds offer financial and practical support. 

The latter consisting of support services such as accessibility and security 
audits, advice on license compliance, help with testing, documentation or UX design.

Announce on mastodon
========================

- https://mastodon.social/@box464/113759177011721699

NLNet announces newly funded projects, including quite a few related to 
ActivityPub.


linkblocks federated link sharing platform
-------------------------------------------

- @raffomania (https://mstdn.io/@raffomania)  for the linkblocks federated link sharing platform

  - https://nlnet.nl/project/linkblocks/
  - https://github.com/raffomania/linkblocks
  
federated 3d model sharing
--------------------------------
  
- @manyfold (https://3dp.chat/@manyfold) - federated 3d model sharing

  - https://nlnet.nl/project/Manyfold-Discovery
  - https://manyfold.app/
  
  
federated event sharing platform
-------------------------------------
  
- @mobilizon (https://framapiaf.org/@mobilizon)  for their federated event sharing platform

  - https://nlnet.nl/project/Empowering-Mobilizon
  - https://joinmobilizon.org/fr/
  
  
Announce
+++++++++++

- https://social.tchncs.de/@Blort/113754803645149254
  
Creating a governance structure to hit Mobilizon's most important goals and 
priorities. 

We really need an open, privacy respecting way to get people offline and 
building community now more than ever. 

Governance may not be sexy, but this will make a huge difference! to funding, 
planning and increasing the features and reach of Mobilizon!
  
  
storage solution for federated data
-----------------------------------------
  
- @activitypods (https://fosstodon.org/@activitypods) is a storage solution for federated data

  - https://nlnet.nl/project/ActivityPods-3.0/
  - https://activitypods.org/
  - https://github.com/activitypods/activitypods
  
  
The Oaken project
--------------------------
  
- @spritely (https://social.coop/@spritely) and their Oaken project (fedi adjacent and happy for them)

  - https://nlnet.nl/project/SpritelyOaken/
  - https://spritelyproject.org/#oaken

