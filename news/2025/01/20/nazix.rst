.. index::
   pair: nazix ; 2024-01-20


.. _nazix_2025_01_20:

==================================================================================================
2025-01-20 **nazix** #20janvier opération HelloQuitteX le 20 Janvier, je quitte Twitter !
==================================================================================================

- https://www.helloquitx.com/
- https://piaille.fr/@HelloQuitteX
- https://mastodon.social/@helloQuitX
- https://helloquittex.com/-Je-cherche-dans-la-FAQ-.html

David Chavalarias le 13 novembre 2024
==========================================

- https://x.com/chavalarias/status/1856607144759505360
- https://editions.flammarion.com/toxic-data/9782080274946 (Toxic Data Comment les réseaux manipulent nos opinions)


Le #20janvier, #Trump est investi, #Musk n'a plus aucune limite dans la 
désinformation et le complotisme et X devient la principale machine de 
propagande US. 

Pour savoir qui est Musk et de quoi il est capable: https://www.youtube.com/live/FhFxlZzptys?t=3535s

.. figure:: images/chavalarias_2024_11_13.webp



Articles
===========

Contre Elon Musk, quittons X le 20 janvier, par Basta!
-------------------------------------------------------------

- https://basta.media/contre-elon-musk-quittons-x-le-20-janvier

Elon Musk a aidé Trump, il soutient désormais les partis d’extrême droite 
en Europe. Les raisons pour quitter le réseau X/ex-twitter, détenu par Musk, 
ne manquent pas. 
À Basta!, nous avons décidé de rejoindre le mouvement #HelloQuitteX.

Ceux qui quittent twitter
===============================

- https://bonpote.com/bon-pote-quitte-twitter-ce-nest-quun-au-revoir/


Nous quittons Twitter par La Quadrature du Net
-------------------------------------------------------

- https://www.laquadrature.net/2025/01/20/nous-quittons-twitter/

Nous avons pris la décision de quitter X, anciennement Twitter, ce lundi 20 janvier 2025. 
Cette décision est mûrement réfléchie ; Twitter encourage depuis longtemps 
les discours de haine et le harcèlement mais, ces derniers mois, il est devenu 
l’espace d’expression privilégié de l’extrême droite.

Nous nous joignons donc, comme des milliers de personnes, à l’initiative 
`HelloQuitX <https://www.helloquitx.com/>`_ afin de réamorcer la désertion des réseaux sociaux commerciaux 
centralisés vers de meilleurs espaces. 

Elle facilite notamment le départ en Twitter en proposant un outil de migration.
