

.. _jocelyn_etienne_2025_02_05:

=========================================================================================================================
2025-02-25 Decentralized social media for scientists: Big Brother doesn't have to boss you around by Jocelyn Etienne
=========================================================================================================================

- https://zenodo.org/records/14908184
- https://scicomm.xyz/@jocelyn_etienne/114044236835841491


The resilience of networks, such as the electrical grid or communication 
networks, requires to avoid bottleneck nodes whose failure compromises the 
whole network. 

This is true of the physical infrastructure, but also of the software and 
governance architecture of networks. 

Historically, the world wide web has developed as a decentralised network, 
defined by a communication protocol, but made of independently hosted websites 
connected by hyperlinks. 

Social media such as Twitter, however, have developed under a centralised 
governance, making their users dependent on central decisions. 

The takeover of Twitter by Elon Musk has brought increased interest for 
WWW-like alternatives, which are made of a myriad of independently-run but interconnected services.
 
The Fediverse is the global network that all these services form together. 

In this talk, I will give both a global perspective on the organisation of 
this decentralised network and give a flavour of what a user may experience 
when using it, with an attention on the interests of a scientific researcher.
 
This talk is not be about why it matters but rather showing what the 
differences are, for you to decide what is best to do or not to do.
 
Disclaimer : these contents do not correspond to my research field, and 
are merely the perception of an interested scientist of those social media.
 
A few links: example of instances 
======================================

Generalist mastodon instances
-----------------------------------

- https://mastodon.social : largest, 300k users
- https://piaille.fr : large French instance
- https://pouet.chapril.org : smaller, managed by free software nonprofit l'April 

Science oriented ones, managed by volunteers:
------------------------------------------------------

- https://mathstodon.xyz : with \LaTeX support
- https://fediscience.org : large-ish, registration limited to "researchers 
  currently publishing in scientific journals" (or expecting to, case-by-case acceptability decisions)
- https://sciences.re : intended for the French-speaking higher education and research community
- https://scicomm.xyz : a small instance ran by volunteers

Science, institutional
---------------------------

- https://biologists.social : by UK learned society co of biologists, everyone welcome though.
- https://universites.social : only for people with an eduGAIN account, so staff of higher education/research in France.


We can chat about it... on the Fediverse : @jocelyn_etienne@scicomm.xyz
