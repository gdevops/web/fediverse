
.. _nextcloud_20:

============================================================================================================
Nextcloud Hub 20 debuts Dashboard, unifies search and notifications, integrates with other technologies
============================================================================================================

.. seealso::

   - https://nextcloud.com/blog/nextcloud-hub-20-debuts-dashboard-unifies-search-and-notifications-integrates-with-other-technologies/

.. contents::
   :depth: 3


Description
============


At the Nextcloud Conference in Berlin, Nextcloud CEO Frank Karlitschek
introduced a big release of Nextcloud Hub with a wide range of improvements
in the core file handling, communication and groupware areas.

In a major shift, integration with over a dozen third party platforms
including Moodle, Microsoft Teams, Gitlab, Slack and many more was
announced.

Users can download the latest release from our website or use the beta
channel of our updater to get the new release.

The three biggest features we introduce are:

- 🏁 Our new dashboard provides a great starting point for the day with
  over a dozen widgets ranging from Twitter and Github to Moodle and
  Zammad already available
- 🔍 Search was unified, bringing search results of Nextcloud apps as
  well as external services like Gitlab, Jira and Discourse in one place
- 🗨 Talk introduced bridging to other platforms including MS Teams,
  Slack, IRC, Matrix and a dozen others


There is more
=================

- 📢 Notifications and Activities were brought together, making sure you won’t miss anything important
- We added a ‘status’ setting so you can communicate to other users what you are up to
- 🗨 Talk also brings dashboard and search integration, emoji picker, upload view, camera and microphone settings, mute and more
- 📅 Calendar integrates in dashboard and search, introduced a list view and design improvements
- 📫 Mail introduces threaded view, mailbox management and more
- 🗂 Deck integrates with dashboard and search, introduces Calendar integration, modal view for card editing and series of smaller improvements

👾 Some other app improvements we want to highlight include:
===============================================================

- ↕ Flow adds push notification and webhooks so other web apps can easily integrate with Nextcloud
- 🗒 Text introduced direct linking to files in Nextcloud
- 🗄 Files lets you add a description to public link shares

Integrations!
=================

But before we get to the long feature list with many details, we want to
talk about something else we are working on.

Frank talked about this in his keynote (see our live stream from the
conference).
To become an even more useful platform for collaboration, we will be
working towards deeper integration with third party platforms.

The results of the first efforts in this direction are part of this release.

Integrations in Nextcloud 20 include well known technologies like Slack,
MS Online Office Server, SharePoint and Teams, Jira and Github as well
as open source contenders like Matrix, Gitlab, Zammad and Moodle among many others.

Nextcloud Hub 20 builds on an open API, the Open Collaboration Services,
making integration efforts transparent and well documented.

The release introduces over 3 dozen integrations with a wide range of
other technologies.


Gitlab
=======

With Nextcloud 20, an integration app provides Dashboard widgets with a
GitLab todo overview and support for finding GitLab repositories and other
content using Nextcloud’s new unified search.

The integration features make it easier for Nextcloud users to track
the state of GitLab work, improving response times and efficiency.





