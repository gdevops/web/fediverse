
.. _nextcloud_2020_09_22:

==================================================================================
2020-09-22 10 manières de se tenir au courant de l’actualité Nextcloud par Brume
==================================================================================

.. seealso::

   - https://blog.genma.fr/?10-manieres-de-se-tenir-au-courant-de-l-actualite-Nextcloud

.. contents::
   :depth: 3

Introduction
=============


Billet invité, rédigé par Brume https://brume.ink/ Merci à elle. ;-)

Nextcloud est une suite logicielle libre et auto-hébergeable proposant
des services : synchronisation et hébergement de fichiers, édition en
collaboration, calendrier…

Il propose une centaine d’applications téléchargeables en un clic.

Il s’agit d’un fork d’ownCloud datant de 2016, dont Frank Karlitschek @fkarlitschek@x.com)
est le fondateur. La société Nextcloud GmbH, ainsi que la communauté,
contribuent au développement de Nextcloud.

Il existe des clients mobiles (iOS et Andoid, des clients bureau, ainsi
qu’une version web.

Si le logiciel vous intéresse, il est fort probable que vous souhaiteriez
rester informé de son évolution.

Nous allons, dans cet article, vous présenter les différentes manières
de se tenir au courant de l’actualité Nextcloud.

