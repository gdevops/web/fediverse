.. index::
   pair: Wordpress ; ActivityPub
   pair: Gitlab ; ActivityPub

.. _faire_commun:

======================================
Faire commun par Arthur Perret
======================================

- https://www.arthurperret.fr/blog/2023-10-30-faire-commun.html
- https://www.inshs.cnrs.fr/fr/cnrsinfo/le-declin-de-twitter-migrations-numeriques-et-promesses-des-reseaux-sociaux-decentralises
- https://social.sciences.re
- https://wordsmith.social/elilla/mastodon-for-twitter-users-in-5-minutes

Comme pas mal de gens autour de moi, j’ai arrêté d’utiliser Twitter. Ce
site ne me convenait plus du tout, que ce soit comme source de veille ou comme
espace de sociabilité.

Je dis « pas mal de gens » ; je ne sais pas objectivement si nous sommes
nombreux ou pas à être partis. Mais certains parlent de « migration numérique
», comme dans la Lettre de l’INSHS de mars dernier – titrée « Le déclin
de Twitter ». On peut y lire que, fatigués des réseaux centralisés et
propriétaires, de nombreux internautes cherchent désormais à investir des
lieux « communs ». Il faut entendre ce terme au sens que lui donne par exemple
Louise Merzeau : des espaces collectivement construits et administrés


Mastodon est un logiciel qui permet de créer de tels espaces.
Quelqu’un installe le logiciel sur un serveur, qui devient une instance
Mastodon. D’autres personnes peuvent alors venir s’y inscrire.
Chaque instance décide de sa ligne éditoriale, de sa politique de modération,
de son rapport aux autres instances. Ceci permet à tout un chacun de choisir
l’instance qui convient le mieux à ses attentes.

J’ai rejoint l’instance Mastodon social.sciences.re pour un essai de quelques
mois et celui-ci s’est avéré tout à fait concluant. C’est une instance
spécialisée, par opposition à d’autres instances plus généralistes :
elle est consacrée au monde des sciences et des savoirs, donc elle rassemble
avant tout une communauté de gens qui participent ou s’intéressent à
ce monde-là. J’y ai trouvé une source de veille et de socialisation
professionnelle vraiment satisfaisante – saine, diverse, surprenante.

Mastodon fournit une interface en forme de cercles concentriques

- le fil des abonnements permet de voir ce que publient les personnes
  qu’on a choisi de suivre ;
- le fil public local permet de voir ce que publient les personnes
  présentes dans la même instance ;
- le fil public global permet de voir ce que publient des personnes
  présentes dans d’autres instances.

J’utilise un peu le fil des abonnements mais je me surprends à consulter
surtout le fil public local : il m’informe, me dépayse, me donne un sentiment
de communauté. J’utilise peu le fil public global mais je suis sûr que
certains y trouvent aussi leur compte.

Un mot sur le format d’écriture : Mastodon est un logiciel de microblogging,
donc le volume de chaque message est restreint. Chaque instance fixe la limite
exacte. Mon instance utilise une limite de 500 caractères qui représente pour
moi un volume idéal : cela permet des échanges à la fois riches et digestes,
car on a de la place pour argumenter, renvoyer vers des sources… mais pas
assez pour écrire un pavé. C’est un bon dosage entre expressivité et
contrainte, très stimulant.

Au final, les questions les plus délicates restent celles de la gouvernance et
de la pérennité de l’instance. Mastodon n’est qu’un logiciel : c’est
aux gens de s’organiser pour créer et administrer des ressources communes,
éventuellement accessibles sur le temps long. L’instance que j’ai choisie
est gérée par au moins une personne appartenant au monde universitaire
français ; c’est déjà mieux qu’Elon Musk. Un financement participatif
est en place pour contribuer aux frais mais il n’est pas présenté comme
une condition à la viabilité de l’instance. J’avoue ne pas avoir creusé
le sujet pour le moment mais si je continue à utiliser cet espace, je pense
que je m’y intéresserai de plus près.

Ce que j’ai bien cerné grâce à cette période d’essai, c’est que
Mastodon n’est pas Twitter. De nombreux articles s’attachent à l’expliquer
; cette comparaison `d’Elilla & Friends <https://wordsmith.social/elilla/mastodon-for-twitter-users-in-5-minutes>`_ m’avait beaucoup plu :

    “Twitter is good for finding reports from the ground, anywhere
    in the world, in real-time. Mastodon is good for building small,
    intimate, high-trust communities for mutual support and positive, healing
    interactions.” « Twitter est un bon moyen d’obtenir des témoignages
    de ce qui se passe à un instant t, n’importe où dans le monde, en temps
    réel. Mastodon est un bon moyen de construire de petites communautés
    où règne une certaine intimité, où la confiance est élevée, qui sont
    pensées pour l’entraide, des interactions positives, réparatrices. »

On peut tempérer ces deux affirmations : Twitter est de moins en moins bon
pour faire émerger des faits, et Mastodon n’est qu’une partie de ce
qui permet de construire une communauté de confiance (ce n’est qu’un
logiciel).

Mais cela illustre assez bien le fait que Twitter et Mastodon sont
diamétralement opposés, et que leurs modèles sont complémentaires du point
de vue des pratiques informationnelles.

Quelqu’un l’a résumé en disant que Mastodon n’est pas un réseau social –
pas au sens dominant que cette expression a pour nous depuis quelques années,
à savoir une sociabilité centralisée, organisée par une plateforme unique –
mais plus un reflet de la sociabilité hors-ligne.

Ciel pas si bleu (Bluesky)
==============================

C’est avec ces réflexions en tête que je suis tombé aujourd’hui sur
un article de Mark Carrigan, via Justin Poncet : « Could Bluesky be the
replacement for Academic Twitter ? ».

La loi de Betteridge stipule qu’on
peut toujours répondre « non » à un titre d’article de presse rédigé
sous forme de question.

Ici la réponse non assumée est « oui » mais sinon
on a le même point d’interrogation attrape-clic (ou cache-sexe), la même
tendance à enfouir la réponse loin dans l’article.

Un peu de contexte : j’ai déjà critiqué ici les propos de Carrigan ;
c’était dans « Pourquoi tenir un blog scientifique », où je relevais
qu’il confondait sa propre fatigue de blogueur et la vitalité du blogging
en tant que pratique. À sa décharge, Carrigan a bâti sa carrière sur le
Web des blogs, puis celui des réseaux sociaux.

Les transformations de ces environnements l’affectent directement, violemment,
dans sa stabilité professionnelle. Il en tire toutefois des conclusions étranges.

Bluesky est un réseau social qui se présente comme à mi-chemin entre Twitter
et la galaxie Mastodon : un espace unique, donc pas de fragmentation parfois
synonyme d’isolement et de frictions ; mais une logique de décentralisation,
notamment dans le choix de l’algorithme qui peuple le flux de publications.

Je ne suis pas loin d’être d’accord avec Carrigan : Bluesky est bien
parti pour être une solution de repli pour le Twitter universitaire… si les
critères de sélection pour cette solution sont l’opacité, l’absence
de modèle économique, les discours fumeux, le naufrage garanti à moyen
terme, etc.

Bluesky n’est pas plus conforme au RGPD que Threads, le clone de Twitter
adossé à Instagram.

En l’état, c’est juste un **aspirateur à données personnelles de plus**, avec
un vernis de décentralisation qui reste pour l’instant à l’état de promesse :
basé sur un protocole propriétaire, nommé AT, Bluesky est dans une position
assez inconfortable, avec d’un côté **l’erreur stratégique de ne pas avoir
adopté le protocole ActivityPub** (utilisé par Mastodon mais aussi Threads…),
et de l’autre le désaveu du fondateur Jack Dorsey, parti promouvoir un
autre protocole plus fumeux encore, Nostr.

En fait, ça ne m’étonne pas que des gens comme Carrigan voient en Bluesky
la solution à la désagrégation de leur réseau : le modèle étant le même
que Twitter (à peu de chose près), ils espèrent pouvoir transplanter sans
trop de déperdition **leur capital social d’une plateforme à l’autre**.

Rien de surprenant venant de ce « Twitter universitaire » qui a le nez rivé
sur le facteur d’impact et le compteur d’abonnés, qui s’accommode
très bien d’une organisation de la communication sur un mode centralisé,
descendant, fermé, complètement orienté par des métriques réductrices…
parce que c’est une chaîne alimentaire qu’on peut dominer.

Ces gens-là sont incapables d’envisager la communication comme ce qui fait
commun : pas seulement mettre en commun mais faire advenir des communs –
lesquels ne portent pas intrinsèquement des valeurs, et donc appellent
une organisation mûrement réfléchie

Il incombe donc à d’autres de réfléchir précisément à cela.
