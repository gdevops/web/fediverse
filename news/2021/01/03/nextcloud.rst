
.. _nextcloud_2021_01_03:

============================================================================================
2021-01-03 One more month until the next big step forward for Nextcloud! I can't wait!
============================================================================================


.. figure:: next_big_release.png
   :align: center

   https://x.com/fkarlitschek/status/1345749036503019523?s=20
