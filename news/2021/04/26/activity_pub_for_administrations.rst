
.. _activity_pub_2021_04_26:

==============================================
2021-04-26 ActivityPub for Administrations
==============================================

- https://conf.tube/w/o5vNCrwVWPMuY2XrKTxbJG?start=0s


Introduction
=============

This is a recording with live chat of the second webinar in the
ActivityPub for Administrations series. Find out more information
on: https://socialhub.activitypub.rocks/pub/ec-ngi0-liaison-webinars-and-workshop-april-2021.


Announce
===========


About a year and a half ago I presented to the EU about "Decentralized
Networks as Essential Infrastructure".  I shared the video with @blaine
and @bmann who encouraged me to re-share it again: https://conf.tube/w/o5vNCrwVWPMuY2XrKTxbJG?start=0s

That was when I was in nonbinary mode, right before I came out as
trans-femme, so... keep that in mind, especially the difference in name!

Otherwise I think it's a good video, the points I made are still strong

