.. index::
   pair: loops.video ; 2024-11-24

.. _loops_video_2024_11_24:

======================================================================================================================================
2024-11-24 **the fediverse now has a fast growing TikTok alternative: https://loops.video ; Spread the word. Loops has arrived**
======================================================================================================================================

Announce
===============

- https://mastodon.social/@dansup/113537935105742139


the fediverse now has a fast growing TikTok alternative: https://loops.video

While it remains in a pre-release beta state atm, we are testing federation 
and will be releasing full federation support, along with the source 
code once stable.

And we expect by this time next month, you will have a bright, shiny 
and easy to install TikTok alternative under your tree 🎄

**Spread the word. Loops has arrived**.

