

.. _chavalarias_2024_11_13:

================================================================================
2024-11-13 **Réseaux sociaux : une menace durable pour la démocratie ?**
================================================================================

- https://www.radiofrance.fr/franceinter/podcasts/le-debat-du-7-10/le-debat-du-7-10-du-mercredi-13-novembre-2024-6831920

Réseaux sociaux : une menace durable pour la démocratie ? 

Avec Aurélie Jean, docteure en sciences, spécialiste en modélisation 
algorithmique, et David Chavalarias, directeur de recherches au centre 
d'analyses et de mathématiques sociales du CNRS, auteur de Toxic data (Flammarion)
