

==========================
2024-11-13
==========================

- https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/le-reseau-social-bluesky-s-envole-peut-il-etre-un-refuge-perenne-a-la-toxicite-de-x-2316038

Plus de 700 000 utilisateurs gagnés depuis l’élection de Donald Trump, 
le réseau social au papillon connait une embellie, comment peut-il 
faire pour que l'expérience des nouveaux entrants ne soit pas éphémère ?

Bluesky refait surface, le réseau dont le logo représente un papillon 
revendique 700 000 nouveaux utilisateurs depuis l’élection de Donald Trump. 

Le grand public a découvert cette application l’an dernier, grâce déjà 
aux frasques d’Elon Musk, beaucoup de Twittos ont cherché un refuge 
pour vivre une expérience plus saine avec davantage de modération et 
moins de violence dans les échanges en ligne. 

Pour celles et ceux qui ne le connaissent pas, l’interface fonctionne 
comme X, des profils, un fil et des micro messages que l’on peut liker ou partager.

Rien d’étonnant, car à l’origine Bluesky a été développé en interne par 
l’ancien directeur de Twitter, Jack Dorsey qui depuis a quitté le conseil 
d’administration de Bluesky tout en redevenant actif sur X, une manière 
de lâcher son bébé et d’adhérer à la vision d’Elon Musk. 

Car migrer sur Bluesky est un acte politique, celui de ne plus vouloir 
donner de son temps, de ses idées et ses données à un réseau devenu une 
arme de campagne pour le camp républicain. 

**Ce sont les Swiffties, les fans de Taylor Swift**, soutien de Kamala Harris, 
qui ont impulsé un grand mouvement en proposant cette transhumance 
numérique vers Bluesky. 

Plusieurs témoignages faisaient part d’un regain de tensions et d’insultes 
sur X depuis le succès de Donald Trump.

Beaucoup de femmes se plaignaient également de la nouvelle fonctionnalité 
de X qui permet à une personne qui été bloquée de continuer à voir les 
publications de l’utilisateur qui l'a banni. 

Sous couvert de défendre la liberté d’expression, rien n’est fait pour 
protéger celles et ceux qui subissent le cyberharcèlement. 

La suspension de X au Brésil a aussi fait gagner des utilisateurs à Bluesky, 
le réseau dépasse aujourd’hui les 14 millions d’utilisateurs contre 9 en 
septembre. 

14 millions dont Alexandria Ocasio-Cortez (représentante des Etats-Unis) 
qui n’a pas manqué l’occasion de faire son retour et de déclarer 
"Mon Dieu, c'est agréable d'être dans un espace numérique avec d'autres 
êtres humains réels."

