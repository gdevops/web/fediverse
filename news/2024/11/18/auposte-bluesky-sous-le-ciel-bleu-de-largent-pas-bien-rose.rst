

.. david_dufresne_2024_11_18:

============================================================================================
2024-11-18 **Bluesky: sous le ciel bleu, de l’argent pas bien rose** par David Dufresne
============================================================================================

- https://www.auposte.fr/bluesky-sous-le-ciel-bleu-de-largent-pas-bien-rose/


Dans un communiqué du 24 octobre 2024, l’entreprise Bluesky se félicite 
de son nouveau tour de table financier: 15 millions de dollars de 
Blockchain Capital avec la participation d’Alumni Ventures, True Ventures, 
SevenX, Amir Shevat, Joe Beda, et quelques autres suspects habituels 
du capital-risque de la Vallée du Silicium, Californie.

Un des fondateurs de Blockchain Capital n’est d’autre que Brock Pierce, 
ex-comédien, ex-candidat malheureux à la Présidence US (2020) et surtout 
vieil ami de… Steve Bannon, l’âme damnée de Trump et des Libertariens ricains. 

De Bannon, Pierce dit qu’il a été "son bras droit" sept ans durant. 

L’homme de l’ombre de Trump a même soutenu activement le techno-libertarien 
dans une autre campagne, "anti-establishment", perdue elle aussi 
(pour le Sénat US, en 2022). 

La fortune de Pierce, estimée par Forbes à 1 milliard de dollars, provient 
principalement de la crypto et du blockchain. 

Une industrie opaque et salement dévastatrice pour l’environnement.

Dans un article de TechCrunch, à propos de l’arrivée en fanfare de 
Blockchain Capital (lié de près ou de loin à quelques 110 entreprises) 
dans Bluesky, cette dernière se défend de se livrer un jour à 
l’"hyper-financiarisation de l’expérience sociale" avec le recours aux 
habituelles dérives du genre et du domaine (tokens, cryptomonnaie, NFTs, minage). 

On sait ce que valent les promesses de la Tech.

Déjà, anticipant l’incroyable ruée vers elle avec l’élection probable de 
Trump, et une partie de la désaffection de X, BlueSky a annoncé dans un 
futur proche… des abonnements payants (pour pouvoir "customiser" son profil, 
ou poster des vidéos de meilleure qualité — elles sont actuellement 
limitées à 60 secondes).

A chaque fois, la com’ de l’entreprise se veut rassurante: c’est pour 
le bien de ses utilisateurs, et les bienfaits des conversations. 

Bluesky promet, ô grand jamais, que l’entreprise ne favorisera les comptes 
payants (Musk en son temps avait promis la même chose).

On notera aussi, dans la courte mais déjà tumultueuse histoire de Bluesky, 
l’appréciation de son fondateur, écarté depuis: Jack Dorsey, père de Twitter 
et, en partie, de Bluesky. 

En mai 2024, Dorsey annonce son départ du conseil d’administration pour 
une raison simple, et édifiante: "Bluesky a été lancé pour devenir la 
couche de protocole open source des plateformes de médias sociaux, 
mais l’équipe a fini par “répéter littéralement toutes les erreurs 
que nous avons faites” avec Twitter" (PirateWires). 

Depuis, Dorsey est chez Disney. C’est dire s’il s’y connait en fable.

Concernant les autres nouveaux partenaires de BlueSky, on notera les 
amendes et sanctions contre Alumni Ventures (pratiques de frais de 
gestion trompeuses, transferts illégaux de fonds) ou la démesure de 
True Ventures (qui gère plus de 2 milliards de dollars d’actifs dans 
pas moins de… 300 entreprises).

Autrement dit: ne plus s’exprimer sur X, c’est vital mais se ruer sur 
BlueSky en croyant y trouver un havre de paix, construit sur un contrat 
social juste et progressiste, est à coup sûr se bercer d’illusions. 

Et se préparer à rejouer, tôt ou tard, la même scène qu’avec feu Twitter 
en son temps.

David Dufresne


Remarques
============

Au Poste est joignable sur Mastodon (https://piaille.fr/@auposte) et BlueSky (auposte.bsky.social).

Suite à des retours sur… BSKY:

MaJ le 19 novembre à 0 h 06 min: précisions sur le communiqué de Bluesky 
PBLLC repris par TechCrunch (la version originelle de notre éditorial 
pouvait laisser croire que la citation était du magazine)

MaJ le 19 novembre à 0 h 52: un passage de Jack Dorsey, critiquant le 
" modération centralisée " de BSBY, a été retiré. 
Il pouvait porter à confusion, sachant que le point saillant est bien, 
ici, sa déclaration non contestée: "Bluesky a été lancé pour devenir la 
couche de protocole open source des plateformes de médias sociaux, mais 
l’équipe a fini par “répéter littéralement toutes les erreurs que nous avons faites” avec Twitter"
