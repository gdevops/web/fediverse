

.. _sarah_perez_2024_11_18:

============================================================================================
2024-11-18 **Mastodon sees a boost from the 'X exodus,' too, founder says** by Sarah Perez
============================================================================================

- https://techcrunch.com/2024/11/18/mastodon-sees-a-boost-from-the-x-exodus-too-founder-says/
- https://joinmastodon.org/
- https://mastodon.social/@Gargron/113488825856891653


Author
=========

- https://techcrunch.com/author/sarah-perez/


Decentralized social network Mastodon is also seeing some uplift from the X
“exodus,” founder Eugen Rochko shared on the platform just ahead of the
weekend. 

While the Twitter-like startup Bluesky has been capturing headlines
and attention for its rapid adoption among former X users, **a smaller group
has apparently turned to the open source networking platform Mastodon**, which
has seen downloads increase by 47% on iOS and **its monthly active users grow
to 894,000 across all Mastodon servers**, Rochko says.

In a post on Mastodon, the founder and CEO noted that official app downloads
were growing. 
In addition to the 47% on iOS, they had increased by a smaller 17% on 
Android. 
Month over month, sign-ups were also up by 27% with 90,000 newly registered 
accounts in November.

mastodon does not depend on venture capital to survive
==========================================================

“We may not be the biggest by numbers yet, but Mastodon (and the fediverse)
has proven itself to be an effective and reliable communications platform
over the course of the last 8 years, and does not depend on venture capital
to survive,” wrote Rochko, subtly jabbing at one of Bluesky’s potential
weaknesses with that last bit.

While Mastodon is largely crowdfunded and grant-supported, Bluesky has
gone the more traditional startup route by raising money from outside
investors. 


The company bluseky last month closed on a $15 million Series A, led by Blockchain  Capital (beurk!)
========================================================================================================

The company last month closed on a $15 million Series A, led by Blockchain 
Capital. 

Bluesky plans to generate revenue via subscriptions at some later point 
and by providing other paid services, like access to custom domains. 

For some former X users now choosing Mastodon, Bluesky’s obligation to 
chase venture-sized returns is a non-starter.

Despite Mastodon’s recent “X exodus”-fueled adoption, the latest
activity still falls below late 2022 levels, when Mastodon had gotten an
early boost thanks to other Twitter drama, including the product changes
Elon Musk introduced around moderation and paid verification. 

As of November 2022, Mastodon had surpassed 1 million monthly active 
users and was seeing “thousands” of registrations per hour, it said at the time.

Things have slowed since for the X alternative, which today has north 
of 7.6 million total users compared with Bluesky’s now more 
than 19.4 million, which is up from 16 million just days ago.

Despite the threat from Bluesky, Rochko says that the fediverse — or the open
social web powered by the ActivityPub protocol that underpins Mastodon and
newer efforts like Meta’s Threads — is the “long game.” 
(Threads’ 275 million+ users are not counted among the 10.9+ million 
broader fediverse users because the platform isn’t fully federated yet.)

“I spent 25% of my life working on Mastodon. It’s kind of wild when
you think about it,” Rochko mused on Mastodon. “When I started, Google+
was still around. We’re doing what one of the most resourceful companies
in the world couldn’t do. It’s not easy. It’s always easier to just
build A Website, then slap some ads on it,” he said, likely referencing
Threads’ plan to be ad-supported. “But the fediverse is the long game.”
