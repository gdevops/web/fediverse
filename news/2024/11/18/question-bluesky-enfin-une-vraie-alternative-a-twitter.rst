
.. _bluesky_2024_11_18:

======================================================================================
2024-11-18 **Bluesky : enfin une vraie alternative à Twitter ?** par Maël Thomas
======================================================================================

- https://bonpote.com/bluesky-enfin-une-vraie-alternative-a-twitter


Les réactions sur mastodon
==============================

- https://mastodon.zaclys.com/@maxsol/113505874285228987
- https://mstdn.social/@jbouchez/113505882637985413

.. figure:: images/bonpote_bad.webp

   https://mastodon.zaclys.com/@maxsol/113505874285228987


Considering Bluesky ?
--------------------------

- https://mastodon.social/@ASegar/113494145062267528

Please bear in mind that #Bluesky is:

Funded by #BlockchainCapital:

- co-founded by Steve Bannon pal Brock Pierce, a major crypto advocate, 
  and close friend of Eric Adams
- run in part by Kirill Dorofeev, who also works for #VK, Russia’s state social network.

- A venture-funded startup, so once they need to monetize they're likely 
  to turn to an exploitative business model.

Sources: @davetroy, @thenexusofprivacy

Eugene Rochko
------------------

- https://mastodon.social/@Gargron/113365319263963755

Mastodon is financed by crowdfunding instead of venture capital not because 
we don't know that venture capital exists, not because we don't have bills 
to pay, and not because venture capital isn't willing to give money to 
new social media platforms. 

VCs don't want a sustainable business, they want a big exit. 

Every VC-backed business is on a timer to deliver or die.


Auteur: Maël Thomas
==========================

- https://bonpote.com/auteurs/mael-thomas/

Introduction
================

Éviter les dangers de la centralisation sans sacrifier la simplicité

Mastodon et Bluesky partent du même constat : un média social centralisé 
n’est aucunement résilient au rachat du réseau par un acteur toxique.

La clef du succès actuel de Bluesky, c’est un savant compromis entre la 
centralisation excessive de Twitter/X et la décentralisation totale mais 
complexe de Mastodon, que seuls les plus geeks et les plus motivés ont 
accepté de surmonter à ce jour.

Bluesky répond à l’usage principal qui a fait le succès initial de 
Twitter : un réseau collaboratif unifié où cohabitent célébrités, 
politiciens, journalistes, influenceurs thématiques… créant des sources 
primaires d’information relayées par les médias plus classiques.


Les skeets favoris
=======================

- https://bsky.app/profile/jay.bsky.team/post/3laxorm2isl2f

Les messages postés sur Bluesky, que certains appellent “skeets”, ne 
peuvent pas encore être mis en favoris pour les retrouver par la suite. 

C’est une fonctionnalité annoncée.

En parlant de pérennité…
================================

- https://www.sky-follower-bridge.dev/fr/

Du côté de Mastodon, les mêmes questions de financenement se posent : 
les instances étant gratuites, sans moyen de paiement mis à disposition 
des propriétaires, ces derniers doivent bénévolement payer des factures 
importantes et les compenser tant bien que mal par des appels aux dons.

Notons finalement que la perennité de X n’a quant à elle jamais été aussi 
incertaine. 

La migration massive actuelle depuis X dont celles d’organes de presse 
importants, la fuite des annonceurs, la perte de valorisation de l’entreprise 
depuis son rachat à 44 milliards par Musk, semble augurer un statut de 
pseudo média de propagande d’État, dont la pérennité financière serait 
alors directement liée à sa bonne entente avec la nouvelle présidence 
d’extrême droite des États-Unis.


Essayez Bluesky !
==================

- https://bonpote.com/bluesky-enfin-une-vraie-alternative-a-twitter/#Essayez_Bluesky

Testez Bluesky, c’est simple et gratuit.
 
La communauté francophone est en croissance fulgurante et propose déjà 
de nombreux contenus intéressants. 

Un `outil https://www.sky-follower-bridge.dev/fr/ <https://www.sky-follower-bridge.dev/fr/>`_ 
permet même de trouver vos  abonnements X sur Bluesky.

L’outil d’écriture de “threads”, ces fils de messages qui ont fait le 
succès de la vulgarisation scientifique et environnementale sur Twitter, 
vient tout juste d’être mis en ligne, à nous d’en faire bon usage !

Comme tous les réseaux sociaux, son avenir reste incertain mais l’effort 
pour empêcher son rachat par un milliardaire est réel.

