

.. _derrac_2024_11_18:

=========================================================================================================================
2024-11-18 **Le web progressiste aurait intérêt à migrer sur Mastodon, pas sur Bluesky** par Louis Derrac
=========================================================================================================================

- https://louisderrac.com/le-web-progressiste-aurait-interet-a-migrer-sur-mastodon-pas-sur-bluesky/

Auteur: Louis Derrac
=======================

- https://louisderrac.com/


Le web progressiste aurait intérêt à migrer sur Mastodon, pas sur Bluesky
==================================================================================

Note : je poursuis la réflexion dans un autre article. Et si le problème
des réseaux sociaux, c’étaient les phares ?

Edit : par web « progressiste », j’emploie volontairement un qualificatif
flou (et dépassant le clivage gauche/droite) pour englober la part du web
qui ne se reconnaît pas dans la dérive (ultra)conservatrice et d’extrême
droite de la tech (qui maitrise aujourd’hui une partie des réseaux sociaux),
et plus largement, d’une partie de la société américaine.

Avec l’élection de Trump/Musk1, le réseau social X (anciennement Twitter)
vit un nouvel épisode de « départ » massif. Ou plus précisément,
les réseaux sociaux concurrents voient un nouvel épisode d’arrivée
massive. Ce qui n’est pas tout à fait pareil. Et pas vraiment nouveau,
puisque de tels épisodes se déroulent régulièrement depuis l’achat de
X par Elon Musk.

Si un grand nombre d’utilisateurices cherchent un espace d’expression
moins toxique, beaucoup quittent (pour certain⋅e⋅s définitivement)
X pour des raisons morales2. Et donc, le réseau social qui gonfle en ce
moment, c’est Bluesky.

La semaine dernière, je suis donc allé me créer un compte Bluesky,
pour voir. J’ai lu pas mal d’articles sur ce réseau social, certains
dithyrambiques, d’autres beaucoup plus nuancés, et quelques-uns très
négatifs3. Aujourd’hui, mon avis est fait : le web progressiste aurait
tout intérêt à migrer directement sur Mastodon, pas sur Bluesky. Pourquoi
? Parce que, sauf à espérer un miracle, Bluesky is the next TwitterX.

Bluesky a été créé par Jack Dorsey, le même qui a créé Twitter. 
Vous voyez un peu le début du problème ? Bluesky est financé par de gros
capitaux-risqueurs de la Silicon Valley, pas de ceux qui se contentent de
créer un réseau social décentralisé pour le bien commun. 

Nous avions le droit d’être naïfs au milieu des années 2000, quand on nous vendait le
web social comme une utopie. Plus aujourd’hui. Parmi les investisseurs, les
soutiens de la première heure, les premiers salariés de Bluesky, on retrouve
la crypto-galaxie. Celle qui a activement fait campagne… pour Donald Trump4.

Sur le plan moral, donc, Bluesky a beau être un réseau social très jeune, il n’est déjà pas très convaincant
---------------------------------------------------------------------------------------------------------------

Sur le plan moral, donc, Bluesky a beau être un réseau social très jeune,
il n’est déjà pas très convaincant. Or on sait maintenant que les projets
idéologiques et politiques sont déterminants dans la tech US. 

Sur le plan économique, Bluesky est aux mains de tout ce qu’il y a de plus classique
pour les Big Tech de la Silicon Valley. Espérer qu’il fasse autre chose que
ce qu’ont fait toutes les autres Big Tech (maximisation des bénéfices,
enfermement progressif, économie de l’attention, captation des données
personnelles, publicités de plus en plus intrusives, viralisation des contenus
toxiques, etc.) relève de la croyance la plus aveugle. Cela reviendrait
même à croire au(x) miracle(s). Il faudrait réussir à croire qu’une
entreprise capitaliste, financée par des VC technosolutionnistes américains
crypto-addicts, va vraiment s’ingénier à concevoir une infrastructure
technique sincèrement open-source et interopérable. Pour le bien commun
et l’intérêt général. Sur le (très) long terme5. Youhou ! Vous vous
rappelez d’OpenAI (la boite derrière ChatGPT), qui était à la base une
association à but non lucratif dont l’objectif était de produire de la
science ouverte sur l’IA ?


Bref, le web progressiste, particulièrement les plus militants, n’a pas un temps et une énergie illimités
--------------------------------------------------------------------------------------------------------------

Bref, le web progressiste, particulièrement les plus militants, n’a pas
un temps et une énergie illimités. Je crois qu’il aurait intérêt à
gagner un peu de temps en sautant l’étape Bluesky et à migrer directement
sur une instance Mastodon. Ou plus largement, à découvrir le Fédivers6. Ce
n’est pas parfait bien sûr, et il reste de nombreux enjeux : le financement,
car rien n’est gratuit, la modération, l’ergonomie, etc.. Bien sûr,
ça demande un peu d’habitude, de construire de nouveaux repères, comme
n’importe quel nouvel outil. Mais ça existe, c’est fonctionnel, et ça
progresse tous les jours !

Internautes progressistes en recherche de nouveaux repères, nous vous
attendons. Venez nous aider à construire le web social alternatif dont nous
avons tant besoin.

Note : je poursuis la réflexion dans un autre article. Et si le problème
des réseaux sociaux, c’étaient les phares ?

Edit 2 : j’ai supprimé le guide et le tableau comparatif de l’article,
suite à de nombreux commentaires montrant que l’exercice était périlleux
et desservait en réalité le but de l’article, qui n’est PAS de glorifier
Mastodon ou de prétendre que tout y est parfait.

Notes de bas de page
===========================

1
---

Et ce qu’on découvre progressivement sur la façon dont Musk a mis X au 
service de la victoire de Trump, ou sur l’influence grandissante de la 
tech de droite aux US.

2
---

Mieux vaut tard que jamais, j’estimais pour ma part dès décembre 2022 
que Twitter était (déjà) devenu le nouveau Truth Social, et que le 
quitter était devenu un choix moral.

3
---

Vous pouvez les retrouver dans ma veille

4
----

The crypto industry plowed tens of millions into the election. 
Now, it’s looking for a return on that investment

5
----

Ici, il faut bien avoir en tête l’excellent concept de « merdification » 
des services numériques, proposé par Cory Doctorow. I
l l’évoque précisément sur le cas Bluesky dans un article.


6
----

Qui dépasse Mastodon avec de nombreuses autres plateformes sociales qui 
peuvent communiquer les unes avec les autres

