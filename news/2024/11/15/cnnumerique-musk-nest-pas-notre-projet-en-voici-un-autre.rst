

.. _cnn_2024_11-15:

========================================================================================================
2024-11-15 **Musk n’est pas notre projet. En voici un autre** par le Conseil national du numérique
========================================================================================================

- https://cnnumerique.fr/lettre-dinformation/musk-nest-pas-notre-projet-en-voici-un-autre
- https://www.radiofrance.fr/franceinter/podcasts/le-debat-du-7-10/le-debat-du-7-10-du-mercredi-13-novembre-2024-6831920
- https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/le-reseau-social-bluesky-s-envole-peut-il-etre-un-refuge-perenne-a-la-toxicite-de-x-2316038
- https://siecledigital.fr/2024/11/13/bluesky-decolle-avec-145-millions-dutilisateurs-apres-lelection-de-donald-trump/

Depuis le 25 octobre 2024, le Conseil National Numérique ne poste plus sur X. 

Nous sommes sur Mastodon depuis plus d’un an maintenant et nous y sommes 
bien. 

Depuis l’élection de Donald Trump, divers appels à quitter X ont été 
formulés, d’autres ont simplement pris et annoncé cette décision. 

Au premier titre, `David Chavalarias encourageait sur France Inter <https://www.radiofrance.fr/franceinter/podcasts/le-debat-du-7-10/le-debat-du-7-10-du-mercredi-13-novembre-2024-6831920>`_
à se  mettre en ordre de départ pour le 20 janvier 2025 et invitait les médias à 
s’inscrire dans ce mouvement. 

Le même jour, le Guardian et La Vanguardia ont annoncé quitter Twitter. 

On comptera aussi `la chronique de François Saltiel <https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/le-reseau-social-bluesky-s-envole-peut-il-etre-un-refuge-perenne-a-la-toxicite-de-x-2316038>`_ 
invitant à migrer  vers Bluesky, un réseau qu’on aime beaucoup citer pour le choix qu’il 
donne aux utilisateurs sur la recommandation algorithmique et la modération 
des contenus. 

Depuis la victoire de Donald Trump, le nombre d’utilisateurs de ce réseau 
social `a bondi <https://siecledigital.fr/2024/11/13/bluesky-decolle-avec-145-millions-dutilisateurs-apres-lelection-de-donald-trump/>`_, 
marquant un sursaut similaire au moment où Elon Musk  avait acquis Twitter.
