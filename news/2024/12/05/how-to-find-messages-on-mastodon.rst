.. index::
   pair : mastodon; searching

.. _searching_from_me_in_library:

===========================================================================================================
2024-12-05 **Reminder you can find your own posts on mastodon with from:me <texte> and in:library <text>**
===========================================================================================================

- https://gamedev.lgbt/@aetataureate/113601416173123595

from:me <text> 
================

reminder you can find your own posts on here by typing::

    from:me <text> 
     
in the search box followed by whatever search terms you want.

in:library <text>
======================

you can also search your posts plus ones you've starred or bookmarked 
by searching


::

    in:library <text>
    
then your search terms.

many servers don't have "full text search" because it adds a lot of server 
load ($). so flagging posts to join your own accessible "library" can 
be a big help!

thanks kydia who was asking, because this is great for everyone to know!
