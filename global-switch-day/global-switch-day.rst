.. index::
   ! Global switch day


.. _global_switch_day:

=============================================================================================
**Global switch day** #GlobalSwitchDay #eXit #HelloQuitX #twittermigration #StarterPacks
=============================================================================================

- https://globalswitchday.net/
- https://jointhefediverse.net/?lang=fr
- https://the-federation.info/
- https://alternativeto.net/feature/fediverse/

All applis with Signal
=================================

.. figure:: images/all_applis_signal.png
   :width: 400

All applis and XMPP/Deltachat
=================================

.. figure:: images/all_applis_xmpp.png
   :width: 400


Tiktok => loops
===================

.. figure:: images/tiktok_loops.webp
   :width: 400


Facebook => Friendica
=============================

.. figure:: images/facebook_friendica.webp
   :width: 400


X => mastodon
==================

.. figure:: images/x_mastodon.webp
   :width: 400

.. toctree::
   :maxdepth: 3
   
   hello-quitte-x/hello-quitte-x 
   
      
Instagram => Pixelfed
=========================
   
.. figure:: images/instagram_pixelfed.png
   :width: 400


WhatsApp => Signal
=========================   
   
.. figure:: images/whats_app_signal.png   
   :width: 400
   
WhatsApp => XMPP
=========================
   
.. figure:: images/whats_app_xmpp.png   
   :width: 400
   
   
WhatsApp => DeltaChat
=========================
  
.. figure:: images/whats_app_deltachat.png  
   :width: 400
