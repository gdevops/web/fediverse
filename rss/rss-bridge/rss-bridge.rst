.. index::
   pair: Bridge ; RSS
   ! RSS bridge

.. _rss_bridge:

===========================================================
**RSS bridge** (The RSS feed for websites missing it)
===========================================================

- https://rss-bridge.org/bridge01/
- https://github.com/RSS-Bridge/rss-bridge
- https://github.com/RSS-Bridge/rss-bridge/releases.atom
