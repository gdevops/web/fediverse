.. index::
   pair: mastodon ; RSS

.. _mastodon_rss:

============================
RSS feed for mastodon
============================


Add .rss to the mastodon account.


Examples
==========

- https://kolektiva.social/@raar/
- https://kolektiva.social/@raar.rss
