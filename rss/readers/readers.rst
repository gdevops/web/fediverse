.. index::
   pair: Readers; RSS

.. _rss_readers:

===========================================
**RSS readers** |FluxWeb|
===========================================

- https://gofoss.net/foss-apps/#rss-online-radio

.. toctree::
   :maxdepth: 3

   feedbot/feedbot
   flux/flux
   thunderbird/thunderbird
