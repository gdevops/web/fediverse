.. index::
   pair: Feedbot ; RSS Reader
   ! Feedbot

.. _thunderbird:

===========================================
**Thunderbird** RSS reader
===========================================


**Thunderbird + RSS: How To Bring Your Favorite Content To The Inbox** by Jason Evangelho
==========================================================================================

- https://blog.thunderbird.net/2022/05/thunderbird-rss-feeds-guide-favorite-content-to-the-inbox

I first discovered RSS feeds in 2004 when I fell in love with podcasting. 


That’s when I learned I could utilize RSS to bring my favorite web content 
to me, on my schedule. 

Whether it was weekly music podcasts, tech blogs, newspaper articles, or 
a local weather forecast, RSS became a way to more easily digest and 
disseminate the growing onslaught of content on the web. 

Back then, I used Google Reader (RIP). But now I use Thunderbird to manage 
and read all my news feeds, and I love it!

In this post I’ll explain what RSS is, why it’s useful, and how to get 
all set up with some feeds inside of Thunderbird. 
