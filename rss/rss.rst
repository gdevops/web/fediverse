.. index::
   pair: Fediverse; RSS
   ! really simple syndication

.. _fedi_rss:

================================================
**RSS (really simple syndication)** |FluxWeb|
================================================

- https://fr.wikipedia.org/wiki/RSS
- https://gofoss.net/foss-apps/#rss-online-radio
- https://www.teotimepacreau.fr/blog/g%C3%A9n%C3%A9rer-un-flux-rss-pour-nimporte-quel-site-web/


.. toctree::
   :maxdepth: 3

   description/description
   rss-bridge/rss-bridge
   find/find
   firefox/firefox
   github/github
   mastodon/mastodon
   people/people
   readers/readers
   examples/examples

::

    https://framapiaf.org/web/tags/mastodon.rss
 
