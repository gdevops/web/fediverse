
.. _get_rss_feed_url:

============================
Get RSS Feed URL
============================

- https://addons.mozilla.org/fr/firefox/addon/get-rss-feed-url/
- https://github.com/shevabam/get-rss-feed-url-extension


À propos de cette extension

Get RSS Feed URL is a Firefox extension that provides links to the various
RSS/Atom feeds of a website.

Indeed, websites do not always provide a direct link to the RSS feed.
It is then necessary to look in the source code of the website and find
the URL of the feed.

This extension makes it possible to avoid this manipulation because
the URLs of the RSS feeds of the website are displayed directly and
can be copied with one click!

In addition, this extension allows you to easily retrieve the RSS feed
from a Youtube channel / user or from a subreddit!

Supports Firefox on desktop as well as Firefox Nightly on Android!
