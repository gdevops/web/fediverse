.. index::
   ! Firefox extensions

.. _firefox_extensions:

============================
RSS firefox extensions
============================

.. toctree::
   :maxdepth: 3

   get-rss-feed-url
