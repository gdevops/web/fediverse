
.. _finding_rss:

============================
Finding Fediverse Feeds
============================

- https://hyperborea.org/tech-tips/fediverse-feeds/
- https://wandering.shop/@KelsonV/109696121243401067

All of these platforms can talk to each other via ActivityPub (or at least try to),
but sometimes you want to hook up a public stream to another application – maybe to
read on your own feed reader like NetNewsWire or Feedly or Thunderbird,
maybe to drive an action through IFTTT or Zapier.

Fortunately, most Fediverse platforms do feature an RSS or Atom feed
for public posts!

But it’s not always easy to find the feed URL, which is why I made this list.

