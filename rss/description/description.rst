.. index::
   pair: Description; RSS

.. _rss_desc:

================================================
**RSS description**
================================================

- https://fr.wikipedia.org/wiki/RSS

Why You’ll Love RSS
===========================

- https://blog.thunderbird.net/2022/05/thunderbird-rss-feeds-guide-favorite-content-to-the-inbox/

Here are a few compelling reasons for using RSS feeds to consume your 
favorite web content:

- You don’t have to track down the news. The news comes to you!
- Stay on top of updates from your favorite sites without needing to 
  subscribe to newsletters or remembering to manually check in. 
  (Especially useful for sites that don’t update regularly.)
- Organize your favorite content into categories, tags, folders and sub-folders just like your email.
- Bypass algorithms, intrusive ads and privacy-invading trackers.
- RSS feeds are free
- All podcasts (except Spotify exclusives) use RSS feeds. So does YouTube!
- It’s easy to move your RSS feed subscriptions to other desktop and mobile apps.
- Shameless plug: You can read this Thunderbird blog in Thunderbird! 
