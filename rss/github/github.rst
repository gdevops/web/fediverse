.. index::
   pair: Github ; RSS

.. _github_rss:

============================
RSS feed for github
============================

GitHub provides some official RSS feeds officially
=======================================================

- https://github.com/owner/repo/releases.atom
- https://github.com/owner/repo/commits.atom
- https://github.com/user.atom

Examples
==========

- https://github.com/prefix-dev/pixi/releases.atom
