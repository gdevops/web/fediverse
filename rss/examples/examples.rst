.. index::
   pair: Examples; RSS

.. _rss_examples:

===========================================
RSS examples
===========================================


.. :ref:`fed_rss_2022_11_18`


Did you know that #Mastodon supports #RSS feeds ?
====================================================

- https://mastodon.online/@thunderbird/109381711803014089


That means you can follow your favorite people and topics right inside
of Thunderbird!

→ Just add ".rss" to the URL ←

For example, our Mastodon URL "https://mastodon.online/@thunderbird"

becomes::

    "https://mastodon.online/@thunderbird.rss"

What about #hashtags ? YEP !
=============================

Let's look at #OpenSource. From our instance, it is: "https://mastodon.online/tags/opensource"

So, we just append .rss and it works!::

    "https://mastodon.online/tags/opensource.rss"

REALLY useful if you don't want to miss a thing!


You all probably already knew that, but I thought it was pretty cool! :)


::

    https://framapiaf.org/web/tags/iran.rss
    https://framapiaf.org/web/tags/mazeldon.rss
    https://framapiaf.org/web/tags/KianPirfalak.rss
    https://framapiaf.org/web/tags/bloodynovember.rss
    https://framapiaf.org/web/tags/mahsaamini.rss
    https://framapiaf.org/web/tags/babka.rss
    https://framapiaf.org/web/tags/HumanisticJudaism.rss
    https://framapiaf.org/web/tags/ucl.rss
    https://framapiaf.org/web/tags/kurdistan.rss
    https://framapiaf.org/web/tags/rojava.rss
    https://framapiaf.org/web/tags/MahsaAmini.rss
    https://framapiaf.org/web/tags/MahsaJinaAmini.rss
    https://framapiaf.org/web/tags/NagihanAkarsel.rss
    https://framapiaf.org/web/tags/chagall.rss
    https://framapiaf.org/web/tags/ocalan.rss
    https://framapiaf.org/web/tags/climatejustice.rss
    https://framapiaf.org/web/tags/furo.rss
    https://framapiaf.org/web/tags/giec.rss
    https://framapiaf.org/web/tags/climatechange.rss
    https://framapiaf.org/web/tags/wmo.rss
    https://framapiaf.org/web/tags/ipbes.rss
    https://framapiaf.org/web/tags/Women4Biodiversity.rss
    https://framapiaf.org/web/tags/cop15.rss
    https://framapiaf.org/web/tags/berivanOmar.rss
    https://framapiaf.org/web/tags/StopExecutionInIran.rss
    https://framapiaf.org/web/tags/FrenchMP4Iran.rss
    https://framapiaf.org/web/tags/ps752.rss
    https://framapiaf.org/web/tags/mediapart.rss
    https://framapiaf.org/web/tags/MorvaridAyaz.rss
    https://framapiaf.org/web/tags/Grenoble.rss
    https://framapiaf.org/web/tags/pyconus.rss
    https://framapiaf.org/web/tags/toomaj.rss
    https://framapiaf.org/web/tags/ukraine.rss
    https://framapiaf.org/web/tags/osmfr.rss
    https://framapiaf.org/web/tags/OpenStreetMap.rss
    https://framapiaf.org/web/tags/klezmer.rss
    https://framapiaf.org/web/tags/antisemitism.rss
    https://framapiaf.org/web/tags/antisemitisme.rss
    https://framapiaf.org/web/tags/raar.rss
    https://framapiaf.org/web/tags/golem.rss


RSS examples
===============

- https://framapiaf.org/web/tags/pages.rss
- https://framapiaf.org/web/tags/sentinelle.rss
- https://framapiaf.org/web/tags/elk.rss
- https://framapiaf.org/web/tags/mastodon.rss
- https://framapiaf.org/web/tags/codeberg.rss
- https://framapiaf.org/web/tags/python.rss
- https://framapiaf.org/web/tags/licenses.rss
- https://framapiaf.org/web/tags/postgresql.rss
- https://framapiaf.org/web/tags/api.rss
- https://framapiaf.org/web/tags/django.rss
- https://framapiaf.org/web/tags/debian.rss
- https://framapiaf.org/web/tags/vmware.rss
- https://framapiaf.org/web/tags/ssh.rss
- https://framapiaf.org/web/tags/PlatformEngineering.rss
- https://framapiaf.org/web/tags/vscode.rss
- https://framapiaf.org/web/tags/presentation.rss
- https://framapiaf.org/web/tags/htmx.rss
- https://framapiaf.org/web/tags/introduction.rss
- https://framapiaf.org/web/tags/adminsys.rss
- https://framapiaf.org/web/tags/rust.rss
- https://framapiaf.org/web/tags/rss.rss
- https://framapiaf.org/web/tags/mkdocs.rss
- https://framapiaf.org/web/tags/sphinx.rss
- https://framapiaf.org/web/tags/breadcrumb.rss
- https://framapiaf.org/web/tags/ansible.rss
- https://framapiaf.org/web/tags/devops.rss
- https://framapiaf.org/web/tags/packer.rss
- https://framapiaf.org/web/tags/terraform.rss
- https://framapiaf.org/web/tags/grenoble.rss
- https://framapiaf.org/web/tags/mdn.rss
- https://framapiaf.org/web/tags/superset.rss
- https://framapiaf.org/web/tags/gitea.rss
- https://framapiaf.org/web/tags/git.rss
- https://framapiaf.org/web/tags/htmx.rss
- https://framapiaf.org/web/tags/httpie.rss
- https://framapiaf.org/web/tags/restapi.rss
- https://framapiaf.org/web/tags/AaronSwartz.rss
- https://framapiaf.org/web/tags/playwright.rss
- https://framapiaf.org/web/tags/dagger.rss
- https://framapiaf.org/web/tags/castopod.rss
- https://framapiaf.org/web/tags/logiciellibre.rss
- https://framapiaf.org/web/tags/Thunderbird.rss
- https://mastodon.green/tags/ClimateAction.rss
- https://framapiaf.org/web/tags/mahsaamini.rss
- https://framapiaf.org/web/tags/noamchomsky.rss
- https://framapiaf.org/web/tags/spinoza.rss
- https://framapiaf.org/web/tags/einstein.rss
- https://framapiaf.org/web/tags/HumanisticJudaism.rss
- https://framapiaf.org/web/tags/kurdes.rss
- https://framapiaf.org/web/tags/mobilizon.rss
- https://framapiaf.org/web/tags/httpie.rss
- https://framapiaf.org/web/tags/postman.rss
- https://framapiaf.org/web/tags/standardsql.rss
- https://framapiaf.org/web/tags/sql.rss
- https://framapiaf.org/web/tags/fosstodon.rss
- https://framapiaf.org/web/tags/glab.rss
- https://framapiaf.org/web/tags/locust.rss
- https://framapiaf.org/web/tags/fosdem.rss
- https://framapiaf.org/web/tags/fosdem2023.rss
- https://framapiaf.org/web/tags/offdem.rss
- https://framapiaf.org/web/tags/python.rss
- https://framapiaf.org/web/tags/glab.rss
- https://framapiaf.org/web/tags/opentofu.rss
