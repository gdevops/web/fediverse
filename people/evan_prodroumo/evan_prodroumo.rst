.. index::
   ! Evan Prodroumo


.. _evan_prodroumo:

===================================================================
**Evan Prodroumo**
===================================================================

- https://octodon.social/@cwebber/109389730820332877


For anyone who doesn't  know, @evan was largely responsible for OStatus,
which is what the fediverse used to run on.  Then later ActivityPub was
based on Evan's Pump API.

@evan *more than anyone* did the key design work that the fediverse
is based on.

Evan is being too humble.  I am making up for it.  Evan is awesome.
