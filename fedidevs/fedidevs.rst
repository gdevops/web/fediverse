.. index::
   pair: Fedidevs ; https://fedidevs.com/ 
   pair: Starter packs ; Écrivain·e·s de l’imaginaire 
   ! Fedidevs 

.. _fedidevs:

==========================
**Fedidevs**
==========================

- https://fedidevs.com/
- https://fosstodon.org/@fedidevs
- https://fosstodon.org/@anze3db
- https://fosstodon.org/@anze3db.rss
- https://github.com/anze3db/fedidevs
- https://github.com/anze3db/fedidevs/commits.atom

Devs
=========

- https://fedidevs.com/python/
- https://fedidevs.com/starter-packs/?q=python 
- https://fedidevs.com/rust/
- https://fedidevs.com/htmx/
- https://fedidevs.com/linux/
- https://fedidevs.com/opensource/
- https://fedidevs.com/django/
- https://fedidevs.com/starter-packs/?q=django
- https://fedidevs.com/machinelearning/


Starter packs
==================

- https://fedidevs.com/starter-packs/

Écrivain·e·s de l’imaginaire
---------------------------------

- https://fedidevs.com/s/NDU/ (Écrivain·e·s de l’imaginaire)

Écrivain·e·s, romanciers et autrices : iels écrivent des livres en français 
pour nous faire rêver ou frissonner. 

Pour être ajouté: 

- 1) vérifiez que votre profil est en mode "discoverable" depuis au moins 24h 
- 2) Vérifiez que votre bio indique votre activité d’écriture et comment vous lire. 
- 3) Envoyez un message privé à @ploum@mamot.fr


Climat
------------

- https://fedidevs.com/starter-packs/?q=climat 
- https://fedidevs.com/starter-packs/?q=francophones

Médias, associations et experts de l'écologie par https://pouet.chapril.org/@Greguti
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

- https://fedidevs.com/s/MTYw/

Comptes francophones sur les thématiques de l'écologie : environnement, énergies 
renouvelables, numérique responsable, transition écologique, circulations actives, 
transformation des espaces publics, pollutions, etc. 

Il s'agit de médias, de personnalités militantes et/ou expertes, d'ONG et d'associations


Django
--------

- https://fedidevs.com/starter-packs/?q=django


Python
-----------

- https://fedidevs.com/starter-packs/?q=python 

Wiki people
-----------------

- https://fedidevs.com/starter-packs/?q=wiki
- https://fedidevs.com/s/MjA4/
