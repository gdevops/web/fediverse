
.. _bluesky:

========================================
**Bluesky** (A venture-funded startup)
========================================


Tools
===========

- https://skircle.me/

Bridgy Fed connects web sites, the fediverse, and Bluesky
=============================================================

- https://fed.brid.gy/

Bridgy Fed connects web sites, the fediverse, and Bluesky. 

You can use it to make your profile on one visible in another, follow people, 
see their posts, and reply and like and repost them. Interactions work in 
both directions as much as possible

Considering Bluesky ?
=============================

- https://mastodon.social/@ASegar/113494145062267528

Please bear in mind that #Bluesky is:

Funded by #BlockchainCapital:

- co-founded by Steve Bannon pal Brock Pierce, a major crypto advocate, 
  and close friend of Eric Adams
- run in part by Kirill Dorofeev, who also works for #VK, Russia’s state social network.

- A venture-funded startup, so once they need to monetize they're likely 
  to turn to an exploitative business model.

Sources: @davetroy, @thenexusofprivacy


How to set your domain as your handle
=======================================

- https://bsky.social/about/blog/4-28-2023-domain-handle-tutorial

Profiles bluesky
====================

- https://bsky.app/profile/raar2021.bsky.social
- https://bsky.app/profile/emmanuelrevah.fr
- https://bsky.app/profile/oliouchka.bsky.social
- https://bsky.app/profile/bonpote.com
- https://bsky.app/profile/framasoft.org
- https://bsky.app/profile/abkgrenoble.bsky.social
- https://bsky.app/profile/gillesnamur.bsky.social
- https://bsky.app/profile/slpng-giants-fr.bsky.social
- https://bsky.app/profile/c-a-g0101.bsky.social
- https://bsky.app/profile/oeildejustice.bsky.social 
- https://bsky.app/profile/associationarra.bsky.social
- https://bsky.app/profile/prlogos.bsky.social 
  (Compte francophone dédié à la recherche et au signalement de comptes 
  d'extrême droite, racistes, sexistes, homophobes, transphobes, etc...)



Listes de blocage
-------------------

- https://bsky.app/profile/oeildejustice.bsky.social/post/3lb5qe7amec2w

Starters pack
--------------

- https://bsky.app/starter-pack/sciencecqfd.bsky.social/3lb7r5nxuni2b
- https://bsky.app/starter-pack/longcovidadvoc.bsky.social/3lbad3bbezv2w
- https://bsky.social/about/blog/4-28-2023-domain-handle-tutorial
- https://bsky.app/starter-pack/soniaseneviratne.bsky.social/3larwap3xit2k
