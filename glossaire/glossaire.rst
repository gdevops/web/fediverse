.. index::
   pair: Glossaire ; Fediverse


.. _glossaire_fediverse:

========================================
Glossaire dev
========================================

.. glossary::

   ntfy
       ntfy (pronounce: notify) is a simple HTTP-based pub-sub notification service.

       - https://github.com/binwiederhier/ntfy

